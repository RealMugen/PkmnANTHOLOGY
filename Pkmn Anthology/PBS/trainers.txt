﻿#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# PALLET TOWN
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[RIVAL1,Gary]
LoseText = "Eso no fue nada... Me dio el sol en los ojos..."
Pokemon = EEVEE,4
    Gender = male
    Moves = HELPINGHAND,TACKLE,TAILWHIP
    AbilityIndex = 0


#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# CIUDAD VERDE
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
[TEAMROCKETJJ,Jessie & James]
LoseText = "¡Oye chico, no eres cualquier cosa!"
Pokemon = EKANS,5
    Gender = female
    Moves = WRAP,LEER,POISONSTING
    AbilityIndex = 0
Pokemon = KOFFING,5
    Gender = male
    Moves = POISONGAS,TACKLE,SMOG
    AbilityIndex = 0

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# RUTA 22
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[RIVAL1,Gary,1]
LoseText = "¡Auuu! ¡Tuviste demasiada suerte!"
Pokemon = EEVEE,8
    Gender = male
    Moves = HELPINGHAND,TACKLE,TAILWHIP
    AbilityIndex = 0
Pokemon = PIDGEY,9
    Gender = male
    Moves = TACKLE,SANDATTACK,GUST
    AbilityIndex = 0

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# BOSQUE VERDE
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

#-------------------------------
[BUGCATCHER,Jano]
LoseText = "¡Oh, no! ¡Mi Caterpie ya no da para mas!"
Pokemon = WEEDLE,6
Pokemon = CATERPIE,6
#-------------------------------
[BUGCATCHER,Domin]
LoseText = "¿Será posible? ¡Me he quedado sin Pokémon!"
Pokemon = WEEDLE,7
Pokemon = KAKUNA,7
Pokemon = WEEDLE,7
#-------------------------------
[BUGCATCHER,Anton]
LoseText = "Vaya, pues se ve que tampoco soy demasiado fuerte."
Pokemon = CATERPIE,7
Pokemon = CATERPIE,8
#-------------------------------
[BUGCATCHER,Carmelo]
LoseText = "¡Oh! ¡He perdido!"
Pokemon = CATERPIE,7
Pokemon = METAPOD,7
Pokemon = CATERPIE,7
#-------------------------------
[BUGCATCHER,Sami]
LoseText = "¡Me rindo! ¡Eres demasiado fuerte!"
Pokemon = WEEDLE,9
#-------------------------------
[SAMURAIBUGCATCHER,Samurai]
LoseText = "No esperaba eso."
Pokemon = METAPOD,7
Pokemon = PINSIR,8
#-------------------------------
[LASSV,Sonia] #VALOR
LoseText = "¡No me lo puedo creer!"
Pokemon = RATTATA,4
#-------------------------------
[BUGCATCHERI,Mariano] #INSTINTO
LoseText = "Me has hecho morder el polvo en un abrir y cerrar de ojos."
Pokemon = WEEDLE,3
#-------------------------------
[LASSS,Miki] #SABIDURÍA
LoseText = "¡Pero bueno, al menos contesta a mi pregunta...!"
Pokemon = NIDORANfE,3
#-------------------------------
[BUGCATCHER,Dionis] #MASTER
LoseText = "¡Increíble!¡Tienes un talento innato!"
Pokemon = WEEDLE,65
    Gender = male
    Moves = POISONSTING,STRINGSHOT
    IV = 20,20,20,20,20,20
#-------------------------------
[LASS,Ikue] #MASTER
LoseText = "¡Guau! ¡posees una fuerza descomunal!"
Pokemon = PIKACHU,75
    Gender = female
    Moves = SLAM,TOXIC,SUBSTITUTE,REFLECT
    IV = 20,20,20,20,20,20

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# CIUDAD PLATEADA
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

#-------------------------------
[CAMPER,Angelito]
LoseText = "¡Vaya! ¡Años luz no suponen tiempo! ¡Miden distancia!"
Pokemon = GEODUDE,8
Pokemon = SANDSHREW,8
#-------------------------------
[HIKER,Benjamín]
LoseText = "¡Para ser tan joven eres bastante bueno!"
Pokemon = GEODUDE,9
Pokemon = DIGLETT,9
#-------------------------------
[LEADER_Brock,Brock]
Items = FULLRESTORE
LoseText = "Te he subestimado."
Pokemon = GEODUDE,12
    Gender = male
    Moves = DEFENSECURL,HEADSMASH,ROCKPOLISH,ROCKTHROW
    AbilityIndex = 0
    IV = 20,20,20,20,20,20
Pokemon = ONIX,14
    Gender = male
    Moves = HEADSMASH,ROCKTHROW,RAGE,ROCKTOMB
    AbilityIndex = 0
    Item = SITRUSBERRY
    IV = 20,20,20,20,20,20
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# RUTA 03
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

#-------------------------------
[LASS,Lucrecia]
LoseText = "¡Eres de lo que no hay!"
Pokemon = PIDGEY,9
    Gender = female
Pokemon = PIDGEY,9
    Gender = female
#-------------------------------
[BUGCATCHER,Cornelio]
LoseText = "¡Has ganado otra vez!"
Pokemon = CATERPIE,10
    Gender = male
Pokemon = WEEDLE,10
    Gender = female
Pokemon = CATERPIE,10
    Gender = male
#-------------------------------
[YOUNGSTER,Calixto]
LoseText = "¡He perdido! ¡He perdido!"
Pokemon = SPEAROW,14
    Gender = male
#-------------------------------
[YOUNGSTER,Ben]
LoseText = "¡Me he quedado corto, más que mis pantalones!"
Pokemon = RATTATA,11
    Gender = male
Pokemon = EKANS,11
    Gender = male
#-------------------------------
[LASS,Susana]
LoseText = "¡Sé amable!"
Pokemon = RATTATA,10
    Gender = female
Pokemon = NIDORANfE,10
#-------------------------------
[BUGCATCHER,Gregorio]
LoseText = "¡Con nuevos POKÉMON te habría ganado!"
Pokemon = WEEDLE,9
    Gender = male
Pokemon = KAKUNA,9
    Gender = female
Pokemon = CATERPIE,9
    Gender = male
Pokemon = METAPOD,9
    Gender = male
#-------------------------------
[LASS,Roberta]
LoseText = "¿Eso es todo?"
Pokemon = JIGGLYPUFF,14
    Gender = female
#-------------------------------
[BUGCATCHER,Jaime]
LoseText = "¡Estoy acabado!"
Pokemon = CATERPIE,11
    Gender = male
Pokemon = METAPOD,11
    Gender = male
#-------------------------------
[INSTRUCTOR,Ramón]
LoseText = "¡No se te dan nada mal los combates!"
Pokemon = BULBASAUR,11
    Gender = male
#-------------------------------
[CAMPER,Jaime] #VALOR
LoseText = "Morado me he puesto yo, pero de la rabia que me ha dado perder..."
Pokemon = NIDORANmA,7
    Gender = male
#-------------------------------
[CAMPER,Rudolf] #INSTINTO
LoseText = "Es que soy impredecible..."
Pokemon = NIDORANmA,7
    Gender = male
#-------------------------------
[PICNICKER,Roberta]
LoseText = "Solo lo dije porque podría tropezarme..."
Pokemon = NIDORANfE,7
#-------------------------------
[COOLTRAINER_M,Isma] #MASTER
LoseText = "¡Bien hecho! Tu fuerza no conoce límites."
Pokemon = NIDORANfE,65
    Moves = SUPERFANG,THUNDERBOLT,ICEBEAM
    IV = 20,20,20,20,20,20
#Tras perder:   ¡Vuelve a verme cuando quieras combatir de nuevo!

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# MT.LUNA
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

#-------------------------------
[BUGCATCHER,Casimiro]
LoseText = "¡Me has ganado!"
Pokemon = WEEDLE,11
    Gender = male
Pokemon = KAKUNA,11
    Gender = male
#-------------------------------
[LASS,Iris]
LoseText = "¿He perdido?"
Pokemon = CLEFAIRY,14
    Gender = female
#-------------------------------
[SUPERNERD,Gustavo]
LoseText = "Me parece que estos Pokémon no consiguen un nivel de rendimiento óptimo..."
Pokemon = MAGNEMITE,11
Pokemon = VOLTORB,11
#-------------------------------
[BUGCATCHER,Robi]
LoseText = "He perdido."
Pokemon = CATERPIE,10
    Gender = male
Pokemon = CATERPIE,10
    Gender = male
Pokemon = METAPOD,10
    Gender = male
#-------------------------------
[LASS,Mirna]
LoseText = "¡Oh! ¡He perdido!"
Pokemon = ODDISH,11
    Gender = female
Pokemon = BELLSPROUT,11
    Gender = female
#-------------------------------
[YOUNGSTER,José]
LoseText = "¡Perder es lo peor!"
Pokemon = RATTATA,10
    Gender = male
Pokemon = RATTATA,10
    Gender = male
Pokemon = ZUBAT,10
    Gender = male
#-------------------------------
[HIKER,Marcos]
LoseText = "¡Uau! ¡Has vuelto a impresionarme!"
Pokemon = GEODUDE,10
    Gender = male
Pokemon = GEODUDE,10
    Gender = male
Pokemon = ONIX,10
    Gender = male
#-------------------------------
[TEAMROCKET_F,Chica,1]
LoseText = "¡Lo he hecho fatal!"
Pokemon = SANDSHREW,11
    Gender = male
Pokemon = RATTATA,11
    Gender = male
Pokemon = ZUBAT,11
    Gender = male
#-------------------------------
[TEAMROCKET_M,Chico,1]
LoseText = "Pues eres muy fuerte..."
Pokemon = ZUBAT,11
    Gender = male
Pokemon = EKANS,11
    Gender = male
#-------------------------------
[TEAMROCKET_M,Chico,2]
LoseText = " ¡Estoy muy enfadado!"
Pokemon = RATTATA,13
    Gender = male
Pokemon = SANDSHREW,13
    Gender = male
#-------------------------------
[TEAMROCKET_F,Chica,2]
LoseText = "¡Argh! ¡Eres lo peor!"
Pokemon = SANDSHREW,11
    Gender = male
Pokemon = RATTATA,13
    Gender = male
Pokemon = ZUBAT,13
    Gender = male
#-------------------------------
[SUPERNERD,Miguel]
LoseText = "¡De acuerdo! ¡Los compartiré!"
Pokemon = GRIMER,12
    Gender = male
Pokemon = KOFFING,12
    Gender = male
Pokemon = VOLTORB,12
#-------------------------------
[TEAMROCKETJJ,Jessie & James,1]
LoseText = "¡¿Quién diablos te crees?!"
Pokemon = EKANS,12
    Gender = female
    AbilityIndex = 0
Pokemon = KOFFING,12
    Gender = male
    AbilityIndex = 0
#-------------------------------
[BUGCATCHER,Fer] #MASTER
LoseText = "¡Increíble! ¡Tienes un talento innato!"
Pokemon = PARAS,70
    Gender = male
#-------------------------------
[LASS,Lidia] #MASTER
LoseText = "¡Guau! ¡Posees una fuerza descomunal!"
Pokemon = CLEFABLE,80
    Gender = female
#-------------------------------
[HIKER,Odón] #MASTER
LoseText = " ¡Madre mía, eres superfuerte!"
Pokemon = OMANYTE,70
    Gender = male
#-------------------------------
[HIKER,Paulino] #MASTER
LoseText = " ¡Me derritoooo!"
Pokemon = KABUTO,70
    Gender = male

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# RUTA 04
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

#-------------------------------
[INSTRUCTOR,Eduardo]
LoseText = "Atravesar el Mt.Luna debe haber sido pan comido para ti"
Pokemon = MEOWTH,13
    Moves = PAYDAY,BITE,FURYSWIPES,TAUNT
    IV = 20,20,20,20,20,20
    Gender = female
#-------------------------------
[YOUNGSTER,Nicolás] #MASTER
LoseText = "¡Guau!¡Posees una fuerza descomunal!"
Pokemon = MAGIKARP,70
    Moves = TACKLE,SPLASH
    IV = 20,20,20,20,20,20
    Gender = male
#-------------------------------
[COOLTRAINER_M,Loren] #MASTER
LoseText = "¡Bien hecho! Tu fuerza no conoce límites."
Pokemon = NIDORANmA,70
    Moves = IRONTAIL,HEADBUTT
    IV = 20,20,20,20,20,20
    Gender = male
#-------------------------------
[LASS,Mariona] #MASTER
LoseText = "¡Guau! ¡Posees una fuerza descomunal!"
Pokemon = BULBASAUR,65
    Moves = SLUDGEBOMB,TAKEDOWN,GROWTH
    IV = 20,20,20,20,20,20
    Gender = female
#-------------------------------
[SCIENTIST,Serafín] #MASTER
LoseText = "Impresionante. Jamás había visto semejante fuerza."
Pokemon = EKANS,70
    Moves = GLARE,BITE,ROCKSLIDE
    IV = 20,20,20,20,20,20
    Gender = male
#-------------------------------
[BEAUTY,Agustina] #MASTER
LoseText = "¡Guau! ¡Posees una fuerza descomunal!"
Pokemon = GOLDUCK,75
    Moves = PSYCHIC,YAWN,CALMMIND
    IV = 20,20,20,20,20,20
    Gender = male




















#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

#-------------------------------
[TEAMROCKET_F,Grunt,1]
LoseText = "You're too good for me!"
Pokemon = BURMY,19
Pokemon = WINGULL,19
Pokemon = ELECTABUZZ,20
    Shadow = yes
#-------------------------------
[CAMPER,Jeff]
LoseText = "A very good battle, indeed!"
Pokemon = SPEAROW,16
Pokemon = SENTRET,16
#-------------------------------
[CAMPER,Jeff,1]
LoseText = "You beat me again!"
Pokemon = FEAROW,29
Pokemon = FURRET,29
#-------------------------------
[PICNICKER,Susie]
LoseText = "Defeated! Oh my!"
Pokemon = MEOWTH,18
#-------------------------------
[PICNICKER,Susie,1]
LoseText = "Defeated! Oh my!"
Pokemon = PERSIAN,32
#-------------------------------
[HIKER,Ford]
LoseText = "You're too good for me!"
Pokemon = MACHOP,19
#-------------------------------
[FISHERMAN,Andrew]
LoseText = "Washed out!"
Pokemon = MAGIKARP,14
Pokemon = MAGIKARP,18
#-------------------------------
[BEAUTY,Bridget]
LoseText = "My Skitty!"
Pokemon = SKITTY,18
#-------------------------------
[LASS,Crissy]
LoseText = "You didn't have to win so convincingly!"
Pokemon = PLUSLE,17
Pokemon = MINUN,17
#-------------------------------
[COOLCOUPLE,Alice & Bob]
LoseText = "Our teamwork failed!"
Pokemon = TURTWIG,19
Pokemon = CHIMCHAR,19
#-------------------------------
[POKEMONTRAINER_Brendan,Brendan]
LoseText = "..."
Pokemon = MARSHTOMP,19
#-------------------------------
[SWIMMER2_F,Ariel]
LoseText = "Washed out!"
Pokemon = STARYU,15
Pokemon = GOLDEEN,11
#-------------------------------
[RIVAL1,Blue,1]
LoseText = "Not too shabby."
Pokemon = PIDGEOTTO,17
Pokemon = RATTATA,15
Pokemon = CHARMANDER,18
#-------------------------------
[RIVAL1,Blue,2]
LoseText = "Not too shabby."
Pokemon = PIDGEOTTO,17
Pokemon = RATTATA,15
Pokemon = SQUIRTLE,18
#-------------------------------
[CHAMPION,Blue]
LoseText = "A good battle indeed!"
Pokemon = VENUSAUR,63
    Item = SITRUSBERRY
Pokemon = CHARIZARD,63
    Item = SITRUSBERRY
Pokemon = BLASTOISE,63
    Item = SITRUSBERRY
