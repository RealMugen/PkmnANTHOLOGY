﻿#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#------ENCOUNTERS TYPES
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#:Land => "Hierba",
#:LandDay => "Hierba • Día",
#:LandNight => "Hierba • Noche",
#:LandMorning => "Hierba • Mañana",
#:LandAfternoon => "Hierba • Tarde", 
#:LandEvening => "Hierba • Atardecer",
#:Cave => "Cueva",
#:CaveDay => "Cueva • Día",
#:CaveNight => "Cueva • Noche",
#:CaveMorning => "Cueva • Mañana",
#:CaveAfternoon => "Cueva • Tarde",
#:CaveEvening => "Cueva • Atardecer",
#:Water => "Surf",
#:WaterDay => "Surf • Día",
#:WaterNight => "Surf • Noche",
#:WaterMorning => "Surf • Mañana",
#:WaterAfternoon => "Surf • Tarde",
#:WaterEvening => "Surf • Atardecer",
#:OldRod => "Pescando • Caña Vieja",
#:GoodRod => "Pescando • Caña Buena",
#:SuperRod => "Pescando • Supercaña",
#:RockSmash => "Golpe Roca",
#:HeadbuttLow => "Cabezazo • Raro",
#:HeadbuttHigh => "Cabezazo • Común",
#:BugContest => "Competencia de Bichos",
#:SeasonSummer => "Verano",
#:SeasonSpring => "Primavera",
#:SeasonAutumn => "Otoño",
#:SeasonWinter => "Invierno",
#:Rain => "Hierba • Lluvia",
#:RainDay => "Hierba • Lluvia • Día",
#:RainNight => "Hierba • Lluvia • Noche",
#:RainMorning => "Hierba • Lluvia • Mañana",
#:RainAfternoon => "Hierba • Lluvia • Tarde", 
#:RainEvening => "Hierba • Lluvia • Atardecer",
#:HeavyRain => "Hierba • Lluvia Intensa",
#:HeavyRainDay => "Hierba • Lluvia Intensa • Día",
#:HeavyRainNight => "Hierba • Lluvia Intensa • Noche",
#:HeavyRainMorning => "Hierba • Lluvia Intensa • Mañana",
#:HeavyRainAfternoon => "Hierba • Lluvia Intensa • Tarde",
#:HeavyRainEvening => "Hierba • Lluvia Intensa • Atardecer",
#:Storm => "Hierba • Tormenta",
#:StormDay => "Hierba • Tormenta • Día",
#:StormNight => "Hierba • Tormenta • Noche",
#:StormMorning => "Hierba • Tormenta • Mañana",
#:StormAfternoon => "Hierba • Tormenta • Tarde",
#:StormEvening => "Hierba • Tormenta • Atardecer",
#:Snow => "Hierba • Nieve",
#:SnowDay => "Hierba • Nieve • Día",
#:SnowNight => "Hierba • Nieve • Noche",
#:SnowMorning => "Hierba • Nieve • Mañana",
#:SnowAfternoon => "Hierba • Nieve • Tarde",
#:SnowEvening => "Hierba • Nieve • Atardecer",
#:Blizzard => "Hierba • Ventisca",
#:BlizzardDay => "Hierba • Ventisca • Día",
#:BlizzardNight => "Hierba • Ventisca • Noche",
#:BlizzardMorning => "Hierba • Ventisca • Mañana",
#:BlizzardAfternoon => "Hierba • Ventisca • Tarde",
#:BlizzardEvening => "Hierba • Ventisca • Atardecer",
#:Sandstorm => "Hierba • Tormenta de Arena",
#:SandstormDay => "Hierba • Tormenta de Arena • Día",
#:SandstormNight => "Hierba • Tormenta de Arena • Noche",
#:SandstormMorning => "Hierba • Tormenta de Arena • Mañana",
#:SandstormAfternoon => "Hierba • Tormenta de Arena • Tarde",
#:SandstormEvening => "Hierba • Tormenta de Arena • Atardecer",
#:Sun => "Hierba • Soleado",
#:SunDay => "Hierba • Soleado • Día",
#:SunNight => "Hierba • Soleado • Noche",
#:SunMorning => "Hierba • Soleado • Mañana",
#:SunAfternoon => "Hierba • Soleado • Tarde",
#:SunEvening => "Hierba • Soleado • Atardecer",
#:Moonlight => "Hierba • Luz de Luna",
#:Fog => "Hierba • Niebla",
#:FogDay => "Hierba • Niebla • Día",
#:FogNight => "Hierba • Niebla • Noche",
#:FogMorning => "Hierba • Niebla • Mañana",
#:FogAfternoon => "Hierba • Niebla • Tarde",
#:FogEvening => "Hierba • Niebla • Atardecer",
#:PhenomenonGrass => "Hierba • Phenomena",
#:PhenomenonWater => "Surf • Phenomena",
#:PhenomenonCave => "Cueva • Phenomena",
#:PhenomenonBird => "Cielo • Phenomena"

#-------------------------------
#-------------------------------
#-------------------------------
#--------WEATHER ENCOUNTERS
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
# NUBLADO • NORMAL ,ROCA
# PARCIALMENTE NUBLADO • HADA, LUCHA, VENENO
# VIENTO • DRAGON, VOLADOR, PSÍQUICO
# LLUVIA • AGUA, BICHO, ELECTRICO, PLANTA
# NIEBLA • FANTASMA, SINIESTRO
# NIEVE • HIELO, ACERO
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------





[002] # Pueblo Paleta
Land,21
    40,PIDGEY,2,3
    40,RATTATA,2,3
    9,PIDGEY,4,5
    9,RATTATA,4,5
    1,PIDGEY,6
    1,RATTATA,6
LandNight,21
    20,PIDGEY,2,3
    60,RATTATA,2,3
    9,PIDGEY,4,5
    9,RATTATA,4,5
    2,RATTATA,6
Water,2
    60,TENTACOOL,14,19
    30,MAGIKARP,15,16
    10,POLIWAG,14,16
OldRod
    100,MAGIKARP,5,5
GoodRod
    60,MAGIKARP,5,10
    20,POLIWAG,5,10
    20,GOLDEEN,5,10
SuperRod
    40,POLIWAG,5,15
    40,TENTACOOL,5,15
    15,STARYU,5,10
    5,TENTACOOL,10,20

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[015] # Ruta 01
Land,21
    20,PIDGEY,2,3
    20,RATTATA,2,3
    20,SPEAROW,2,3
    10,PIDGEY,4,5
    10,SPEAROW,4,5
    5,MANKEY,2,4
    5,SANDSHREW,2,5
LandNight,21
    20,PIDGEY,2,3
    40,RATTATA,2,3
    10,PIDGEY,4,5
    10,SPEAROW,4,5
    5,MANKEY,2,4
    5,SANDSHREW,2,5
Water,2
    100,MAGIKARP,2
OldRod
    100,MAGIKARP,2
GoodRod
    100,MAGIKARP,8
SuperRod
    100,MAGIKARP,12

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[016] # CIUDAD VERDE
Water,2
    50,PSYDUCK,20,40
    50,SLOWPOKE,20,40
OldRod
    100,MAGIKARP,5
GoodRod
    20,POLIWAG,5,15
    20,GOLDEEN,5,15
    60,MAGIKARP,5,15
SuperRod
    20,POLIWAG,15,25
    20,POLIWHIRL,15,30
    20,GYARADOS,15,25
    20,PSYDUCK,15,35
    20,SLOWPOKE,15,35

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[023] # RUTA 22
LandDay,21
    30,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
LandNight,21
    30,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
Water,2
    60,PSYDUCK,3,5
    40,SLOWPOKE,3,5
OldRod
    100,MAGIKARP,5,5
GoodRod
    60,MAGIKARP,5,15
    20,POLIWAG,5,15
    20,GOLDEEN,5,15
SuperRod
    20,POLIWAG,15,25
    10,POLIWHIRL,20,30
    10,GYARADOS,15,25
    30,SLOWPOKE,15,35
    30,PSYDUCK,15,35
SeasonSummer,21
    30,MANKEY,2,5
    40,RATTATA,2,5
    25,SPEAROW,2,5
    5,DODUO,6
SeasonSpring,21
    30,MANKEY,2,5
    40,RATTATA,2,5
    25,SPEAROW,2,5
    5,LEDYBA,2,3
SeasonWinter,21
    30,MANKEY,2,5
    40,RATTATA,2,5
    25,SPEAROW,2,5
    5,BIDOOF,5
SeasonAutumn,21
    30,MANKEY,2,5
    40,RATTATA,2,5
    25,SPEAROW,2,5
    5,SPINARAK,2,3
RainDay,21
    20,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
    10,POLIWAG,3,4
RainNight,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,POLIWAG,3,4
HeavyRainDay,21
    20,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
    10,POLIWAG,3,4
HeavyRainNight,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,POLIWAG,3,4
StormDay,21
    20,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
    10,POLIWAG,3,4
StormNight,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,POLIWAG,3,4
SnowDay,21
    20,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
    10,ZIGZAGOON,3,4
SnowNight,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,ZIGZAGOON,3,4
BlizzardDay,21
    20,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
    10,ZIGZAGOON,3,4
BlizzardNight,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,ZIGZAGOON,3,4
Sun,21
    20,MANKEY,2,5
    40,RATTATA,2,5
    30,SPEAROW,2,5
    10,PONYTA,6
Moonlight,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,HOOTHOOT,2,3
Fog,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,HOOTHOOT,2,3
Sakura,21
    20,MANKEY,2,7
    50,RATTATA,2,7
    20,SPEAROW,2,7
    10,ODDISH,2,3

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[025] # RUTA 2
Land,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    40,RATTATA,3,5
LandNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    20,KAKUNA,3,7
PhenomenonGrass
    30,PIDGEY,6,8
    30,RATTATA,6,8
    30,CATERPIE,6,8
    8,BUTTERFREE,10,11
    2,LEDYBA,10
SeasonSummer,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,SPINARAK,5,7
SeasonSpring,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,LEDYBA,5,7
SeasonWinter,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,HOOTHOOT,3,7
SeasonAutumn,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,SENTRET,3,7
RainDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,MINUN,3
RainNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,MINUN,3
HeavyRainDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,MINUN,3
HeavyRainNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,MINUN,3
StormDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,SHINX,3,5
StormNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,SHINX,3,5
SnowDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,ZIGZAGOON,3,5
SnowNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,ZIGZAGOON,3,5
BlizzardDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,ZIGZAGOON,3,5
BlizzardNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,ZIGZAGOON,3,5
Sun,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,PLUSLE,3,5
Moonlight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,HOOTHOOT,3,5
Fog,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,HOOTHOOT,3,5
Sakura,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,PLUSLE,3,5


#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[027] # RUTA 2
Land,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    40,RATTATA,3,5
LandNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    20,KAKUNA,3,7
PhenomenonGrass
    30,PIDGEY,6,8
    30,RATTATA,6,8
    30,CATERPIE,6,8
    8,BUTTERFREE,10,11
    2,LEDYBA,10
SeasonSummer,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,SPINARAK,5,7
SeasonSpring,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,LEDYBA,5,7
SeasonWinter,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,HOOTHOOT,3,7
SeasonAutumn,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,SENTRET,3,7
RainDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,MINUN,3
RainNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,MINUN,3
HeavyRainDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,MINUN,3
HeavyRainNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,MINUN,3
StormDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,SHINX,3,5
StormNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,SHINX,3,5
SnowDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,ZIGZAGOON,3,5
SnowNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,ZIGZAGOON,3,5
BlizzardDay,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,ZIGZAGOON,3,5
BlizzardNight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,ZIGZAGOON,3,5
Sun,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,PLUSLE,3,5
Moonlight,21
    30,WEEDLE,4,5
    50,RATTATA,3,5
    15,KAKUNA,3,7
    5,HOOTHOOT,3,5
Fog,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,HOOTHOOT,3,5
Sakura,21
    10,WEEDLE,4,5
    10,CATERPIE,4,5
    40,PIDGEY,3,5
    35,RATTATA,3,5
    5,PLUSLE,3,5

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[028] # BOSQUE VERDE
Land,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,PIDGEOTTO,10
LandNight,21
    15,KAKUNA,3,7
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,PIDGEOTTO,10
PhenomenonGrass
    30,PIKACHU,6,8
    30,CATERPIE,6,8
    30,WEEDLE,6,8
    10,PIDGEOTTO,10,11
SeasonSummer,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,ODDISH,3,6
SeasonSpring,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    2,BULBASAUR,3,6
SeasonWinter,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SPINARAK,3,4
SeasonAutumn,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SEEDOT,3,7
RainDay,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SHROOMISH,3,7
RainNight,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SHROOMISH,3,7
HeavyRainDay,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SHROOMISH,3,7
HeavyRainNight,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SHROOMISH,3,7
StormDay,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,PICHU,3,7
StormNight,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,PICHU,3,7
SnowDay,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SEEDOT,3,7
SnowNight,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SEEDOT,3,7
BlizzardDay,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SEEDOT,3,7
BlizzardNight,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,SEEDOT,3,7
Sun,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,NUMEL,5
Moonlight,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,KRICKETOT,3,5
Fog,21
    15,KAKUNA,3,5
    10,METAPOD,3,5
    20,CATERPIE,3,5
    25,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,KRICKETOT,3,7
Sakura,21
    10,KAKUNA,3,5
    15,METAPOD,3,5
    25,CATERPIE,3,5
    20,WEEDLE,3,5
    10,PIKACHU,3,5
    10,PIDGEY,3,5
    5,BEEDRILL,10
    5,BUDEW,3,5

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[031] # CIUDAD PLATEADA
Land,21
    30,PIDGEY,3,5
    30,RATTATA,3,5
    20,GEODUDE,2,5
    10,POLIWAG,2,5
    5,CATERPIE,4,5
    5,WEEDLE,4,5
LandNight,21
    20,PIDGEY,3,5
    40,RATTATA,3,5
    20,GEODUDE,2,5
    10,POLIWAG,2,5
    5,CATERPIE,4,5
    5,WEEDLE,4,5
Water,2
    60,PSYDUCK,3,5
    40,SLOWPOKE,3,5
OldRod
    100,MAGIKARP,5,5
GoodRod
    60,MAGIKARP,5,15
    20,POLIWAG,5,15
    20,GOLDEEN,5,15
SuperRod
    20,POLIWAG,15,25
    10,POLIWHIRL,20,30
    10,GYARADOS,15,25
    30,SLOWPOKE,15,35
    30,PSYDUCK,15,35

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[040] # RUTA 03
Land,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    10,JIGGLYPUFF,3,7
LandNight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    10,JIGGLYPUFF,7
PhenomenonGrass
    40,SPEAROW,9,10
    30,NIDORANmA,8,9
    10,NIDORANfE,9,10
    5,JIGGLYPUFF,8,9
    5,CLEFAIRY,6,7
SeasonSummer,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,PLUSLE,5,7
SeasonSpring,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,WURMPLE,6,10
SeasonWinter,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,MINUN,5,7
SeasonAutumn,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,PINECO,6,7
RainDay,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,SHINX,5,8
RainNight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,SHINX,5,8
HeavyRainDay,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,SHINX,5,8
HeavyRainNight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,SHINX,5,8
StormDay,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,SHINX,5,8
StormNight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,SHINX,5,8
SnowDay,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,HOOTHOOT,5,8
SnowNight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,HOOTHOOT,5,8
BlizzardDay,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,HOOTHOOT,5,8
BlizzardNight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,HOOTHOOT,5,8
Sun,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,GULPIN,5
Moonlight,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,BALTOY,5
Fog,21
    20,SPEAROW,6,8
    30,RATTATA,5,10
    20,EKANS,6
    20,ZUBAT,5
    5,JIGGLYPUFF,7
    5,BALTOY,5
Sakura,21
    20,SPEAROW,6,8
    30,PIDGEY,6,7
    20,NIDORANmA,6,7
    20,MANKEY,7
    5,JIGGLYPUFF,3,7
    5,WURMPLE,5

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[043] # MT.LUNA • 1
Cave,5
    30,GEODUDE,7,9
    40,ZUBAT,6,10
    20,PARAS,8
    10,CLEFAIRY,8
PhenomenonCave
    30,GEODUDE,10,12
    40,ZUBAT,12,13
    20,PARAS,10,12
    10,CLEFAIRY,12,13

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[044] # MT.LUNA • 2
Cave,5
    100,PARAS,5,10
PhenomenonCave
    30,GEODUDE,10,12
    40,ZUBAT,12,13
    20,PARAS,10,12
    10,CLEFAIRY,12,13

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[045] # MT.LUNA • 3
Cave,5
    30,GEODUDE,7,9
    40,ZUBAT,6,10
    20,PARAS,8
    10,CLEFAIRY,8
PhenomenonCave
    30,GEODUDE,10,12
    40,ZUBAT,12,13
    20,PARAS,10,12
    10,CLEFAIRY,12,13

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------

[046] # RUTA 04
Land,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    20,MANKEY,10,12
    10,JIGGLYPUFF,6
LandNight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    17,MANKEY,10,12
    3,ARBOK,10
SeasonSummer,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,WHISMUR,5
SeasonSpring,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,WURMPLE,6,10
SeasonWinter,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,BIDOOF,8
SeasonAutumn,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,PINECO,6,7
RainDay,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,PSYDUCK,3,4
RainNight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,PSYDUCK,10
HeavyRainDay,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,PSYDUCK,3,4
HeavyRainNight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,PSYDUCK,10
StormDay,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,PSYDUCK,3,4
StormNight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,PSYDUCK,10
SnowDay,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,ZIGZAGOON,8
SnowNight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,ZIGZAGOON,8
BlizzardDay,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    25,MANKEY,10,12
    5,ZIGZAGOON,8
BlizzardNight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,ZIGZAGOON,8
Sun,21
    20,RATTATA,8,12
    30,SPEAROW,8,12
    20,EKANS,6,12
    28,MANKEY,10,12
    2,CHARMANDER,8
Moonlight,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,HOOTHOOT,9,10
Fog,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    5,HOOTHOOT,9,10
Sakura,21
    40,RATTATA,8,12
    20,ZUBAT,5
    20,EKANS,8
    25,MANKEY,10,12
    2,BUTTERFREE_1,10

#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------
#-------------------------------




[999] # Tiall Region
Land,21
    20,GEODUDE_1,11,14
    20,RATTATA_1,11,14
    10,CUBONE,11,14
    10,DIGLETT_1,11,14
    10,MEOWTH_1,11,14
    10,PIKACHU,11,14
    10,SANDSHREW_1,11,14
    10,VULPIX_1,11,14
