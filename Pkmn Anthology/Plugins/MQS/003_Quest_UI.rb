
#===============================================================================
# Class that creates the scrolling list of quest names
#===============================================================================

class Window_Quest < Window_DrawableCommand

  def initialize(x,y,width,height,viewport)
    @quests = []
    super(x,y,width,height,viewport)
    self.windowskin = nil
    @selarrow = AnimatedBitmap.new("Graphics/Pictures/selarrow")
    RPG::Cache.retain("Graphics/Pictures/selarrow")
  end
  
  def quests=(value)
    @quests = value
    refresh
  end
  
  def itemCount
    return @quests.length
  end
  
  def drawItem(index,_count,rect)
    return if index>=self.top_row+self.page_item_max
    
    rect = Rect.new(rect.x+16,rect.y,rect.width-16,rect.height)
    name = $quest_data.getName(@quests[index].id)
    name = "<b>" + "#{name}" + "</b>" if @quests[index].story
    base = Color.new(249,249,249)
    shadow = Color.new(22,22,22)
    col = @quests[index].color
    pbSetFont(self.contents,"Barlow Condensed",26)
    drawFormattedTextEx(self.contents,rect.x,rect.y+4,
      436,"<c2=#{col}>#{name}</c2>",Color.new(249,249,249),Color.new(22,22,22))
    pbDrawImagePositions(self.contents,[[sprintf("Graphics/Pictures/QuestUI/new"),rect.width-16,rect.y+2]]) if @quests[index].new
  end

  def refresh
    @item_max = itemCount
    dwidth  = self.width-self.borderX
    dheight = self.height-self.borderY
    self.contents = pbDoEnsureBitmap(self.contents,dwidth,dheight)
    self.contents.clear
    for i in 0...@item_max
      next if i<self.top_item || i>self.top_item+self.page_item_max
      drawItem(i,@item_max,itemRect(i))
    end
    drawCursor(self.index,itemRect(self.index)) if itemCount >0
  end
  
  def update
    super
    @uparrow.visible   = false
    @downarrow.visible = false
  end
end

#===============================================================================
# Class that controls the UI
#===============================================================================
class QuestList_Scene
  
  def pbUpdate
    pbUpdateSpriteHash(@sprites)
  end

  def pbStartScene
    @viewport = Viewport.new(0,0,Graphics.width,Graphics.height)
    @viewport.z = 99999
    @sprites = {}
    @base = Color.new(249,249,249)
    @shadow = Color.new(22,22,22)
    addBackgroundPlane(@sprites,"bg","QuestUI/bg_1",@viewport)
    @sprites["base"] = IconSprite.new(0,0,@viewport)
    @sprites["base"].setBitmap("Graphics/Pictures/QuestUI/bg_2")
    @sprites["base2"] = IconSprite.new(0,0,@viewport)
    @sprites["base2"].setBitmap("Graphics/Pictures/QuestUI/bg_3")
    @sprites["base2"].opacity = 0
    @sprites["page_icon1"] = IconSprite.new(0,4,@viewport)
    if SHOW_FAILED_QUESTS
      @sprites["page_icon1"].setBitmap("Graphics/Pictures/QuestUI/page_icon1a")
    else
      @sprites["page_icon1"].setBitmap("Graphics/Pictures/QuestUI/page_icon1b")
    end
    @sprites["page_icon1"].x = Graphics.width - @sprites["page_icon1"].bitmap.width - 10
    @sprites["page_icon2"] = IconSprite.new(0,4,@viewport)
    @sprites["page_icon2"].setBitmap("Graphics/Pictures/QuestUI/page_icon2")
    @sprites["page_icon2"].x = Graphics.width - @sprites["page_icon2"].bitmap.width - 10
    @sprites["page_icon2"].opacity = 0
    @sprites["pageIcon"] = IconSprite.new(@sprites["page_icon1"].x,4,@viewport)
    @sprites["pageIcon"].setBitmap("Graphics/Pictures/QuestUI/pageIcon")
    @quests = [
      $PokemonGlobal.quests.active_quests,
      $PokemonGlobal.quests.completed_quests
    ]
    @quests_text = ["Pendientes", "Completados"]
    if SHOW_FAILED_QUESTS
      @quests.push($PokemonGlobal.quests.failed_quests)
      @quests_text.push("Failed")
    end


    @current_quest = 0
    @sprites["itemlist"] = Window_Quest.new(22,28,Graphics.width-22,Graphics.height-80,@viewport)
    @sprites["itemlist"].index = 0
    @sprites["itemlist"].baseColor = Color.new(249,249,249)
    @sprites["itemlist"].shadowColor = Color.new(22,22,22)
    @sprites["itemlist"].quests = @quests[@current_quest]
    @sprites["overlay1"] = BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    #pbSetSystemFont(@sprites["overlay1"].bitmap)
    pbSetFont(@sprites["overlay1"].bitmap,"Bebas Neue",28)
    @sprites["overlay2"] = BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["overlay2"].opacity = 0
    #pbSetSystemFont(@sprites["overlay2"].bitmap)
    pbSetFont(@sprites["overlay2"].bitmap,"Barlow Condensed",26)
    @sprites["overlay3"] = BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["overlay3"].opacity = 0
    #pbSetSystemFont(@sprites["overlay3"].bitmap)
    pbSetFont(@sprites["overlay3"].bitmap,"Barlow Condensed",26)
    @sprites["overlay_control"] = BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    pbSetSystemFont(@sprites["overlay_control"].bitmap)
    pbSetFont(@sprites["overlay_control"].bitmap,"Barlow Condensed",26)
    pbDrawTextPositions(@sprites["overlay1"].bitmap,[
      [_INTL("Objetivos {1}", @quests_text[@current_quest]),6,-2,0,Color.new(248,248,248),Color.new(0,0,0),true]
    ])
    #drawFormattedTextEx(@sprites["overlay_control"].bitmap,38,316,
      #436,"<c2=#{colorQuest("red")}>ARROWS:</c2> Navigate",@base,@shadow)
    #drawFormattedTextEx(@sprites["overlay_control"].bitmap,38,348,
      #436,"<c2=#{colorQuest("red")}>A/S:</c2> Jump Down/Up",@base,@shadow)
    #drawFormattedTextEx(@sprites["overlay_control"].bitmap,326,316,
      #436,"<c2=#{colorQuest("red")}>New Activity:</c2>",@base,@shadow)
    #pbDrawImagePositions(@sprites["overlay_control"].bitmap,[
      #[sprintf("Graphics/Pictures/QuestUI/new"),464,314]
    #])
    pbFadeInAndShow(@sprites) { pbUpdate }
  end

  def pbScene
    loop do
      selected = @sprites["itemlist"].index
      @sprites["itemlist"].active = true
      dorefresh = false
      Graphics.update
      Input.update
      pbUpdate
      if Input.trigger?(Input::BACK)
        pbPlayCloseMenuSE
        break
      elsif Input.trigger?(Input::USE)
        if @quests[@current_quest].length==0
          pbPlayBuzzerSE
        else
          pbPlayDecisionSE
          fadeContent
          @sprites["itemlist"].active = false
          pbQuest(@quests[@current_quest][selected])
          showContent
        end
      elsif Input.trigger?(Input::RIGHT)
        pbPlayCursorSE
        @current_quest +=1; @current_quest = 0 if @current_quest > @quests.length-1
        dorefresh = true
      elsif Input.trigger?(Input::LEFT)
        pbPlayCursorSE
        @current_quest -=1; @current_quest = @quests.length-1 if @current_quest < 0
        dorefresh = true
      end
      swapQuestType if dorefresh
    end
  end
  
  def swapQuestType
    @sprites["overlay1"].bitmap.clear
    @sprites["itemlist"].index = 0 # Resets cursor position
    @sprites["itemlist"].quests = @quests[@current_quest]
    @sprites["pageIcon"].x = @sprites["page_icon1"].x + 32*@current_quest
    pbDrawTextPositions(@sprites["overlay1"].bitmap,[
      [_INTL("Objetivos {1}", @quests_text[@current_quest]),6,-2,0,Color.new(248,248,248),Color.new(0,0,0),true]
    ])
  end
  
  def fadeContent
    15.times do
      Graphics.update
      @sprites["itemlist"].contents_opacity -= 17
      @sprites["overlay1"].opacity -= 17; @sprites["overlay_control"].opacity -= 17
      @sprites["page_icon1"].opacity -= 17; @sprites["pageIcon"].opacity -= 17
     
    end
  end
  
  def showContent
    15.times do
      Graphics.update
      @sprites["itemlist"].contents_opacity += 17
      @sprites["overlay1"].opacity += 17; @sprites["overlay_control"].opacity += 17
      @sprites["page_icon1"].opacity += 17; @sprites["pageIcon"].opacity += 17
    end
  end

def pbEndChars
   @sprites["PSS"].dispose
   @sprites["Char"].dispose
end
  
  def pbQuest(quest)
    @sprites["base2"].opacity = 255
         # Quest graphics
    questSprite = $quest_data.getQuestSprite(quest.id,quest.stage)
    questChar = $quest_data.getQuestnpcchar(quest.id,quest.stage)
    @sprites["PSS"] = IconSprite.new(0, 0, @viewport)
    @sprites["PSS"].setBitmap("Graphics/Characters/PSS/#{questSprite}")
    @sprites["PSS"].x = 250
    @sprites["PSS"].y = 25
    @sprites["PSS"].src_rect.height = (@sprites["PSS"].bitmap.height / 4).round
    @sprites["PSS"].src_rect.width = (@sprites["PSS"].bitmap.width / 4).round
    @sprites["PSS"].opacity = 255


    @sprites["Char"] = TrainerWalkingCharSprite.new(questChar,@viewport)
    charwidth  = @sprites["Char"].bitmap.width
    charheight = @sprites["Char"].bitmap.height
    @sprites["Char"].x        = 86*2-charwidth/8
    @sprites["Char"].y        = 56*2-charheight/8
    @sprites["Char"].src_rect = Rect.new(0,0,charwidth/4,charheight/4)


    #@sprites["Char"].setBitmap("Graphics/Characters/#{questChar}")
    @sprites["Char"].x = 397
    @sprites["Char"].y = 87
    #@sprites["Char"].src_rect.height = (@sprites["Char"].bitmap.height / 4).round
    #@sprites["Char"].src_rect.width = (@sprites["Char"].bitmap.width / 4).round
    @sprites["Char"].opacity = 255


    quest.new = false
    drawQuestDesc(quest)
    15.times do
      Graphics.update
      @sprites["overlay2"].opacity += 17; @sprites["overlay3"].opacity += 17; @sprites["page_icon2"].opacity += 17
    end
    page = 1
    loop do
      Graphics.update
      Input.update
      pbUpdate
      showOtherInfo = false
      if Input.trigger?(Input::RIGHT) && page==1
        pbPlayCursorSE
        page += 1
        @sprites["page_icon2"].mirror = true
        drawOtherInfo(quest)
      elsif Input.trigger?(Input::LEFT) && page==2
        pbPlayCursorSE
        page -= 1
        @sprites["page_icon2"].mirror = false
        drawQuestDesc(quest)
      elsif Input.trigger?(Input::BACK)
        @sprites["PSS"].opacity = 0
        @sprites["Char"].opacity = 0
        @sprites["base2"].opacity = 0
        pbPlayCloseMenuSE
        break
      end
    end
    15.times do
      Graphics.update
      @sprites["overlay2"].opacity -= 17; @sprites["overlay3"].opacity -= 17; @sprites["page_icon2"].opacity -= 17
    end
    @sprites["page_icon2"].mirror = false
    @sprites["itemlist"].refresh
  end
  
  def drawQuestDesc(quest)
    @sprites["overlay2"].bitmap.clear; @sprites["overlay3"].bitmap.clear

    # Quest name
    questName = $quest_data.getName(quest.id)
    pbDrawTextPositions(@sprites["overlay2"].bitmap,[
      ["#{questName}",6,-2,0,Color.new(248,248,248),Color.new(0,0,0),true]
    ])
    # Quest description
    questDesc = "<c2=#{colorQuest("info")}>INFORMACIÓN</c2> #{$quest_data.getQuestDescription(quest.id,quest.stage)}"
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,188,
      436,questDesc,@base,@shadow)
    # Stage description
    questStageDesc = $quest_data.getStageDescription(quest.id,quest.stage)
    # Stage location
    questStageLocation = $quest_data.getStageLocation(quest.id,quest.stage)
    # Quest reward
    questUrgency = $quest_data.getQuestUrgency(quest.id)
    # If 'nil' or missing, set to '???'
    if questStageLocation=="nil" || questStageLocation==""
      questStageLocation = "(Desconocida)"
    end
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,118,
      436,"<c2=#{colorQuest("info")}>TIPO</c2> #{questUrgency}",@base,@shadow)
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,Graphics.height-58,
      436,"<c2=#{colorQuest("info")}>OBJETIVO</c2> #{questStageDesc}",@base,@shadow)
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,153,
      436,"<c2=#{colorQuest("info")}>LUGAR</c2> #{questStageLocation}",@base,@shadow)
  end

  def drawOtherInfo(quest)
    @sprites["overlay3"].bitmap.clear
    # Guest giver
    questGiver = $quest_data.getQuestGiver(quest.id,quest.stage)
    # If 'nil' or missing, set to '???'
    if questGiver=="nil" || questGiver==""
      questGiver = "(Desconocido)"
    end
    # Total number of stages for quest
    questLength = $quest_data.getMaxStagesForQuest(quest.id)
    # Map quest was originally started
    originalMap = quest.location
    # Vary text according to map name
    loc = originalMap.include?("Route") ? "on" : "in"
    # Format time
    timenow = Time.now()
    month=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"][timenow.mon-1]
    #time = quest.time.strftime("%B %d %Y %H:%M")
    time = quest.time.strftime("%d %Y %H:%M")
    if getActiveQuests.include?(quest.id)
      time_text = "start"
    elsif getCompletedQuests.include?(quest.id)
      time_text = "completion"
    else
      time_text = "failure"
    end
    # Quest reward
    questReward = $quest_data.getQuestReward(quest.id)
    if questReward=="nil" || questReward==""
      questReward = "???"
    end
    textpos = [
      [sprintf("PARTE %d / %d",quest.stage,questLength),38,108,0,@base,@shadow],
      ["#{questGiver}",38,249,0,@base,@shadow],
      ["#{originalMap}",38,177,0,@base,@shadow],
      ["#{month}",268,249,0,@base,@shadow],
      ["#{time}",268,269,0,@base,@shadow]
    ]
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,227,
      436,"<c2=#{colorQuest("info")}>RECIBIDA DE</c2>",@base,@shadow)
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,155,
      436,"<c2=#{colorQuest("info")}>INICIADA EN</c2>",@base,@shadow)
    drawFormattedTextEx(@sprites["overlay3"].bitmap,268,227,
      436,"<c2=#{colorQuest("info")}>FECHA DE INICIO</c2>",@base,@shadow)
    drawFormattedTextEx(@sprites["overlay3"].bitmap,38,Graphics.height-58,
      436,"<c2=#{colorQuest("info")}>RECOMPENSA</c2> #{questReward}",@base,@shadow)
    pbDrawTextPositions(@sprites["overlay3"].bitmap,textpos)
  end

  def pbEndScene
    pbFadeOutAndHide(@sprites) { pbUpdate }
    pbDisposeSpriteHash(@sprites)
    @viewport.dispose
  end
end

#===============================================================================
# Class to call UI
#===============================================================================
class QuestList_Screen
  def initialize(scene)
    @scene = scene
  end

  def pbStartScreen
    @scene.pbStartScene
    @scene.pbScene
    @scene.pbEndScene
  end
end

# Utility method for calling UI
def pbViewQuests
  scene = QuestList_Scene.new
  screen = QuestList_Screen.new(scene)
  screen.pbStartScreen
end
