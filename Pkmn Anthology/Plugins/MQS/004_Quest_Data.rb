module QuestModule
  
  # You don't actually need to add any information, but the respective fields in the UI will be blank or "???"
  # I included this here mostly as an example of what not to do, but also to show it's a thing that exists
  Quest0 = {
  
  }
  
  # Here's the simplest example of a single-stage quest with everything specified
 Quest1 = {
    :ID => "1",
    :Name => "¡Pokemón! ¡Yo te elijo!",
    :QuestGiver1 => "Profesor Oak",
    :Urgency => "General",
    :Sprite1 => "PSSOAK",
    :NPCchar1 => "PROFESOR OAK",
    :Stage1 => "Ve al laboratorio y consigue un Pokemón.",
    :Location1 => "Pueblo Paleta",
    :QuestDescription1 => "¡No es bueno ir por ahí solo! Como todos los años, el PROFESOR OAK ha llamado a todos los jóvenes entrenadores a su laboratorio para conseguir su primer POKEMÓN.",
    :RewardString => "¡Tu primer Pokemón!"
  }
  
  Quest2 = {
    :ID => "2",
    :Name => "Dejando el Nido",
    :QuestGiver1 => "Profesor Oak",
    :Urgency => "General",
    :Sprite1 => "PSSOAK",
    :NPCchar1 => "PROFESOR OAK",
    :Stage1 => "Avanza hacia Ciudad Verde",
    :Location1 => "Ciudad Verde",
    :QuestDescription1 => "¡Finalmente tienes a tu primer POKEMÓN! Ya es hora de recorrer el mundo y enfrentar los desafíos que te esperan. ¡El primer paso es dejar tu pueblo natal e iniciar tu viaje!",
  }


  Quest3 = {
    :ID => "3",
    :Name => "Emergencia Pokemón",
    :QuestGiver1 => "Misty",
    :Urgency => "Urgente",
    :Sprite1 => "PSSMISTY",
    :NPCchar1 => "MISTY",
    :Stage1 => "Lleva a Pikachu al Centro Pokemón",
    :Location1 => "Ciudad Verde",
    :QuestDescription1 => "PIKACHU no va a poder con todos esos SPEAROW furiosos de la RUTA 01. ¡Llévalo de inmediato a un CENTRO POKEMÓN y asegúrate de que esté sano y salvo!",
  }

  Quest4 = {
    :ID => "4",
    :Name => "Entrega Inmediata",
    :QuestGiver1 => "Vendedor",
    :Urgency => "Necesaria",
    :Sprite1 => "PSSVENDEDOR",
    :NPCchar1 => "BW070",
    :Stage1 => "Entrega el Paquete al Profesor Oak",
    :Location1 => "Pueblo Paleta",
    :QuestDescription1 => "El encargado de la tienda te ha pedido llevar un paquete al PROFESOR OAK. No es asunto tuyo saber lo que lleva, pero es importante que llegue a salvo.",
  }

  Quest5 = {
    :ID => "5",
    :Name => "El Bosque Verde",
    :QuestGiver1 => "Enfermera Joy",
    :Urgency => "General",
    :Sprite1 => "PSSJOY",
    :NPCchar1 => "JOY",
    :Stage1 => "Cruza el Bosque Verde",
    :Location1 => "Bosque Verde",
    :QuestDescription1 => "CIUDAD PLATEADA tiene el GIMNASIO POKEMÓN más cercano. Pero para llegar hasta allá debes cruzar por el BOSQUE VERDE, al norte de CIUDAD VERDE.",
  }

  Quest6 = {
    :ID => "6",
    :Name => "El Desafío del Samurai",
    :QuestGiver1 => "Chico",
    :Urgency => "Opcional",
    :Sprite1 => "PSSCHICO",
    :NPCchar1 => "BW004",
    :Stage1 => "Derrota al Samurai Cazabichos",
    :Location1 => "Bosque Verde",
    :QuestDescription1 => "Un muchacho vestido de forma extravagante, busca a los entrenadores más fuertes del BOSQUE VERDE. ¿Crees poder con él?",
    :RewardString => "Superpoción"
  }

  Quest7 = {
    :ID => "7",
    :Name => "Combate en Ciudad Plateada",
    :QuestGiver1 => "Flint",
    :Urgency => "General",
    :Sprite1 => "PSSFLINT",
    :NPCchar1 => "BROCKDAD",
    :Stage1 => "Derrota al Líder BROCK.",
    :Location1 => "Gimnasio de Ciudad Plateada",
    :QuestDescription1 => "BROCK es el nombre del LÍDER DE GIMNASIO de CIUDAD PLATEADA. Hasta ahora pocos se atreven a desafiarlo, pero tiene una medalla que podría ser tuya.",
    :RewardString => "Medalla Roca"
  }

  Quest8 = {
    :ID => "8",
    :Name => "Los Misterios del Mt.Luna",
    :QuestGiver1 => "Brock",
    :Urgency => "General",
    :Sprite1 => "PSSBROCK",
    :NPCchar1 => "BROCKsm",
    :Stage1 => "Cruza el Mt.Luna",
    :Location1 => "Mt.Luna",
    :QuestDescription1 => "Finalmente triunfaste en tu 1er GIMNASIO. Pero faltan 7 medallas y el siguiente espera. ¡Es momento de cruzar el MT.LUNA para llegar a CIUDAD CELESTE!",
  }

    Quest9 = {
    :ID => "9",
    :Name => "Hora de elegir un bando",
    :QuestGiver1 => "Profesor Oak",
    :QuestGiver2 => "Profesor Willow",
    :Urgency => "Opcional",
    :Sprite1 => "PSSOAK",
    :Sprite2 => "PSSWILLOW",
    :NPCchar1 => "PROFESOR OAK",
    :NPCchar2 => "WILLOW",
    :Stage1 => "Ve a tu reunión en el Museo de la Ciencia",
    :Stage2 => "Encuentra al Asistente del Profesor Willow",
    :Location1 => "Museo de la Ciencia",
    :Location2 => "Ciudad Celeste",
    :QuestDescription1 => "Un viejo colega del Profesor Oak quiere conocerte un poco. Te está esperando en el Museo de la Ciencia de CIUDAD PLATEADA.",
    :QuestDescription2 => "Tras conocerte, el Profesor Willow te ha invitado a encontrarte con uno de sus asistentes en la cafetería de CIUDAD CELESTE.",
  }

  Quest10 = {
    :ID => "10",
    :Name => "Clefairy y la Piedra Lunar",
    :QuestGiver1 => "Seymour",
    :Urgency => "Opcional",
    :Sprite1 => "PSSSEYMOUR",
    :NPCchar1 => "SEYMUR",
    :Stage1 => "Apaga todas luces dentro del Mt.Luna",
    :Location1 => "Mt.Luna",
    :QuestDescription1 => "SEYMOUR está investigando la PIEDRA LUNAR y a los Pokemón de Mt.Luna, pero buscadores de fósiles están confundiéndolos con sus luces.",
    :RewardString => "Piedra Lunar"
  }

  Quest11 = {
    :ID => "11",
    :Name => "Cosas de Fósiles",
    :QuestGiver1 => "Supernerd Miguel",
    :Urgency => "Opcional",
    :Sprite1 => "PSSSUPERNERD",
    :NPCchar1 => "trcharCollector",
    :Stage1 => "Visita el laboratorio de Isla Canela",
    :Location1 => "Isla Canela",
    :QuestDescription1 => "Se dice que muy lejos, en ISLA CANELA, hay un laboratorio donde se estudia cómo regenerar POKEMÓN a partir de fósiles antiguos.",
  }
  
  # Here's an example of a quest with lots of stages that also doesn't have a stage location defined for every stage
  Quest54 = {
    :ID => "3",
    :Name => "Last-minute chores",
    :QuestGiver => "Grandma",
    :Stage1 => "A",
    :Stage2 => "B",
    :Stage3 => "C",
    :Stage4 => "D",
    :Stage5 => "E",
    :Stage6 => "F",
    :Stage7 => "G",
    :Stage8 => "H",
    :Stage9 => "I",
    :Stage10 => "J",
    :Stage11 => "K",
    :Stage12 => "L",
    :Location1 => "nil",
    :Location2 => "nil",
    :Location3 => "Dewford Town",
    :QuestDescription => "Isn't the alphabet longer than this?",
    :RewardString => "Chicken soup!"
  }
  
  # Here's an example of not defining the quest giver and reward text
  Quest55 = {
    :ID => "4",
    :Name => "A new beginning",
    :QuestGiver => "nil",
    :Stage1 => "Turning over a new leaf... literally!",
    :Stage2 => "Help your neighbours.",
    :Location1 => "Milky Way",
    :Location2 => "nil",
    :QuestDescription => "You crash landed on an alien planet. There are other humans here and they look hungry...",
    :RewardString => "nil"
  }
  
  # Other random examples you can look at if you want to fill out the UI and check out the page scrolling
  Quest56 = {
    :ID => "5",
    :Name => "All of my friends",
    :QuestGiver => "Barry",
    :Stage1 => "Meet your friends near Acuity Lake.",
    :QuestDescription => "Barry told me that he saw something cool at Acuity Lake and that I should go see. I hope it's not another trick.",
    :RewardString => "You win nothing for giving in to peer pressure."
  }
  
  Quest57 = {
    :ID => "6",
    :Name => "The journey begins",
    :QuestGiver => "Professor Oak",
    :Stage1 => "Deliver the parcel to the Pokémon Mart in Viridian City.",
    :Stage2 => "Return to the Professor.",
    :Location1 => "Viridian City",
    :Location2 => "nil",
    :QuestDescription => "The Professor has entrusted me with an important delivery for the Viridian City Pokémon Mart. This is my first task, best not mess it up!",
    :RewardString => "nil"
  }
  
  Quest58 = {
    :ID => "7",
    :Name => "Close encounters of the... first kind?",
    :QuestGiver => "nil",
    :Stage1 => "Make contact with the strange creatures.",
    :Location1 => "Rock Tunnel",
    :QuestDescription => "A sudden burst of light, and then...! What are you?",
    :RewardString => "A possible probing."
  }
  
  Quest59 = {
    :ID => "8",
    :Name => "These boots were made for walking",
    :QuestGiver => "Musician #1",
    :Stage1 => "Listen to the musician's, uhh, music.",
    :Stage2 => "Find the source of the power outage.",
    :Location1 => "nil",
    :Location2 => "Celadon City Sewers",
    :QuestDescription => "A musician was feeling down because he thinks no one likes his music. I should help him drum up some business."
  }
  
  Quest69 = {
    :ID => "9",
    :Name => "Got any grapes?",
    :QuestGiver => "Duck",
    :Stage1 => "Listen to The Duck Song.",
    :Stage2 => "Try not to sing it all day.",
    :Location1 => "YouTube",
    :QuestDescription => "Let's try to revive old memes by listening to this funny song about a duck wanting grapes.",
    :RewardString => "A loss of braincells. Hurray!"
  }
  
  Quest80 = {
    :ID => "10",
    :Name => "Singing in the rain",
    :QuestGiver => "Some old dude",
    :Stage1 => "I've run out of things to write.",
    :Stage2 => "If you're reading this, I hope you have a great day!",
    :Location1 => "Somewhere prone to rain?",
    :QuestDescription => "Whatever you want it to be.",
    :RewardString => "Wet clothes."
  }
  
  Quest81 = {
    :ID => "11",
    :Name => "When is this list going to end?",
    :QuestGiver => "Me",
    :Stage1 => "When IS this list going to end?",
    :Stage2 => "123",
    :Stage3 => "456",
    :Stage4 => "789",
    :QuestDescription => "I'm losing my sanity.",
    :RewardString => "nil"
  }
  
  Quest82 = {
    :ID => "12",
    :Name => "The laaast melon",
    :QuestGiver => "Some stupid dodo",
    :Stage1 => "Fight for the last of the food.",
    :Stage2 => "Don't die.",
    :Location1 => "A volcano/cliff thing?",
    :Location2 => "Good advice for life.",
    :QuestDescription => "Tea and biscuits, anyone?",
    :RewardString => "Food, glorious food!"
  }

end
