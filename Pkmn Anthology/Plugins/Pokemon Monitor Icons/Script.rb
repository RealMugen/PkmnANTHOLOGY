# Pokemon monitor icons
# Credit: Ulithium_Dragon, bo4p5687
#
# Call: MonitorIcons.show
# If you want to change coordinate of bitmap, call like this MonitorIcons.show(x', y')
# 	-> x', y' are numbers that have equation: x (real) = x (recent) + x'; y (real) = y (recent) + y'
# You can change zoom with this method -> MonitorIcons.show(x', y', zoom)
# If you want to change zoom but you don't want to change x and y, just call MonitorIcons.show(0, 0, zoom)
# It uses icon file of PE

module MonitorIcons

	class Show
		def self.icons(position, x, y, zoom)
			pkmn = $Trainer.party[position-1]
			species = pkmn.species
			species = GameData::Species.get(species).species
			bmpkmn  = GameData::Species.icon_bitmapPC(species, pkmn.form, pkmn.gender, pkmn.shiny?, pkmn.shadowPokemon?)
			realw   = bmpkmn.width / 2
			realh   = bmpkmn.height
			viewport   = Viewport.new(0, 0, Graphics.width, Graphics.height)
      viewport.z = 99999
			@sprite    = Sprite.new(viewport)
			@sprite.bitmap = bmpkmn
			@sprite.src_rect.width = realw
			@sprite.ox = @sprite.src_rect.width / 2
			@sprite.oy = @sprite.src_rect.height / 2
			@sprite.x  = x
			@sprite.y  = y
			@sprite.zoom_x = @sprite.zoom_y = zoom
		end

		def self.finish
			@sprite.dispose
			@sprite = nil
		end
	end

	def self.show(addx=0, addy=0, zoom=1)
		count = $Trainer.pokemon_count
		for i in 1..count
			pbSet(6, i)
			# Add
			event = pbMapInterpreter.get_self
			x = event.screen_x
			y = event.screen_y
			s = Show
			s.icons(i, x+addx, y+addy, zoom)
			pbSEPlay("Battle ball shake")
			pbWait(16)
			# Add
			s.finish
		end
	end

end