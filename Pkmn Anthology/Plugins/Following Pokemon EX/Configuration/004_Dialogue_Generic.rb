#-------------------------------------------------------------------------------
# These are used to define what the Follower will say when spoken to in general
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Generic Item Dialogue
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |_pkmn, _random_val|
  items = [
    :POTION,        :SUPERPOTION,  :GROWTHMULCH,    :REVIVE,        :PPUP,
    :PPMAX,         :RARECANDY,    :REPEL,          :MAXREPEL,      :ESCAPEROPE,
    :HONEY,         :TINYMUSHROOM, :PEARL,          :NUGGET,        :GREATBALL,
    :DAMPMULCH,     :STABLEMULCH,  :GOOEYMULCH,       :BOLADEARROZ,      :RAZZBERRY,
    :REDAPRICORN,   :BLUEAPRICORN, :YELLOWAPRICORN, :GREENAPRICORN, :PINKAPRICORN,
    :BLACKAPRICORN, :WHITEAPRICORN, :BLUKBERRY,      :NANABBERRY,    :WEPEARBERRY,
    :PINAPBERRY,    :CORNNBERRY,   :MAGOSTBERRY,     :RABUTABERRY,  :NOMELBERRY,
    :SPELONBERRY,   :PAMTREBERRY, :WATMELBERRY,      :DURINBERRY,   :BELUEBERRY,
    :POKOCHO,       :CHOKOPOKOCHO,   :BAYAPOKOCHO,   :LATTEPOKOCHO,  :LENA,
    :RAMITAS,       :YESCA,       :BOTELLA,          :ALMUERZO,      :PIKACOLA,
    :GRANODECAFE,   :NIKUMAN,     :MAZORCA,          :HONGOHISUI,    :SUSPIRO, 
    :PIMIENTO,      :BARRACHOCO,  :CARAMELOTRAINER,  :MANZANA,       :KISSAKIBERRIES,
    :YUKINOJO,      :PANECILLOS,  :HONGOSECRUTEAK , :RAZZBERRY, :SILVERRAZZBERRY, :GOLDENRAZZBERRY,
    :NANABBERRY, :SILVERNANABBERRY, :GOLDENNANABBERRY, :PINAPBERRY, :SILVERPINAPBERRY, :GOLDENPINAPBERRY
  ]
 
  # If no message or quantity is specified the default message is used and the quantity of item is 1
  next true if FollowingPkmn.item(items.sample)
}
#-------------------------------------------------------------------------------
# All dialogues with the Music Note animation
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, random_val|
  if random_val == 0
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
     if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> parece querer jugar con {2}."),
      _INTL("<b>{1}</b> está cantando y tarareando."),
      _INTL("<b>{1}</b> está mirando a {2} con una expresión de felicidad."),
      _INTL("<b>{1}</b> se balancea y baila a su antojo."),
      _INTL("¡<b>{1}</b> está saltando de forma despreocupada!"),
      _INTL("¡<b>{1}</b> está mostrando su agilidad!"),
      _INTL("¡<b>{1}</b> se mueve felizmente!"),
      _INTL("¡Whoa! ¡<b>{1}</b> de repente empezó a bailar de felicidad!"),
      _INTL("¡<b>{1}</b> se mantiene al día con {2}!"),
      _INTL("¡<b>{1}</b> es feliz saltando de un lado a otro."),
      _INTL("<b>{1}</b> está mordisqueando juguetonamente el suelo."),
      _INTL("¡<b>{1}</b> está jugando a picar los pies de {2}!"),
      _INTL("¡<b>{1}</b> está siguiendo a {2} muy de cerca!"),
      _INTL("<b>{1}</b> se da la vuelta y mira a {2}."),
      _INTL("¡<b>{1}</b> está trabajando duro para mostrar su poder!"),
      _INTL("¡<b>{1}</b> parece que quiere correr por ahí!"),
      _INTL("<b>{1}</b> se pasea disfrutando del paisaje."),
      _INTL("¡<b>{1}</b> parece estar disfrutando de esto un poco!"),
      _INTL("¡<b>{1}</b> está alegre!"),
      _INTL("¿Parece que <b>{1}</b> está cantando algo?"),
      _INTL("¡<b>{1}</b> está bailando felizmente!"),
      _INTL("¡<b>{1}</b> se divierte bailando una animada canción!"),
      _INTL("¡<b>{1}</b> es tan feliz que se puso a cantar!"),
      _INTL("¡<b>{1}</b> miró hacia arriba y aulló!"),
      _INTL("<b>{1}</b> parece sentirse optimista."),
      _INTL("¡Parece que <b>{1}</b> tiene ganas de bailar!"),
      _INTL("¡De repente <b>{1}</b> empezó a cantar! Parece que se siente muy bien."),
      _INTL("¡Parece que <b>{1}</b> quiere bailar con {2}!"),
      _INTL("<b>{1}</b> está halando el cesped."),
      _INTL("¡Ok! ¡Demasiada azúcar para <b>{1}</b>!"),
      _INTL("<b>{1}</b> hace sus pasos de baile..."),
      _INTL("¡<b>{1}</b> hace gala de su agilidad!"),
      _INTL("¡<b>{1}</b> se mueve sin preocupaciones!"),
      _INTL("¡Whoa! ¡Derrepente, <b>{1}</b> se pone a bailar de felicidad!"),
      _INTL("Pareciera que <b>{1}</b> está ensayando un baile..."),
      _INTL("¡<b>{1}</b> sigue a {2} muy de cerca!"),
      _INTL("¡<b>{1}</b> se está esforzando en mostrar su poder!"),
      _INTL("¡Pareciera que <b>{1}</b> quiere ir y corretear!"),
      _INTL("¡Wha! ¡Derrepente <b>{1}</b> baila de alegría!"),
      _INTL("<b>{1}</b> vaga alrededor y disfruta el paisaje."),
      _INTL("¡Parece que <b>{1}</b> está disfrutando esto un poquito!"),
      _INTL("¡Baila <b>{1}</b>, baila!"),
      _INTL("¡<b>{1}</b> se divierte bailando (algo torpemente)!"),
      _INTL("¡Hoy el optimismo es el segundo nombre de <b>{1}</b>!"),
      _INTL("¡<b>{1}</b> no puede ocultar su ritmo!"),
      _INTL("¡Canta <b>{1}</b>, canta!"),
      _INTL("¡No no! ¡<b>{1}</b>, yo no se bailar!")
    ]
    value = rand(messages.length)
    case value
    # Special move route to go along with some of the dialogue
    when 3, 9
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 65])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0
      ])
    when 4, 5
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 40])
      FollowingPkmn.move_route([
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0
      ])
    when 6, 17
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 40])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp
      ])
    when 7, 28
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 60])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0
      ])
    when 21, 22
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 50])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0
      ])
    end
    pbMessage(_INTL(messages[value], pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# All dialogues with the Angry animation
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, random_val|
  if random_val == 1
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaBattle
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("¡<b>{1}</b> dejar salir un rugido!"),
      _INTL("¡<b>{1}</b> está poniendo cara de enfado!"),
      _INTL("¡<b>{1}</b> parece estar enfadado por alguna razón!"),
      _INTL("<b>{1}</b> mordió los pies de {2}."),
      _INTL("<b>{1}</b> se giró para mirar hacia el otro lado, mostrando una expresión desafiante."),
      _INTL("¡<b>{1}</b> está tratando de intimidar a los enemigos de {2}!"),
      _INTL("¡<b>{1}</b> quiere buscar pelea!"),
      _INTL("¡<b>{1}</b> está listo para luchar!"),
      _INTL("¡Parece que <b>{1}</b> pelearía con cualquiera en este momento!"),
      _INTL("<b>{1}</b> está gruñendo de una manera que suena casi como un discurso..."),
      _INTL("¡Parece que <b>{1}</b> pelearía con cualquiera en este momento!"),
      _INTL("¡<b>{1}</b> dejó salir un rugido!"),
      _INTL("¡<b>{1}</b> tiene una gran cara de enojo!"),
      _INTL("<b>{1}</b> parece estar molesto por alguna razón."),
      _INTL("<b>{1}</b> está tratando de intimidarte."),
      _INTL("<b>{1}</b> gira el rostro y muestra una expresión desafiante..."),
      _INTL("¡<b>{1}</b> está rugiendo molesto!"),
      _INTL("¡Se ve que <b>{1}</b> quiere combatir con quien se le cruce!")
    ]
    value = rand(messages.length)
    # Special move route to go along with some of the dialogue
    case value
    when 6, 7, 8
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 25])
      FollowingPkmn.move_route([
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0
      ])
    end
    pbMessage(_INTL(messages[value], pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# All dialogues with the Neutral Animation
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, random_val|
  if random_val == 2
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
      if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está mirando hacia abajo con firmeza."),
      _INTL("<b>{1}</b> está husmeando."),
      _INTL("<b>{1}</b> se concentra profundamente."),
      _INTL("<b>{1}</b> mira a {2} y asiente."),
      _INTL("<b>{1}</b> está mirando directamente a los ojos de {2}."),
      _INTL("<b>{1}</b> está inspeccionando la zona."),
      _INTL("¡<b>{1}</b> está enfocado con una mirada aguda!"),
      _INTL("<b>{1}</b> mira distraídamente a su alrededor."),
      _INTL("¡<b>{1}</b> bostezó muy fuerte!"),
      _INTL("<b>{1}</b> se relaja cómodamente."),
      _INTL("<b>{1}</b> centra su atención en {2}."),
      _INTL("<b>{1}</b> está mirando fijamente a la nada."),
      _INTL("<b>{1}</b> se está concentrando."),
      _INTL("<b>{1}</b> mira {2} y lo contempla."),
      _INTL("<b>{1}</b> está mirando las huellas de {2}."),
      _INTL("<b>{1}</b> parece querer jugar y está mirando a {2} expectante."),
      _INTL("<b>{1}</b> parece estar pensando profundamente en algo."),
      _INTL("<b>{1}</b> no está prestando atención a {2}...Parece que está pensando en otra cosa."),
      _INTL("<b>{1}</b> parece sentirse cómodo con su seriedad."),
      _INTL("<b>{1}</b> parece desinteresado."),
      _INTL("La mente de <b>{1}</b> parece estar en otra parte."),
      _INTL("<b>{1}</b> parece estar observando el entorno en lugar de mirar a {2}."),
      _INTL("<b>{1}</b> parece aburrido."),
      _INTL("<b>{1}</b> tiene una mirada intensa."),
      _INTL("<b>{1}</b> está mirando en la distancia."),
      _INTL("<b>{1}</b> parece estar examinando cuidadosamente la cara de {2}."),
      _INTL("<b>{1}</b> parece estar tratando de comunicarse con sus ojos."),
      _INTL("... <b>{1}</b> parece haber estornudado!"),
      _INTL("... <b>{1}</b> se dio cuenta de que los zapatos de {2} están algo sucios."),
      _INTL("Parece que <b>{1}</b> comió algo extraño, está poniendo una cara rara... "),
      _INTL("<b>{1}</b> parece oler algo bueno."),
      _INTL("<b>{1}</b> se dio cuenta de que la mochila de {2} necesita sacudirse el polvo..."),
      _INTL("...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ...... ¡<b>{1}</b> asintió en silencio!"),
      _INTL("<b>{1}</b> te anima a seguir adelante."),
      _INTL("<b>{1}</b> se concentra profundamente."),
      _INTL("¡<b>{1}</b> bostezó muy fuerte!"),
      _INTL("Hoy, para <b>{1}</b> es un día de relajo."),
      _INTL("<b>{1}</b> está durmiéndose mientras camina."),
      _INTL("<b>{1}</b> juega intentando esconderse de {2}."),
      _INTL("Si me preguntas, <b>{1}</b> se ve pensando en algo muy profundo."),
      _INTL("<b>{1}</b> no le pone atención a {2}... Piensa en otra cosa."),
      _INTL("...Parece que... <b>{1}</b> se ha relajado demasiado."),
      _INTL("...")
    ]
    value = rand(messages.length)
    # Special move route to go along with some of the dialogue
    case value
    when 1, 5, 7, 20, 21
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 35])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnDown
      ])
    end
    pbMessage(_INTL(messages[value], pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# All dialogues with the Happy animation
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, random_val|
  if random_val == 3
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> comenzó a hurgar a {2}."),
      _INTL("<b>{1}</b> parece muy feliz."),
      _INTL("<b>{1}</b> se acurruca contento con {2}."),
      _INTL("<b>{1}</b> es tan feliz que no puede quedarse quieto."),
      _INTL("¡<b>{1}</b> parece que quiere liderar!"),
      _INTL("<b>{1}</b> viene felizmente."),
      _INTL("¡<b>{1}</b> parece que se siente muy bien al caminar con {2}!"),
      _INTL("<b>{1}</b> está resplandeciente de salud."),
      _INTL("<b>{1}</b> parece muy feliz."),
      _INTL("¡<b>{1}</b> hace un esfuerzo extra por {2}!"),
      _INTL("<b>{1}</b> está oliendo los aromas del aire circundante."),
      _INTL("¡<b>{1}</b> está saltando de alegría!"),
      _INTL("¡<b>{1}</b> sigue sintiéndose muy bien!"),
      _INTL("<b>{1}</b> estira su cuerpo y se relaja."),
      _INTL("<b>{1}</b> está haciendo todo lo posible para seguir a {2}."),
      _INTL("¡<b>{1}</b> se acurruca cerca de {2}!"),
      _INTL("¡<b>{1}</b> está lleno de energía!"),
      _INTL("¡<b>{1}</b> está tan contento que dan ganas de contagiarse!"),
      _INTL("<b>{1}</b> es pasear y escuchar los diferentes sonidos."),
      _INTL("<b>{1}</b> le da a {2} una mirada feliz y una sonrisa."),
      _INTL("¡<b>{1}</b> empezó a respirar bruscamente por la nariz por la excitación!"),
      _INTL("¡<b>{1}</b> está temblando de impaciencia!"),
      _INTL("<b>{1}</b> es tan feliz que empezó a rodar."),
      _INTL("<b>{1}</b> parece encantado de recibir la atención de {2}."),
      _INTL("¡<b>{1}</b> parece muy satisfecho de que {2} cuida de él!"),
      _INTL("¡<b>{1}</b> comenzó a retorcer todo el cuerpo con excitación!"),
      _INTL("¡Parece que <b>{1}</b> apenas puede evitar abrazar a {2}!"),
      _INTL("<b>{1}</b> juega a caminar sobre las huellas de {2}."),
      _INTL("<b>{1}</b> comenzó a hurgarte en el estómago."),
      _INTL("¡Parece que <b>{1}</b> quiere caminar delante!"),
      _INTL("¡Parece que una de las cosas favoritas de <b>{1}</b> es caminar junto a {2}!"),
      _INTL("<b>{1}</b> está radiante hoy."),
      _INTL("<b>{1}</b> se ve auténticamente feliz."),
      _INTL("¡<b>{1}</b> se sigue sintiendo genial!"),
      _INTL("¡<b>{1}</b> está totalmente a gusto!"),
      _INTL("<b>{1}</b> le regala una enorme sonrisa a {2}."),
      _INTL("¡<b>{1}</b> tiembla de entusiasmo!"),
      _INTL("<b>{1}</b> está tan contento que rueda y rueda."),
      _INTL("¡<b>{1}</b> sacude todo su cuerpo con excitación!"),
      _INTL("¡<b>{1}</b> adora estar fuera de la <b>Pokeball</b>!"),
      _INTL("<b>{1}</b> compite con los pies de <b>{2}</b> al caminar...")
    ]
    value = rand(messages.length)
    # Special move route to go along with some of the dialogue
    case value
    when 3
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 45])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0
      ])
    when 11, 16, 17, 24
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 40])
      FollowingPkmn.move_route([
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::Jump, 0, 0
      ])
    end
    pbMessage(_INTL(messages[value], pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# All dialogues with the Heart animation
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, random_val|
  if random_val == 4
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HEART)
      if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> de repente empezó a caminar más cerca de {2}."),
      _INTL("¡Woah! ¡<b>{1}</b> abraza de repente a {2}!"),
      _INTL("<b>{1}</b> se está frotando contra {2}."),
      _INTL("<b>{1}</b> se mantiene cerca de {2}."),
      _INTL("<b>{1}</b> está sonrojado."),
      _INTL("¡<b>{1}</b> le encanta pasar tiempo con {2}!"),
      _INTL("¡<b>{1}</b> es repentinamente juguetón!"),
      _INTL("¡<b>{1}</b> se frota contra las piernas de {2}!"),
      _INTL("¡<b>{1}</b> mira a {2} con gran afecto!"),
      _INTL("<b>{1}</b> parece querer algo de afecto por parte de {2}."),
      _INTL("<b>{1}</b> parece querer algo de atención por parte de {2}."),
      _INTL("<b>{1}</b> parece feliz viajando con {2}."),
      _INTL("<b>{1}</b> parece sentir afecto hacia {2}."),
      _INTL("<b>{1}</b> está mirando a {2} con ojos de amor."),
      _INTL("<b>{1}</b> parece que quiere un regalo de {2}."),
      _INTL("¡<b>{1}</b> parece que quiere que {2} lo acaricie!"),
      _INTL("<b>{1}</b> se frota contra {2} afeccionadamente."),
      _INTL("<b>{1}</b> golpea suavemente su cabeza contra la mano de {2}."),
      _INTL("<b>{1}</b> se da la vuelta y mira a {2} expectante."),
      _INTL("<b>{1}</b> está mirando a {2} con ojos de confianza."),
      _INTL("¡<b>{1}</b> parece que se hace de rogar {2} por un poco de afecto!"),
      _INTL("¡<b>{1}</b> está imitando a {2}!"),
      _INTL("Derrepente, <b>{1}</b> camina más cerca de ti."),
      _INTL("<b>{1}</b> se ruborizó."),
      _INTL("¡<b>{1}</b> se puso juguetón!"),
      _INTL("¡<b>{1}</b> mira a {2} con adoración!"),
      _INTL("<b>{1}</b> adora que {2} sea tan afectuoso con él."),
      _INTL("<b>{1}</b> nunca pensó que estaría así de feliz viajando con {2}."),
      _INTL("<b>{1}</b> se ve seguro mientras más camina con {2}."),
      _INTL("<b>{1}</b> quiere bromear con {2}."),
      _INTL("<b>{1}</b> tiene plena confianza en {2}."),
      _INTL("¡<b>{1}</b> quiere vivir aventuras en compañía de {2}."),
      _INTL("<b>{1}</b> está seguro de que junto a {2} ganarán el campeonato.")
    ]
    value = rand(messages.length)
    case value
    when 1, 6,
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 10])
      FollowingPkmn.move_route([
        PBMoveRoute::Jump, 0, 0
      ])
    end
    pbMessage(_INTL(messages[value], pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# All dialogues with no animation
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, random_val|
  if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
  if random_val == 5
    messages = [
      _INTL("¡<b>{1}</b> gira en un círculo!"),
      _INTL("<b>{1}</b> dejó escapar un grito de guerra."),
      _INTL("¡<b>{1}</b> está al acecho!"),
      _INTL("<b>{1}</b> está de pie pacientemente."),
      _INTL("<b>{1}</b> mira a su alrededor con inquietud."),
      _INTL("<b>{1}</b> está deambulando por ahí."),
      _INTL("¡<b>{1}</b> bostezó fuertemente!"),
      _INTL("<b>{1}</b> está constantemente hurgando en el suelo alrededor de los pies de {2}."),
      _INTL("<b>{1}</b> está mirando a {2} y sonríe."),
      _INTL("<b>{1}</b> está mirando fijamente en la distancia."),
      _INTL("<b>{1}</b> se mantiene al día con {2}."),
      _INTL("<b>{1}</b> parece satisfecho de sí mismo."),
      _INTL("¡<b>{1}</b> sigue siendo fuerte!"),
      _INTL("<b>{1}</b> camina al unísono de {2}."),
      _INTL("<b>{1}</b> está rascandose la piel."),
      _INTL("<b>{1}</b> mira a {2} con aprobación."),
      _INTL("<b>{1}</b> se cayó y parece un poco avergonzado."),
      _INTL("<b>{1}</b> está esperando a ver qué hace {2}."),
      _INTL("<b>{1}</b> intenta entender los planes de {2}."),
      _INTL("<b>{1}</b> está buscando en {2} algún tipo de señal."),
      _INTL("<b>{1}</b> se queda en su sitio, esperando que {2} decida hacia dónde ir."),
      _INTL("<b>{1}</b> se sentó obedientemente."),
      _INTL("¡<b>{1}</b> saltó de sorpresa!"),
      _INTL("¡<b>{1}</b> saltó un poco!"),
      _INTL("¡<b>{1}</b> dejó salir un grito de batalla!"),
      _INTL("¡<b>{1}</b> está alerta!"),
      _INTL("<b>{1}</b> está esperando pacientemente."),
      _INTL("<b>{1}</b> mira a su alrededor inquieto."),
      _INTL("<b>{1}</b> mira alrededor sin descanso."),
      _INTL("<b>{1}</b> mira a {2} y no puede evitar sonreir."),
      _INTL("¡<b>{1}</b> está muy cómodo!"),
      _INTL("¡<b>{1}</b> mira a la distancia, alerta!"),
      _INTL("¡Parece que <b>{1}</b> quiere cuidar que nada le pase a {2}!"),
      _INTL("<b>{1}</b> sigue codo a codo a {2}."),
      _INTL("<b>{1}</b> se ve contento consigo mismo."),
      _INTL("¡Cada día que pasa, <b>{1}</b> es más fuerte!"),
      _INTL("<b>{1}</b> intenta caminar en sincronía con {2}."),
      _INTL("<b>{1}</b> mira a {2} con respeto."),
      _INTL("<b>{1}</b> se tropieza y se ve avergonzando."),
      _INTL("<b>{1}</b> quiere jugar un poco agresivamente con {2}."),
      _INTL("<b>{1}</b> se mira a si mismo y está conforme con quién es, pues no cuestiona su existencia y su apariencia basándose en estereotipos infundados de belleza o éxito. ¡Está feliz consigo mismo y todos deberíamos aprender a aceptarnos como él lo hace!")
    ]
    value = rand(messages.length)
    # Special move route to go along with some of the dialogue
    case value
    when 0
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 15])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown
      ])
    when 2, 4
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 35])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 10,
        PBMoveRoute::TurnDown
      ])
    when 14
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 50])
      FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnRight,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnUp,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnLeft,
        PBMoveRoute::Wait, 4,
        PBMoveRoute::TurnDown
      ])
    when 22, 23
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 10])
      FollowingPkmn.move_route([
        PBMoveRoute::Jump, 0, 0
      ])
    end
    pbMessage(_INTL(messages[value], pkmn.name, $Trainer.name))
    next true
  end
}
