#-------------------------------------------------------------------------------
# These are used to define what the Follower will say when spoken to under
# specific conditions like Status or Weather or Map names
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Amie Compatibility
#-------------------------------------------------------------------------------
#if defined?(PkmnAR)
#  Events.OnTalkToFollower += proc { |_pkmn, _random_val|
#    cmd = pbMessage(_INTL("What would you like to do?"), [
#      _INTL("Play"),
#      _INTL("Talk"),
#      _INTL("Cancel")
#    ])
#    PkmnAR.show if cmd == 0
#    next true if [0, 2].include?(cmd)
#  }
#end
#-------------------------------------------------------------------------------
# Special Dialogue when Switches PIKACHU
#-------------------------------------------------------------------------------

Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[151] && pkmn.isSpecies?(:PIKACHU) #SPEAROW ATTACK
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    pbSEPlay("Pikachu (Triste)")
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está sumamente fatigado...", pkmn.name))
    pbMessage(_INTL("Apenas puede mantenerse en pie...", pkmn.name))
    next true
  end
  }
  
Events.OnTalkToFollower += proc { |pkmn|
  if $game_switches[152] && pkmn.isSpecies?(:PIKACHU) #PIKACHU TE ODIA
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    random3=rand(6)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
        if random3==0
          pbSEPlay("Pika03")
          pbMessage(_INTL("<b>{1}</b> no tiene ningún interés en ti.", pkmn.name))
        elsif random3==1
          pbSEPlay("Pika09")
          pbMessage(_INTL("<b>{1}</b> acaba de hacerte un desprecio.", pkmn.name))
        elsif random3==2
          pbSEPlay("Pikachu (Alerta)")
          pbMessage(_INTL("Parece que <b>{1}</b> no está interesado en ser tu Pokemón.", pkmn.name))
        elsif random3==3
          pbSEPlay("Pikachu (Amenazante)")
          pbMessage(_INTL("<b>{1}</b> no quiere que lo toques.", pkmn.name))
        elsif random3==4
          pbSEPlay("Pikachu (Amenazante 02)")
          pbMessage(_INTL("<b>{1}</b> no parece querer escucharte...", pkmn.name))
        elsif random3==5
          pbSEPlay("Pikachu (Enojado)")
          pbMessage(_INTL("Claramente <b>{1}</b> no te soporta.", pkmn.name))
        end
    next true
  end
}  
  
Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[172] && pkmn.isSpecies?(:PIKACHU) #NO HAT ASH
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbSEPlay("Pikachu (Sorprendido)")
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está extrañado de ver a su entrenador sin su gorra.", pkmn.name))
    next true
  end
  }
  
Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[165] && pkmn.isSpecies?(:PIKACHU) #DERROTADO POR SURGE
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    pbSEPlay("Pikachu (Triste 05)")
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está completamente desmoralizado por su última derrota contra <b>L.T.SURGE</b>.", pkmn.name))
    next true
  end
  }  

    
Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[166] && pkmn.isSpecies?(:PIKACHU) #EVOLUCIONA PIKACHU
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HATE)
    pbSEPlay("Pikachu (Regaño)")
    pbMessage(_INTL("<b>{1}</b> no tiene ningún interés en evolucionar para derrotar a <b>L.T.SURGE</b>.", pkmn.name))
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
     FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,PBMoveRoute::Wait,4,
        PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump",PBMoveRoute::Wait,10,
        PBMoveRoute::TurnUp,PBMoveRoute::Wait,4,
        PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump",PBMoveRoute::Wait,10,
        PBMoveRoute::TurnLeft,PBMoveRoute::Wait,4,
        PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump",PBMoveRoute::Wait,10,
        PBMoveRoute::TurnDown,PBMoveRoute::Wait,4,PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump"
      ])
    pbSEPlay("Pikachu (Molesto)")
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    pbMessage(_INTL("¡<b>{1}</b> no quiere cambiar. ¡Si va a ganar un combate quiere hacerlo tal y como es!", pkmn.name))
    pbMessage(_INTL("¡Juntos venceremos a <b>L.T.SURGE</b> y a su <b>RAICHU</b>!", pkmn.name))
    next true
  end
  }

  Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[180] && pkmn.isSpecies?(:PIKACHU) #PIKACHU RESFRIADO
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    pbSEPlay("Pikachu (Triste 05)")
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> no se ve nada bien. Sus mejillas lanzan pequeños chispazos.", pkmn.name))
    next true
  end
  }  
  
#-------------------------------------------------------------------------------
# Special Dialogue when Switches
#-------------------------------------------------------------------------------  

Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[161] #JIGGLYPUFF CANTA
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SLEEP)
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Bostezando)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está completamente adormecido por el canto de <b>JIGGLYPUFF</b>.", pkmn.name))
    next true
  end
  }  
  
Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_switches[175] #ASH CON VESTIDO
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Bromista)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> no para de reírse al verte con ese vestido y ese cabello.", pkmn.name))
    next true
  end
  }    
  
#-------------------------------------------------------------------------------
# Special Dialogue when the HP is low
#-------------------------------------------------------------------------------  
  
  Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if first_pkmn.hp<=first_pkmn.totalhp/4 
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Triste 05)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> apenas puede resistir.", pkmn.name))
    pbMessage(_INTL("¡Debo atender sus heridas de inmediato!", pkmn.name))
    next true
  end
  }    

 #-------------------------------------------------------------------------------
# Special Dialogue when the player is on crisis.
#-------------------------------------------------------------------------------  
  
  Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[208]<=0 && $game_variables[205]<=20 || $game_variables[208]<=0 && $game_variables[206]<=20
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pika05-2")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> te observa con preocupación por tu estado...", pkmn.name))
    FollowingPkmn.move_route([
        PBMoveRoute::TurnRight,PBMoveRoute::Wait,4,
        PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump",PBMoveRoute::Wait,10,
        PBMoveRoute::TurnUp,PBMoveRoute::Wait,4,
        PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump",PBMoveRoute::Wait,10,
        PBMoveRoute::TurnLeft,PBMoveRoute::Wait,4,
        PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump",PBMoveRoute::Wait,10,
        PBMoveRoute::TurnDown,PBMoveRoute::Wait,4,PBMoveRoute::Jump,0,0,PBMoveRoute::PlaySE,"jump"
      ])
    pbMessage(_INTL("¡Está desesperado! ¡No sabe como ayudarte!", pkmn.name))
    next true
  end
  }     
  
  #-------------------------------------------------------------------------------
# Special Dialogue when the T° changes
#-------------------------------------------------------------------------------  
  
  Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[207]<=4 
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Agotado 03)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> apenas puede soportar el frío...", pkmn.name))
    pbMessage(_INTL("¡Intenta acurrucarse junto a ti para entrar en calor!", pkmn.name))
    next true
  end
  }    
  
    Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[207]>=35
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Cansancio)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> apenas puede soportar el calor...", pkmn.name))
    pbMessage(_INTL("Está sacando la lengua y sudando.", pkmn.name))
    next true
  end
  }    
  
  Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[207]<=4 && pkmn.hasType?(:ICE)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SMILE)
    GameData::Species.play_cry(first_pkmn)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está disfrutando del frío...", pkmn.name))
    pbMessage(_INTL("¡Sonríe al verte temblando!", pkmn.name))
    next true
  end
  }    
  
    Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[207]<=4 && pkmn.hasType?(:FIRE)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    GameData::Species.play_cry(first_pkmn)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está a punto de desvancerse...", pkmn.name))
    pbMessage(_INTL("¡Hace demasiado frío para él!", pkmn.name))
    next true
  end
  }    
  
    Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[207]>=35 && pkmn.hasType?(:ICE)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SAD)
    GameData::Species.play_cry(first_pkmn)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> parece estar derritiéndose...", pkmn.name))
    pbMessage(_INTL("¡Odia el calor con todas sus fuerzas!", pkmn.name))
    next true
  end
  }    
  
      Events.OnTalkToFollower += proc { |pkmn|
  first_pkmn = $Trainer.first_able_pokemon
  if $game_variables[207]>=35 && pkmn.hasType?(:FIRE)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SMILE)
    GameData::Species.play_cry(first_pkmn)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> se ve más contento que nunca...", pkmn.name))
    pbMessage(_INTL("¡Parece querer rogarle al sol que siga ardiendo por siempre!", pkmn.name))
    next true
  end
  }    


#-------------------------------------------------------------------------------
# Special Dialogue when statused
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  case pkmn.status
  when :POISON
    first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Haciendo Fuerza 02)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_POISON)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está temblando con los efectos del veneno.", pkmn.name))
  when :BURN
    first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Huyendo asustado)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("Las quemaduras de <b>{1}</b> se ven dolorosas.\nNo está para nada bien...", pkmn.name))
  when :FROZEN
    first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Haciendo fuerza 05)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> tiene mucho frío. ¡Está congelado!\nNo se ve para nada bien...", pkmn.name))
  when :SLEEP
     first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Bostezando)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SLEEP)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> está dormido profundamente...\nNo se ve para nada bien...", pkmn.name))
  when :PARALYSIS
      first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Haciendo Fuerza) (2)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> se queda quieto y se retuerce.\nNo se ve para nada bien...", pkmn.name))
  when :FROSTBITE
    first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Haciendo fuerza 05)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("La temperatura de <b>{1}</b> lo tiene en las cuerdas...\nNo se ve para nada bien...", pkmn.name))
  when :DROWSY
    first_pkmn = $Trainer.first_able_pokemon
    if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Bostezando)")
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SLEEP)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("<b>{1}</b> se adormenta mientras camina...\nNo se ve para nada bien...", pkmn.name))
  end
  next true if pkmn.status != :NONE
}
#-------------------------------------------------------------------------------
# Special hold item on a map which includes battle in the name
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |_pkmn, _random_val|
  if $game_map.name.include?("Battle")
    first_pkmn = $Trainer.first_able_pokemon
     if pkmn.isSpecies?(:PIKACHU)
      pbPikaPlay
     else
     GameData::Species.play_cry(first_pkmn)
    end
    # This array can be edited and extended to your hearts content.
    items = [:POKEBALL, :POKEBALL, :POKEBALL, :GREATBALL, :GREATBALL, :ULTRABALL]
    # Choose a random item from the items array, give the player 2 of the item
    # with the message "<b>{1}</b> is holding a round object..."
    pbMEPlay("LETS PICKUP ITEM")
    next true if FollowingPkmn.item(items.sample, 2, _INTL("<b>{1}</b> sostiene un objeto redondo..."))
  end
}

#-------------------------------------------------------------------------------
# Flash Use
#-------------------------------------------------------------------------------

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if FollowingPkmn::FIRE_FOLLOWERS.include?($Trainer.first_able_pokemon) && $game_map.name =="TÚNEL DE ROCA" && pkmn.happiness>=25 
    first_pkmn = $Trainer.first_able_pokemon
    GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SMILE)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("¡<b>{1}</b>! ¡Ayúdame a iluminar este lugar con tu flama!", pkmn.name))
    darkness = $PokemonTemp.darknessSprite
    return false if !darkness || darkness.disposed?
    $PokemonGlobal.flashUsed = true
    while darkness.radius<176
    pbSEPlay("Fire2")
    Graphics.update
    Input.update
    pbUpdateSceneMap
    darkness.radius += 4
   end
   next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if FollowingPkmn::FIRE_FOLLOWERS.include?($Trainer.first_able_pokemon) && $game_map.name =="TÚNEL DE ROCA" && pkmn.happiness<=25 
    first_pkmn = $Trainer.first_able_pokemon
    GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    pbMessage(_INTL("¡<b>{1}</b> no parece tener interés en iluminar este lugar!", pkmn.name))
   next true
  end
}

#-------------------------------------------------------------------------------
# Specific message if the Pokemon is a X type and the map's name is X
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Bosque Verde" && pkmn.hasType?(:BUG) || $game_map.name == "Bosque Lirio" && pkmn.hasType?(:BUG)
    first_pkmn = $Trainer.first_able_pokemon
      if pkmn.isSpecies?(:PIKACHU)
      pbPikaPlay
     else
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> parece muy interesado en los árboles."),
      _INTL("Parece que <b>{1}</b> disfruta del zumbido de los Pokemón insecto."),
      _INTL("<b>{1}</b> está saltando inquieto en el bosque.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Túnel de Roca" && pkmn.hasType?(:GROUND) || $game_map.name == "Túnel de Roca" && pkmn.hasType?(:ROCK)
    first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente tranquilo en la cueva."),
      _INTL("¡Parece que el silencio de la cueva relaja a <b>{1}</b>!"),
      _INTL("¡Mientras más profundo estemos, mejor!\n¿Verdad <b>{1}</b>?")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Túnel de Roca" && pkmn.hasType?(:DARK)
    first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente tranquilo en la cueva."),
      _INTL("¡Parece que la oscuridad de la cueva relaja a <b>{1}</b>!"),
      _INTL("<b>{1}</b> está demasiado cómodo en la penumbra... ¡No quiere salir al exterior!")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Mt.Luna" && pkmn.hasType?(:GROUND) || $game_map.name == "Mt.Luna" && pkmn.hasType?(:ROCK)
    first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente tranquilo en la cueva."),
      _INTL("¡Parece que el silencio de la cueva relaja a <b>{1}</b>!"),
      _INTL("¡Mientras más profundo estemos, mejor!\n¿Verdad <b>{1}</b>?")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Cueva Diglett" && pkmn.hasType?(:GROUND) || $game_map.name == "Cueva Diglett" && pkmn.hasType?(:ROCK)
    first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente tranquilo en la cueva."),
      _INTL("¡Parece que <b>{1}</b> detecta a los pequeños <b>DIGLETT</b> bajo tierra!"),
      _INTL("¡Mientras más profundo estemos, mejor!\n¿Verdad <b>{1}</b>?")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Mt.Luna" && pkmn.hasType?(:DARK) || $game_map.name == "Cueva Diglett" && pkmn.hasType?(:DARK)
    first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente tranquilo en la cueva."),
      _INTL("¡Parece que la oscuridad de la cueva relaja a <b>{1}</b>!"),
      _INTL("<b>{1}</b> está demasiado cómodo en la penumbra... ¡No quiere salir al exterior!")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Alcantarillas" && pkmn.hasType?(:POISON)
    first_pkmn = $Trainer.first_able_pokemon
    GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está respirando profundamente los dulces aromas del lugar."),
      _INTL("Parece que <b>{1}</b> siente como sus pulmones se llenan de gases tóxicos y sonríe."),
      _INTL("<b>{1}</b> se revuelca en la mugre con satisfacción.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Ciudad Grisácea" && pkmn.hasType?(:POISON)
    first_pkmn = $Trainer.first_able_pokemon
    GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> respira profundo la toxicidad del aire."),
      _INTL("Parece que <b>{1}</b> disfruta de oler los restos de basura en el piso."),
      _INTL("<b>{1}</b> está relajado con la polución.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Gimnasio Grand P-1" && pkmn.hasType?(:FIGHTING)
    first_pkmn = $Trainer.first_able_pokemon
    GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está haciendo movimientos de artes marciales para impresionar a sus rivales."),
      _INTL("Parece que <b>{1}</b> tiene las mirada más agresiva que he visto."),
      _INTL("<b>{1}</b> está deseoso de combatir.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Torre Pokemón" && pkmn.hasType?(:GHOST) || $game_map.name == "Torre Pokemón" && pkmn.hasType?(:DARK) || $game_map.name == "Torre Pokemón" && pkmn.hasType?(:PSYCHIC)
    first_pkmn = $Trainer.first_able_pokemon
    GameData::Species.play_cry(first_pkmn)
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente a gusto con la oscuridad."),
      _INTL("<b>{1}</b> se siente como en casa."),
      _INTL("<b>{1}</b> está sonriéndole a un rincón...\nDonde no hay nada...")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}


#-------------------------------------------------------------------------------
# Specific message if the map name is Pokemon Lab
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Laboratorio de Oak"
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
        if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está tocando algún tipo de interruptor."),
      _INTL("<b>{1}</b> quiere tocar toda clase de botones."),
      _INTL("Pareciera que <b>{1}</b> quiere jugar con las computadoras.\n¡Como si alguien tuviese tiempo para eso!"),
      _INTL("¡<b>{1}</b> tiene un cable en la boca!"),
      _INTL("<b>{1}</b> parece que quiere tocar los aparatos tecnológicos.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Alcantarillas"
      if pkmn.isSpecies?(:PIKACHU)
      pbSEPlay("Pikachu (Haciendo Fuerza 02)")
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> intenta tapar su nariz a toda costa."),
      _INTL("Parece que <b>{1}</b> tiene ganas de vomitar."),
      _INTL("<b>{1}</b> se ve totalmente desorientado con el hedor del lugar.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Ciudad Grisácea"
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaSad
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> se ve molesto por el aire contaminado."),
      _INTL("Parece que <b>{1}</b> detesta los malos olores del ambiente."),
      _INTL("<b>{1}</b> está nervioso por la oscura bruma que lo rodea.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Túnel de Roca"
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaSad
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("Este lugar es demasiado oscuro para <b>{1}</b>."),
      _INTL("La penumbra dibuja sombras extrañas en los alrededores.\n¡Y <b>{1}</b> las detesta!"),
      _INTL("<b>{1}</b> acaba de tropezar con una roca...")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Mt.Luna" || $game_map.name == "Cueva Desconocida" || $game_map.name == "Cueva Diglett"
    if pkmn.isSpecies?(:PIKACHU)
     pbSEPlay("Pika04")
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está nervioso por la oscuridad de la cueva."),
      _INTL("¡<b>{1}</b> se ha acurrucado asustado junto a ti!"),
      _INTL("<b>{1}</b> se asusta de su propio eco.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "S.S.Anne" 
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SMILE)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está impresionado por el lujo del <b>SS.ANNE</b>."),
      _INTL("<b>{1}</b> mira a <b>{2}</b>...\n¡Al parecer quiere mirar el paisaje de cubierta!"),
      _INTL("Pareciera que <b>{1}</b> está un poco mareado con el movimiento del barco.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Torre Pokemón" 
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaSad
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_SMILE)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está sumamente nervioso"),
      _INTL("<b>{1}</b> tiene una cara de susto terrible."),
      _INTL("Pareciera que <b>{1}</b> ve cosas a su alrededor.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Guarida Rocket" 
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaBattle
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está alerta a su alrededor."),
      _INTL("¡<b>{1}</b> está listo para enfrentar cualquier amenaza!"),
      _INTL("<b>{1}</b> parece querer proteger a {2}.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name == "Gimnasio Grand P-1" 
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaSad
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está algo intimidado por los músculos de los Pokemón que lo rodean."),
      _INTL("¡<b>{1}</b> no se ve muy convencido de entrar a ese ring!"),
      _INTL("<b>{1}</b> observa a {2} asustado. Se pregunta si debe entrar a luchar con esos Pokemón.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

#-------------------------------------------------------------------------------
# Specific message if the map name has the players name in it like the
# Player's House
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name.include?($Trainer.name)
     if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> está olfateando la habitación."),
      _INTL("<b>{1}</b> notó que la madre de {2} está cerca."),
      _INTL("Pareciera que <b>{1}</b> quiere quedarse en casa.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message if the map name has Pokecenter or Store
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name.include?("Poké Center") ||
     $game_map.name.include?("Centro Pokemón")
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
     if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> parece feliz de ver a la enfermera."),
      _INTL("<b>{1}</b> se ve un poco mejor al estar en el CENTRO POKEMÓN."),
      _INTL("<b>{1}</b> parece fascinado por la maquinaria de curación."),
      _INTL("<b>{1}</b> parece que quiere tomar una siesta."),
      _INTL("<b>{1}</b> chirrió un saludo a la enfermera."),
      _INTL("<b>{1}</b> está observando {2} con una mirada juguetona."),
      _INTL("<b>{1}</b> parece estar completamente a gusto."),
      _INTL("<b>{1}</b> se está acomodando."),
      _INTL("Ahora <b>{1}</b> se siente más seguro que afuera."),
      _INTL("Hay una expresión de tranquilidad de <b>{1}</b> después de tantas batallas."),
      _INTL("¡Vaya! ¡<b>{1}</b> también tararea la canción!"),
      _INTL("Hay una expresión de agradecimiento en la cara de <b>{1}</b>.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}

Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name.include?("Poketienda") ||
     $game_map.name.include?("Centro Comercial")
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
     if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> observa con atención la cantidad de objetos a su alrededor."),
      _INTL("<b>{1}</b> no para de oler buscando algún dulce."),
      _INTL("Los objetos llaman la atención de <b>{1}</b>."),
      _INTL("Pareciera que <b>{1}</b> quiere distraer al vendedor."),
      _INTL("<b>{1}</b> está relajado."),
      _INTL("<b>{1}</b> se estresa un poco en las tiendas."),
      _INTL("<b>{1}</b> mira a <b>{2}</b> con expresión de agradecimiento."),
      _INTL("<b>{1}</b> está pensando si es o no buena idea sacar algo de la góndola.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message if the map name has Forest
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name.include?("Bosque")
   first_pkmn = $Trainer.first_able_pokemon
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_MUSIC)
     if pkmn.isSpecies?(:PIKACHU)
      pbPikaPlay
     else
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> parece muy interesado en los árboles."),
      _INTL("<b>{1}</b> parece disfrutar del zumbido de los Pokemón insecto."),
      _INTL("<b>{1}</b> está saltando y disfrutando de la naturaleza."),
      _INTL("<b>{1}</b> ama pasear y escuchar los diferentes sonidos."),
      _INTL("<b>{1}</b> está mordiendo la hierba."),
      _INTL("<b>{1}</b> está paseando y disfrutando del paisaje del bosque."),
      _INTL("<b>{1}</b> está jugando, arrancando trozos de hierba."),
      _INTL("<b>{1}</b> está mirando la luz que viene a través de los árboles."),
      _INTL("¡<b>{1}</b> está jugando con una hoja!"),
      _INTL("<b>{1}</b> parece estar escuchando el sonido de las hojas que crujen."),
      _INTL("<b>{1}</b> está perfectamente quieto y podría estar imitando a un árbol..."),
      _INTL("¡Haha! ¡<b>{1}</b> se enredó en una rama y casi tropieza!"),
      _INTL("<b>{1}</b> mira el paisaje del bosque como si fuese un explorador."),
      _INTL("<b>{1}</b> le interesan bastante los árboles."),
      _INTL("El sonido de las hojas secas le encanta a <b>{1}</b>..."),
      _INTL("El sonido del follaje relaja a <b>{1}</b>."),
      _INTL("<b>{1}</b> está buscando flores de colores."),
      _INTL("<b>{1}</b> se siente completamente a gusto."),
      _INTL("<b>{1}</b> está muy quieto... Parece que...\n¿Está imitando un árbol?..."),
      _INTL("<b>{1}</b> está mascando la hierba... No le agrada..."),
      _INTL("<b>{1}</b> se pone reflexivo siempre que ve un bosque."),
      _INTL("<b>{1}</b> está recolectando semillas..."),
      _INTL("<b>{1}</b> se asustó por un árbol que parece siniestro."),
      _INTL("¡<b>{1}</b> se sorprendió cuando fue golpeado por una rama!")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message if the map name has Gym in it
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name.include?("Gimnasio Pokemón")
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaBattle
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("¡<b>{1}</b> parece ansioso por luchar!"),
      _INTL("<b>{1}</b> mira a {2} con un brillo de determinación en sus ojos."),
      _INTL("<b>{1}</b> está tratando de intimidar a los otros entrenadores."),
      _INTL("<b>{1}</b> confía en {2} para idear una estrategia ganadora."),
      _INTL("<b>{1}</b> está vigilando al líder del gimnasio."),
      _INTL("<b>{1}</b> está dispuesto a pelearse con alguien."),
      _INTL("¡<b>{1}</b> parece que podría estar preparándose para un gran enfrentamiento!"),
      _INTL("¡<b>{1}</b> quiere mostrar lo fuerte que es!"),
      _INTL("El cuerpo de <b>{1}</b> está en forma."),
      _INTL("¡<b>{1}</b> quiere mostrar la fuerte que se ha vuelto!"),
      _INTL("¡¿Estás listo <b>{1}</b>?!"),
      _INTL("¡Esa medalla es nuestra <b>{1}</b>!"),
      _INTL("<b>{1}</b> está gruñendo en silencio en la contemplación...")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message if the map name has Beach in it
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if $game_map.name.include?("Ciudad Portavista") || $game_map.name.include?("Cabo Celeste")
    FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
    if pkmn.isSpecies?(:PIKACHU)
     pbPikaPlay
    else
      first_pkmn = $Trainer.first_able_pokemon
     GameData::Species.play_cry(first_pkmn)
    end
    pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
    messages = [
      _INTL("<b>{1}</b> parece estar disfrutando del paisaje."),
      _INTL("<b>{1}</b> parece disfrutar del sonido de las olas moviendo la arena."),
      _INTL("¡Parece que <b>{1}</b> quiere nadar!"),
      _INTL("<b>{1}</b> apenas puede apartar la mirada del océano."),
      _INTL("<b>{1}</b> mira con anhelo el agua."),
      _INTL("<b>{1}</b> sigue tratando de empujar a {2} hacia el agua."),
      _INTL("¡<b>{1}</b> se emociona al ver el mar!"),
      _INTL("¡<b>{1}</b> está feliz mirando las olas!"),
      _INTL("¡<b>{1}</b> está jugando en la arena!"),
      _INTL("Intenta ocultarlo, pero la verdad es que <b>{1}</b> respeta muchísimo el mar..."),
      _INTL("<b>{1}</b> está mirando fijamente las huellas de {2} en la arena."),
      _INTL("¡La arena se le mete por todos lados a <b>{1}</b>!"),
      _INTL("<b>{1}</b> está rodando por la arena.")
    ]
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message when the weather is Rainy. Pokemon of different types
# have different reactions to the weather.
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if [:Rain, :HeavyRain].include?($game_screen.weather_type)
    if pkmn.hasType?(:FIRE)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> parece muy molesto el clima."),
        _INTL("<b>{1}</b> está temblando..."),
        _INTL("<b>{1}</b> parece que no le gusta estar todo mojado..."),
        _INTL("<b>{1}</b> sigue tratando de sacudirse para secarse..."),
        _INTL("<b>{1}</b> se acercó a {2} por comodidad."),
        _INTL("<b>{1}</b> está mirando al cielo y frunciendo el ceño."),
        _INTL("Claramente, esto no es del agrado de <b>{1}</b>."),
        _INTL("<b>{1}</b> intentó escupir una bola de fuego ¡Y no pudo!"),
        _INTL("Las flamas de <b>{1}</b> son más débiles de lo normal."),
        _INTL("<b>{1}</b> parece tener dificultades para mover su cuerpo.")
      ]
      elsif pkmn.hasType?(:GROUND) || pkmn.hasType?(:ROCK)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> parece muy molesto el clima."),
        _INTL("<b>{1}</b> está temblando..."),
        _INTL("<b>{1}</b> parece que no le gusta estar todo mojado..."),
        _INTL("<b>{1}</b> sigue tratando de sacudirse para secarse..."),
        _INTL("<b>{1}</b> se acercó a {2} por comodidad."),
        _INTL("<b>{1}</b> está mirando al cielo y frunciendo el ceño."),
        _INTL("Claramente, esto no es del agrado de <b>{1}</b>."),
        _INTL("<b>{1}</b> detesta estar así de húmedo..."),
        _INTL("<b>{1}</b> intenta enterrarse bajo tierra..."),
        _INTL("<b>{1}</b> parece tener dificultades para mover su cuerpo.")
      ]  
    elsif pkmn.hasType?(:WATER) || pkmn.hasType?(:GRASS)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> parece estar disfrutando del tiempo."),
        _INTL("¡<b>{1}</b> parece feliz bajo la lluvia!"),
        _INTL("¡<b>{1}</b> parece estar muy sorprendido de que esté lloviendo!"),
        _INTL("¡<b>{1}</b> sonrió felizmente a {2}!"),
        _INTL("<b>{1}</b> está mirando las nubes de lluvia."),
        _INTL("Las gotas de lluvia siguen cayendo sobre <b>{1}</b>."),
        _INTL("¡<b>{1}</b> está sorprendido por el agua que cae en su rostro!"),
        _INTL("<b>{1}</b> está danzando y chapoteando en los charcos."),
        _INTL("¡<b>{1}</b> salpica agua!"),
        _INTL("<b>{1}</b> está mirando hacia arriba con la boca abierta.")
      ]
    else
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
      first_pkmn = $Trainer.first_able_pokemon
      if pkmn.isSpecies?(:PIKACHU)
      pbPikaPlay
      else
      GameData::Species.play_cry(first_pkmn)
      end
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está mirando al cielo."),
        _INTL("<b>{1}</b> parece un poco sorprendido de ver llover."),
        _INTL("<b>{1}</b> sigue tratando de sacudirse para secarse."),
        _INTL("La lluvia no parece molestar demasiado a <b>{1}</b>."),
        _INTL("¡<b>{1}</b> está jugando en un charco!"),
        _INTL("¡Ha ha!\n¡<b>{1}</b> se resbala en el agua y casi se cae!")
      ]
    end
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message when the weather is Storm. Pokemon of different types
# have different reactions to the weather.
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if :Storm == $game_screen.weather_type
    if pkmn.hasType?(:ELECTRIC)
      if pkmn.isSpecies?(:PIKACHU)
      pbPikaPlay
      else
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      end
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está mirando al cielo."),
        _INTL("La tormenta parece estar haciendo <b>{1}</b> se emocione."),
        _INTL("¡<b>{1}</b> miró al cielo y gritó con fuerza!"),
        _INTL("¡La tormenta sólo parece energizar <b>{1}</b>!"),
        _INTL("¡<b>{1}</b> es feliz soltando chispas y saltando en círculos!"),
        _INTL("¡Qué curioso! ¡Los truenos tranquilizan a <b>{1}</b>!")
      ]
    else
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> mira al cielo preocupado porque la tormente empeore."),
        _INTL("La tormenta parece poner un poco nervioso a <b>{1}</b>."),
        _INTL("¡El relámpago asustó a <b>{1}</b>!"),
        _INTL("La lluvia no parece molestar mucho a <b>{1}</b>."),
        _INTL("El tiempo parece poner en vilo a <b>{1}</b>."),
        _INTL("¡<b>{1}</b> se asustó por el rayo y se acurrucó con {2}!")
      ]
    end
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message when the weather is Snowy. Pokemon of different types
# have different reactions to the weather.
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if :Snow == $game_screen.weather_type
    if pkmn.hasType?(:ICE)
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está viendo caer la nieve con gran felicidad."),
        _INTL("¡<b>{1}</b> está encantado con la nieve!"),
        _INTL("<b>{1}</b> mira al cielo con una sonrisa."),
        _INTL("La nieve parece haber puesto a <b>{1}</b> de buen humor."),
        _INTL("¡<b>{1}</b> está alegre por el frío!")
      ]
    else
      if pkmn.isSpecies?(:PIKACHU)
      pbPikaPlay
      else
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      end
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> observa caer la nieve."),
        _INTL("<b>{1}</b> está pellizcando los copos de nieve que caen."),
        _INTL("<b>{1}</b> quiere atrapar un copo de nieve en su boca."),
        _INTL("<b>{1}</b> está fascinado por la nieve."),
        _INTL("¡Los dientes de <b>{1}</b> castañetean!"),
        _INTL("<b>{1}</b> hizo su cuerpo ligeramente más pequeño debido al frío...")
      ]
    end
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message when the weather is Blizzard. Pokemon of different types
# have different reactions to the weather.
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if :Blizzard == $game_screen.weather_type
    if pkmn.hasType?(:ICE)
       first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está viendo caer el granizo."),
        _INTL("A <b>{1}</b> no le molesta en absoluto el granizo."),
        _INTL("<b>{1}</b> está mirando al cielo con una sonrisa."),
        _INTL("El granizo, increíblemente, pone a <b>{1}</b> de buen humor."),
        _INTL("<b>{1}</b> ama roer los trozos de hielo.")
      ]
    else
      if pkmn.isSpecies?(:PIKACHU)
      pbPikaBattle
      else
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      end
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("¡<b>{1}</b>! ¡Está cayendo granizo!"),
        _INTL("<b>{1}</b> quiere evitar el granizo como sea."),
        _INTL("El granizo está golpeando <b>{1}</b> dolorosamente."),
        _INTL("<b>{1}</b> parece infeliz."),
        _INTL("¡<b>{1}</b> está temblando y molesto!")
      ]
    end
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message when the weather is Sandstorm. Pokemon of different types
# have different reactions to the weather.
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if :Sandstorm == $game_screen.weather_type
    if pkmn.hasType?(:ROCK) || pkmn.hasType?(:GROUND)
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está cubierto de arena."),
        _INTL("¡El tiempo no parece molestar a <b>{1}</b> en absoluto!"),
        _INTL("¡La arena no puede frenar a <b>{1}</b>!"),
        _INTL("<b>{1}</b> adora cuando el día está así.")
      ]
    elsif pkmn.hasType?(:STEEL)
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está cubierto de arena, pero no parece importarle."),
        _INTL("<b>{1}</b> parece no molestarse por la tormenta de arena."),
        _INTL("La arena no frena a <b>{1}</b>."),
        _INTL("<b>{1}</b> no parece importarle el tiempo.")
      ]
    else
      if pkmn.isSpecies?(:PIKACHU)
      pbPikaBattle
      else
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      end
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está cubierto de arena..."),
        _INTL("¡<b>{1}</b> escupió con la boca llena de arena!"),
        _INTL("<b>{1}</b> está entrecerrando los ojos a través de la tormenta de arena."),
        _INTL("La arena se mete en todos lados... ¿Verdad <b>{1}</b>?")
      ]
    end
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
#-------------------------------------------------------------------------------
# Specific message when the weather is Sunny. Pokemon of different types
# have different reactions to the weather.
#-------------------------------------------------------------------------------
Events.OnTalkToFollower += proc { |pkmn, _random_val|
  if :Sun == $game_screen.weather_type
    if pkmn.hasType?(:GRASS)
        first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> parece estar encantado de salir al sol."),
        _INTL("<b>{1}</b> está tomando el sol."),
        _INTL("La brillante luz del sol no parece molestar en absoluto a <b>{1}</b>."),
        _INTL("¡<b>{1}</b> dejó salir un poco de esporas de tanta emoción!"),
        _INTL("<b>{1}</b> tiene el cuerpo estirado y se relaja al sol."),
        _INTL("Siempre <b>{1}</b> se pone contento bajo la luz del sol."),
        _INTL("<b>{1}</b> está recargando energías con el sol..."),
        _INTL("<b>{1}</b> tiene un curioso olor. Me recuerda a las praderas de <b>PUEBLO PALETA</b>."),
        _INTL("<b>{1}</b> está desprendiendo un aroma floral.")
      ]
    elsif pkmn.hasType?(:FIRE)
        first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_HAPPY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("¡<b>{1}</b> parece estar feliz por el buen tiempo!"),
        _INTL("La brillante luz del sol no parece molestar en absoluto a <b>{1}</b>."),
        _INTL("<b>{1}</b> ¡parece encantado con el sol!"),
        _INTL("<b>{1}</b> hizo estallar una bola de fuego."),
        _INTL("¡<b>{1}</b> está exhalando fuego!"),
        _INTL("<b>{1}</b> disfruta los días cálidos."),
        _INTL("<b>{1}</b> adora el calor."),
        _INTL("¡<b>{1}</b> se ve maravillado por la luz del sol!"),
        _INTL("¡<b>{1}</b> escupe fuego y grita!"),
        _INTL("¡El día hace que <b>{1}</b> escupa fuego vigorosamente!"),
        _INTL("¡<b>{1}</b> escupe algunas bolas de fuego más! Parece contento..."),
        _INTL("<b>{1}</b> respira profundamente el calor...")
      ]
    elsif pkmn.hasType?(:DARK)
        first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ANGRY)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> odia la luz tan intensa."),
        _INTL("<b>{1}</b> parece personalmente ofendido por el sol."),
        _INTL("El sol brillante parece molestar a <b>{1}</b>."),
        _INTL("<b>{1}</b> parece molesto por alguna razón."),
        _INTL("<b>{1}</b> intenta mantenerse a la sombra de {2}."),
        _INTL("<b>{1}</b> sigue buscando refugio de la luz del sol.")
      ]
    else
       if pkmn.isSpecies?(:PIKACHU)
      pbPikaSad
      else
      first_pkmn = $Trainer.first_able_pokemon
      GameData::Species.play_cry(first_pkmn)
      end
      FollowingPkmn.animation(FollowingPkmn::ANIMATION_EMOTE_ELIPSES)
      pbMoveRoute($game_player, [PBMoveRoute::Wait, 20])
      messages = [
        _INTL("<b>{1}</b> está entrecerrando los ojos bajo la brillante luz del sol."),
        _INTL("<b>{1}</b> empieza a sudar."),
        _INTL("<b>{1}</b> parece un poco incómodo con este tiempo."),
        _INTL("<b>{1}</b> parece un poco recalentado."),
        _INTL("<b>{1}</b> parece que ya no le está gustando el clima."),
        _INTL("<b>{1}</b> se ve muy sediento..."),
        _INTL("¡<b>{1}</b> está cegado por el sol!"),
        _INTL("<b>{1}</b> se ve infeliz con este día caluroso..."),
        _INTL("¡<b>{1}</b> protegió su visión contra la luz centelleante!")
        
      ]
    end
    pbMessage(_INTL(messages.sample, pkmn.name, $Trainer.name))
    next true
  end
}
