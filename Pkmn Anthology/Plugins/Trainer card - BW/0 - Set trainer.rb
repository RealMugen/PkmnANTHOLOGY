module TCBW
	class Show

		SET = [
			# Form:
			#  [file name in Graphics\Pictures\Trainer Card BW\Trainers (just file name), map name of this leader, name of leader, information of leader, name of badge]
			["trainer059", "Ciudad Plateada", "BROCK", "Un criador por naturaleza. Cree en el poder y la determinación.", "Medalla Roca"],
			["trainer060", "Ciudad Celeste", "MISTY", "La cuarta flor acuática, su especialidad es la ofensiva acuática.", "Medalla Cascada"],
			["trainer061", "Ciudad Carmín", "L.T.SURGE", "¡El veterano de guerra! ¡Un relámpago de fuerza y velocidad!", "Medalla Trueno"],
			["trainer062", "Ciudad Azulona", "ERIKA", "Para ella el equilibrio y la compenetración lo es todo.", "Medalla Arcoíris"],
			["trainer063", "Ciudad Fucsia", "KOGA", "Don't lie to yerself! It's best to live honestly.", "Medalla Alma"],
			["trainer064", "Ciudad Azafrán", "SABRINA", "To fly, go beyond yearning and take action!", "Medalla Pantano"],
			["trainer065", "Isla Canela", "BLAINE", "Sincere Trainer! Be flexible!", "Medalla Volcán"],
			["trainer066", "Ciudad Verde", "GIOVANNI", "It's cliché to compare life to a sea, but don't be swept away!", "Medalla Tierra"]
		]

	end
end
