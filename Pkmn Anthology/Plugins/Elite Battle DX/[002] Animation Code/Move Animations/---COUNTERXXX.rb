#-------------------------------------------------------------------------------
#  Counter
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:COUNTER) do | scene, userindex, targetindex, hitnum=0, multihit=false | 

 # inital configuration
  usersprite, targetsprite, player, itself = EliteBattle.getIndexes(scene, userindex, targetindex)
  scene.vector.set(scene.getRealVector(targetindex,player))
    # set up animation
    fp = {}
    fp["bg"] = Sprite.new(targetsprite.viewport)
    fp["bg"].bitmap = Bitmap.new(targetsprite.viewport.rect.width,targetsprite.viewport.rect.height)
    fp["bg"].bitmap.fill_rect(0,0,fp["bg"].bitmap.width,fp["bg"].bitmap.height,Color.new(217/6,189/6,52/6))
    fp["bg"].opacity = 0
    l = 0; m = 0; q = 0
    for i in 0...12
      fp["#{i}"] = Sprite.new(targetsprite.viewport)
      fp["#{i}"].opacity = 0
      fp["#{i}"].z = 51
    end
    fp["punch"] = Sprite.new(targetsprite.viewport)
    fp["punch"].bitmap = pbBitmap("Graphics/Animations/eb108")
    fp["punch"].ox = fp["punch"].bitmap.width/2
    fp["punch"].oy = fp["punch"].bitmap.height/2
    fp["punch"].opacity = 0
    fp["punch"].z = 40
    fp["punch"].angle = 180
    fp["punch"].zoom_x = player ? 6 : 4
    fp["punch"].zoom_y = player ? 6 : 4
    fp["punch"].color = Color.new(217,189,52,50)
    # start animation
    @vector.set(getRealVector(targetindex,player))
    pbSEPlay("#{SE_EXTRA_PATH}fog2",75)
    for i in 0...72
      cx, cy = getCenter(targetsprite,true)
      fp["punch"].x = cx
      fp["punch"].y = cy
      fp["punch"].angle -= 45 if i < 40
      fp["punch"].zoom_x -= player ? 0.2 : 0.15 if i < 40
      fp["punch"].zoom_y -= player ? 0.2 : 0.15 if i < 40
      fp["punch"].opacity += 8 if i < 40
      if i >= 40
        fp["punch"].tone = Tone.new(255,255,255) if i == 40
        fp["punch"].toneAll(-25.5)
        fp["punch"].opacity -= 25.5
      end
      pbSEPlay("#{SE_EXTRA_PATH}hit") if i==40
      for n in 0...12
        next if m>(i-40)/4
        fp["#{n}"].opacity += 51 if fp["#{n}"].tone.gray == 0
        fp["#{n}"].tone.gray = 1 if fp["#{n}"].opacity >= 255
        q += 1 if fp["#{n}"].opacity >= 255
        fp["#{n}"].opacity -= 10 if fp["#{n}"].tone.gray > 0 && q > 96
      end
      fp["bg"].opacity += 4 if  i < 40
      fp["bg"].opacity -= 10 if i >= 56
      targetsprite.tone = Tone.new(100,80,60) if i == 40
      if i >= 40
        if (i-40)/3 > l
          m += 1
          m = 0 if m > 1
          l = (i-40)/3
        end
        targetsprite.zoom_y -= 0.16*(m==0 ? 1 : -1)
        targetsprite.zoom_x += 0.08*(m==0 ? 1 : -1)
        targetsprite.tone.red -= 5 if targetsprite.tone.red > 0
        targetsprite.tone.green -= 4 if targetsprite.tone.green > 0
        targetsprite.tone.blue -= 3 if targetsprite.tone.blue > 0
        targetsprite.still
      end
      @scene.wait(1,(i < 40))
    end
    @vector.set(defaultvector) if !multihit
    pbDisposeSpriteHash(fp)
    return true
end
