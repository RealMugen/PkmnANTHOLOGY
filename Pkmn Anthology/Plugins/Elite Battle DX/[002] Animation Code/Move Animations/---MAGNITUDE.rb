#-------------------------------------------------------------------------------
#  MAGNITUDE
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:MAGNITUDE) do
  @vector.set(@scene.getRealVector(@targetIndex, @targetIsPlayer))
  @scene.wait(16,true)
  factor = @targetSprite.zoom_x
   # set up animation
    fp = {}
    randx = []
    randy = []
    speed = []
    angle = []
    fp["bg"] = Sprite.new(usersprite.viewport)
    fp["bg"].bitmap = Bitmap.new(usersprite.viewport.rect.width,usersprite.viewport.rect.height)
    fp["bg"].bitmap.fill_rect(0,0,fp["bg"].bitmap.width,fp["bg"].bitmap.height,Color.new(0,0,0))
    fp["bg"].opacity = 0
    for m in 0...4
      randx.push([]); randy.push([]); speed.push([]); angle.push([])
      targetsprite = @sprites["pokemon#{m}"]
      next if !targetsprite || targetsprite.disposed? || targetsprite.fainted || !targetsprite.visible
      next if m == userindex
      for j in 0...32
        fp["#{j}#{m}"] = Sprite.new(usersprite.viewport)
        fp["#{j}#{m}"].bitmap = pbBitmap("Graphics/Animations/eb223")
        fp["#{j}#{m}"].ox = fp["#{j}#{m}"].bitmap.width/2
        fp["#{j}#{m}"].oy = fp["#{j}#{m}"].bitmap.height/2
        fp["#{j}#{m}"].z = 50
        z = [0.5,0.4,0.3,0.7][rand(4)]
        fp["#{j}#{m}"].zoom_x = z
        fp["#{j}#{m}"].zoom_y = z
        fp["#{j}#{m}"].visible = false
        randx[m].push(rand(82)+(rand(2)==0 ? 82 : 0))
        randy[m].push(rand(32)+32)
        speed[m].push(4)
        angle[m].push((rand(8)+1)*(rand(2)==0 ? -1 : 1))
      end
    end
    @vector.set(DUALVECTOR)
    16.times do
      fp["bg"].opacity += 8
       @scene.wait(1,true)
    end
    factor = usersprite.zoom_x
    k = -1
    pbSEPlay("#{SE_EXTRA_PATH}Earth4")
    for i in 0...92
      for m in 0...4
        targetsprite = @sprites["pokemon#{m}"]
        next if !targetsprite || targetsprite.disposed? || targetsprite.fainted || !targetsprite.visible
        next if m == userindex
        cx, cy = getCenter(targetsprite,true)
        for j in 0...32
          next if j>(i/2)
          if !fp["#{j}#{m}"].visible
            fp["#{j}#{m}"].visible = true
            fp["#{j}#{m}"].x = cx - 82*targetsprite.zoom_x + randx[m][j]*targetsprite.zoom_x
            fp["#{j}#{m}"].y = targetsprite.y
            fp["#{j}#{m}"].zoom_x *= targetsprite.zoom_x
            fp["#{j}#{m}"].zoom_y *= targetsprite.zoom_y
          end
          fp["#{j}#{m}"].y -= speed[m][j]*2*targetsprite.zoom_y
          speed[m][j] *= -1 if (fp["#{j}#{m}"].y <= targetsprite.y - randy[m][j]*targetsprite.zoom_y) || (fp["#{j}#{m}"].y >= targetsprite.y)
          fp["#{j}#{m}"].opacity -= 35 if speed[m][j] < 0
          fp["#{j}#{m}"].angle += angle[m][j]
        end
      end
      usersprite.zoom_x -= 0.2/6 if usersprite.zoom_x > factor
      usersprite.zoom_y += 0.2/6 if usersprite.zoom_y < factor
      
      moveEntireScene(k*8,0,true,true)
      k *= -1 if i%3==0
      if i%32==0
        pbSEPlay("#{SE_EXTRA_PATH}Earth4",60)
        usersprite.zoom_x = factor*1.2
        usersprite.zoom_y = factor*0.8
      end
      fp["bg"].opacity -= 12 if i >= 72
      @scene.wait(1,false)
    end
    @vector.set(defaultvector) if !multihit
    pbDisposeSpriteHash(fp)
end
