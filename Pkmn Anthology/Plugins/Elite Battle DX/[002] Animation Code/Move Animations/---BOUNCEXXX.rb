#-------------------------------------------------------------------------------
#  Bounce
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:BOUNCE) do
  if @hitNum == 1
    EliteBattle.playMoveAnimation(:BOUNCE_DOWN, @scene, @userIndex, @targetIndex)
  elsif @hitNum == 0
    EliteBattle.playMoveAnimation(:BOUNCE_UP, @scene, @userIndex, @targetIndex)
  end
end

EliteBattle.defineMoveAnimation(:BOUNCE_DOWN) do
  vector = getRealVector(userindex,player)
    factor = player ? 2 : 1.5
    # set up animation
    fp = {}
    fp["fly"] = Sprite.new(usersprite.viewport)
    fp["fly"].bitmap = pbBitmap("Graphics/Animations/eb157")
    fp["fly"].ox = fp["fly"].bitmap.width/2
    fp["fly"].oy = fp["fly"].bitmap.height/2
    fp["fly"].z = 50
    fp["fly"].x, fp["fly"].y = getCenter(usersprite)
    fp["fly"].opacity = 0
    fp["fly"].zoom_x = factor*1.4
    fp["fly"].zoom_y = factor*1.4
    # start animation
    @vector.set(vector)
     @scene.wait(20,true)
    pbSEPlay("#{SE_EXTRA_PATH}Refresh")
    for i in 0...20
      cx, cy = getCenter(usersprite)
      fp["fly"].x = cx
      fp["fly"].y = cy
      fp["fly"].zoom_x -= factor*0.4/10
      fp["fly"].zoom_y -= factor*0.4/10
      fp["fly"].opacity += 51
      usersprite.visible = false if i == 6
      usersprite.hidden = true if i == 6
       @scene.wait(1,true)
    end
    10.times do
      fp["fly"].zoom_x += factor*0.4/10
      fp["fly"].zoom_y += factor*0.4/10
       @scene.wait(1,true)
    end
    @vector.set(vector[0],vector[1]+196,vector[2],vector[3],vector[4],vector[5])
    for i in 0...20
       @scene.wait(1,true)
      cx, cy = getCenter(usersprite)
      if i < 10
        fp["fly"].zoom_y -= factor*0.02
      elsif
        fp["fly"].zoom_x -= factor*0.02
        fp["fly"].zoom_y += factor*0.04
      end
      fp["fly"].x = cx
      fp["fly"].y = cy
      fp["fly"].y -= 32*(i-10) if i >= 10
      pbSEPlay("eb_flying2") if i == 10
    end
    for i in 0...20
      fp["fly"].y -= 32
      fp["fly"].opacity -= 25.5 if i >= 10
      scene.wait(1,true)
    end
    pbDisposeSpriteHash(fp)
end

EliteBattle.defineMoveAnimation(:BOUNCE_UP) do
  vector = getRealVector(targetindex,player)
    factor = player ? 2 : 1.5
    # set up animation
    fp = {}
    fp["drop"] = Sprite.new(targetsprite.viewport)
    fp["drop"].bitmap = pbBitmap("Graphics/Animations/eb157_2")
    fp["drop"].ox = fp["drop"].bitmap.width/2
    fp["drop"].oy = fp["drop"].bitmap.height/2
    fp["drop"].y = 0
    fp["drop"].z = 50
    fp["drop"].visible = false
    # start animation
    @vector.set(vector)
    maxy = ((player ? @vector.y : @vector.y2)*0.1).ceil*10 - 80
    fp["drop"].y = -((maxy-(player ? @vector.y-80 : @vector.y2-80))*0.1).ceil*10
    fp["drop"].x = targetsprite.x
    pbSEPlay("#{SE_EXTRA_PATH}Wind1")
    for i in 0...20
       @scene.wait(1,true)
      if i >= 10
        fp["drop"].visible = true
        fp["drop"].x = targetsprite.x
        fp["drop"].y += maxy/10
        fp["drop"].zoom_x = targetsprite.zoom_x
        fp["drop"].zoom_y = targetsprite.zoom_y*1.4
      end
    end
    usersprite.hidden = false
    usersprite.visible = true
    pbDisposeSpriteHash(fp)
end
