#-------------------------------------------------------------------------------
#  Disable
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:DISABLE) do
  vector = @scene.getRealVector(@targetIndex, @targetIsPlayer)
  factor = @targetIsPlayer ? 2 : 1.5
# set up animation
    fp = {}
    fp["bg"] = ScrollingSprite.new(targetsprite.viewport)
    fp["bg"].speed = 6
    fp["bg"].setBitmap("Graphics/Animations/ebDISABLE",true)
    fp["bg"].color = Color.new(0,0,0,255)
    fp["bg"].opacity = 0
    fp["bg"].oy = fp["bg"].src_rect.height/2
    fp["bg"].y = targetsprite.viewport.rect.height/2
    shake = 8
    zoom = -1
    # start animation
    @vector.set(vector)
    for i in 0...72
      pbSEPlay("eb_glass",80) if i == 40
      pbSEPlay("eb_psychic4",80) if i == 62
      if i < 10
        fp["bg"].opacity += 25.5
      elsif i < 20
        fp["bg"].color.alpha -= 25.5
      elsif i >= 62
        fp["bg"].color.alpha += 25.5
        targetsprite.tone.red += 18
        targetsprite.tone.green += 18
        targetsprite.tone.blue += 18
        targetsprite.zoom_x += 0.04*factor
        targetsprite.zoom_y += 0.04*factor
      elsif i >= 40
        targetsprite.addOx(shake)
        shake = -8 if targetsprite.ox > targetsprite.bitmap.width/2 + 4
        shake = 8 if targetsprite.ox < targetsprite.bitmap.width/2 - 4
        targetsprite.still
      end
      zoom *= -1 if i%2 == 0
      fp["bg"].update
      fp["bg"].zoom_y += 0.04*zoom
      @scene.wait(1,(i<62))
    end
    targetsprite.ox = targetsprite.bitmap.width/2
    10.times do
      targetsprite.tone.red -= 18
      targetsprite.tone.green -= 18
      targetsprite.tone.blue -= 18
      targetsprite.zoom_x -= 0.04*factor
      targetsprite.zoom_y -= 0.04*factor
      targetsprite.still
      wait(1)
    end
    @scene.wait(8)
    @vector.set(defaultvector) if !multihit
    10.times do
      fp["bg"].opacity -= 25.5
      targetsprite.still
      @scene.wait(1,true)
    end
    pbDisposeSpriteHash(fp)
end
