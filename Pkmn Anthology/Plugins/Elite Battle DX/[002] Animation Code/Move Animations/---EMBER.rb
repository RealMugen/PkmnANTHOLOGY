#-------------------------------------------------------------------------------
#  Ember
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:EMBER) do
  itself = (@userIndex==@targetIndex)
   # set up animation
    fp = {}
    rndx = []
    rndy = []
    fp["bg"] = Sprite.new(targetsprite.viewport)
    fp["bg"].bitmap = Bitmap.new(targetsprite.viewport.rect.width,targetsprite.viewport.rect.height)
    fp["bg"].bitmap.fill_rect(0,0,fp["bg"].bitmap.width,fp["bg"].bitmap.height,Color.new(130,52,42))
    fp["bg"].opacity = 0
    for i in 0...16
      fp["#{i}"] = Sprite.new(targetsprite.viewport)
      fp["#{i}"].bitmap = pbBitmap("Graphics/Animations/eb136_2")
      fp["#{i}"].src_rect.set(0,101*rand(3),53,101)
      fp["#{i}"].ox = 26
      fp["#{i}"].oy = 101
      fp["#{i}"].opacity = 0
      fp["#{i}"].z = (player ? 29 : 19)
      rndx.push(rand(64))
      rndy.push(rand(64))
    end
    shake = 2
    # start animation
    for i in 0...132
      for j in 0...16
        if fp["#{j}"].opacity == 0 && fp["#{j}"].tone.gray == 0
          fp["#{j}"].zoom_x = usersprite.zoom_x
          fp["#{j}"].zoom_y = usersprite.zoom_y
          cx, cy = getCenter(usersprite)
          fp["#{j}"].x = cx
          fp["#{j}"].y = cy + 50*usersprite.zoom_y
        end
        next if j>(i/4)
        cx, cy = getCenter(targetsprite,true)
        x2 = cx - 32*targetsprite.zoom_x + rndx[j]*targetsprite.zoom_x
        y2 = cy - 32*targetsprite.zoom_y + rndy[j]*targetsprite.zoom_y + 50*targetsprite.zoom_y
        x0 = fp["#{j}"].x
        y0 = fp["#{j}"].y
        fp["#{j}"].x += (x2 - x0)*0.1
        fp["#{j}"].y += (y2 - y0)*0.1
        fp["#{j}"].zoom_x -= (fp["#{j}"].zoom_x - targetsprite.zoom_x)*0.1
        fp["#{j}"].zoom_y -= (fp["#{j}"].zoom_y - targetsprite.zoom_y)*0.1
        fp["#{j}"].src_rect.x += 53 if i%4==0
        fp["#{j}"].src_rect.x = 0 if fp["#{j}"].src_rect.x >= fp["#{j}"].bitmap.width
        if (x2 - x0)*0.1 < 1 && (y2 - y0)*0.1 < 1
          fp["#{j}"].opacity -= 8
          fp["#{j}"].tone.gray += 8
          fp["#{j}"].tone.red -= 2; fp["#{j}"].tone.green -= 2; fp["#{j}"].tone.blue -= 2
          fp["#{j}"].zoom_x -= 0.02
          fp["#{j}"].zoom_y += 0.04
        else
          fp["#{j}"].opacity += 12
        end
      end
      fp["bg"].opacity += 5 if fp["bg"].opacity < 255*0.5
      pbSEPlay("#{SE_EXTRA_PATH}Fire2",80) if i%12==0 && i <= 96
      pbSEPlay("#{SE_EXTRA_PATH}SMokescreen",120) if i==84
      if i >= 96
        targetsprite.tone.red += 2.4*2 if targetsprite.tone.red < 48*2
        targetsprite.tone.green -= 1.2*2 if targetsprite.tone.green > -24*2
        targetsprite.tone.blue -= 2.4*2 if targetsprite.tone.blue > -48*2
        targetsprite.addOx(shake)
        shake = -2 if targetsprite.ox > targetsprite.bitmap.width/2 + 2
        shake = 2 if targetsprite.ox < targetsprite.bitmap.width/2 - 2
        targetsprite.still
      end
      @vector.set(DUALVECTOR) if i == 24
      @vector.inc = 0.1 if i == 24
      @scene.wait(1,true)
    end
    20.times do
      targetsprite.tone.red -= 2.4*2
      targetsprite.tone.green += 1.2*2
      targetsprite.tone.blue += 2.4*2
      targetsprite.addOx(shake)
      shake = -2 if targetsprite.ox > targetsprite.bitmap.width/2 + 2
      shake = 2 if targetsprite.ox < targetsprite.bitmap.width/2 - 2
      targetsprite.still
      fp["bg"].opacity -= 15
      @scene.wait(1,true)
    end
    targetsprite.ox = targetsprite.bitmap.width/2
    @vector.set(defaultvector) if !multihit
    @vector.inc = 0.2
    pbDisposeSpriteHash(fp)
end
