#-------------------------------------------------------------------------------
#  SEISMICTOSS
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:SEISMICTOSS) do
  vector = @scene.getRealVector(@targetIndex, @targetIsPlayer)
  vector2 = @scene.getRealVector(@userIndex, @userIsPlayer)
  factor = @userIsPlayer ? 2 : 1
   # set up animation
    fp = {}
    rndx = []
    rndy = []
    dx = []
    dy = []
    fp["bg"] = ScrollingSprite.new(targetsprite.viewport)
    fp["bg"].speed = 64
    fp["bg"].setBitmap("Graphics/Animations/speedcloud")
    fp["bg"].color = Color.new(0,0,0,255)
    fp["bg"].opacity = 0
    fp["bg2"] = ScrollingSprite.new(targetsprite.viewport)
    fp["bg2"].speed = 2
    fp["bg2"].setBitmap("Graphics/Animations/speedearth")
    fp["bg2"].color = Color.new(0,0,0,255)
    fp["bg2"].opacity = 0
    for i in 0...12
      fp["#{i}"] = Sprite.new(targetsprite.viewport)
      fp["#{i}"].bitmap = pbBitmap("Graphics/Animations/eb303_2")
      fp["#{i}"].ox = 10
      fp["#{i}"].oy = 10
      fp["#{i}"].opacity = 0
      fp["#{i}"].z = 50
      r = rand(3)
      fp["#{i}"].zoom_x = (targetsprite.zoom_x)*(r==0 ? 1 : 0.5)
      fp["#{i}"].zoom_y = (targetsprite.zoom_y)*(r==0 ? 1 : 0.5)
      fp["#{i}"].tone = Tone.new(60,60,60)
      rndx.push(rand(128))
      rndy.push(rand(64))
    end
    @vector.set(getRealVector(targetindex,player)) if withvector
     @scene.wait(20,true) if withvector
    factor = targetsprite.zoom_y
    frame = Sprite.new(targetsprite.viewport)
    frame.z = 50
    frame.bitmap = pbBitmap("Graphics/Animations/eb303")
    frame.src_rect.set(0,0,64,64)
    frame.ox = 32
    frame.oy = 32
    frame.zoom_x = 0.5*factor
    frame.zoom_y = 0.5*factor
    frame.x, frame.y = getCenter(targetsprite,true)
    frame.opacity = 0
    frame.tone = Tone.new(255,255,255)
    frame.y -= 32*targetsprite.zoom_y
    # start animation
    pbSEPlay("eb_dragon2")
    for i in 0...20
      if i < 10
        fp["bg"].opacity += 25.5
      else
        fp["bg"].color.alpha -= 25.5
      end
      pbSEPlay("#{SE_EXTRA_PATH}Harden") if i == 4
      fp["bg"].update
       @scene.wait(1,true)
    end
     for i in 0...20
      if i < 10
        fp["bg2"].opacity += 25.5
      else
        fp["bg2"].color.alpha -= 25.5
      end
      pbSEPlay("#{SE_EXTRA_PATH}Harden") if i == 4
      fp["bg2"].update
       @scene.wait(1,true)
    end
     @scene.wait(4,true)
    pbSEPlay("eb_normal1") 
    for i in 1..30
      if i < 8 && shake
        x=(i/4 < 1) ? 2 : -2
        moveEntireScene(0,x*2,true,true)
      end
      if i.between?(1,5)
        targetsprite.still
        targetsprite.zoom_y-=0.05*factor
        targetsprite.toneAll(-12.8)
        frame.zoom_x += 0.1*factor
        frame.zoom_y += 0.1*factor
        frame.opacity += 51
      end
      frame.tone = Tone.new(0,0,0) if i == 6
      if i.between?(6,10)
        targetsprite.still
        targetsprite.zoom_y+=0.05*factor
        targetsprite.toneAll(+12.8)
        frame.angle += 2
      end
      frame.src_rect.x = 64 if i == 10
      if i >= 10
        frame.opacity -= 25.5
        frame.zoom_x += 0.1*factor
        frame.zoom_y += 0.1*factor
        frame.angle += 2
      end
      fp["bg"].update
      for j in 0...12
        cx = frame.x; cy = frame.y
        if fp["#{j}"].opacity == 0 && fp["#{j}"].visible
          fp["#{j}"].x = cx
          fp["#{j}"].y = cy
        end
        x2 = cx - 64*targetsprite.zoom_x + rndx[j]*targetsprite.zoom_x
        y2 = cy - 64*targetsprite.zoom_y + rndy[j]*targetsprite.zoom_y
        x0 = fp["#{j}"].x
        y0 = fp["#{j}"].y
        fp["#{j}"].x += (x2 - x0)*0.2
        fp["#{j}"].y += (y2 - y0)*0.2
        fp["#{j}"].zoom_x += 0.01
        fp["#{j}"].zoom_y += 0.01
        if i < 20
          fp["#{j}"].tone.red -= 6; fp["#{j}"].tone.blue -= 6; fp["#{j}"].tone.green -= 6
        end
        if (x2 - x0)*0.2 < 1 && (y2 - y0)*0.2 < 1
          fp["#{j}"].opacity -= 51
        else
          fp["#{j}"].opacity += 51
        end
        fp["#{j}"].visible = false if fp["#{j}"].opacity <= 0
      end
       @scene.wait(1)
    end
    for i in 0...20
      targetsprite.still
      if i < 10
        fp["bg"].color.alpha += 25.5
      else
        fp["bg"].opacity -= 25.5
      end
      fp["bg"].update
       @scene.wait(1,true)
    end
    for i in 0...20
      targetsprite.still
      if i < 10
        fp["bg2"].color.alpha += 25.5
      else
        fp["bg2"].opacity -= 25.5
      end
      fp["bg2"].update
      @scene.wait(1,true)
    end
    frame.dispose
    pbDisposeSpriteHash(fp)
    @vector.set(defaultvector) if !multihit
end
