#-------------------------------------------------------------------------------
#  Water Shuriken
#-------------------------------------------------------------------------------
EliteBattle.defineMoveAnimation(:WATERSHURIKEN) do
   # set up animation
    fp = {}
    rndx = []; prndx = []
    rndy = []; prndy = []
    rangl = []
    dx = []
    dy = []   
    # start animation
    for i in 0...40
      if i == 8 
        @vector.set(vector2)
      elsif i == 30
        pbSEPlay("#{SE_EXTRA_PATH}Water3",80)
      end
      @scene.wait(1,true)
    end
    cx, cy = getCenter(usersprite,true)
    dx = []
    dy = []
    for i in 0...8
      fp["#{i}s"] = Sprite.new(targetsprite.viewport)
      fp["#{i}s"].bitmap = pbBitmap("Graphics/Animations/eb093_2")
      fp["#{i}s"].src_rect.set(rand(3)*36,0,36,36)
      fp["#{i}s"].ox = fp["#{i}s"].src_rect.width/2
      fp["#{i}s"].oy = fp["#{i}s"].src_rect.height/2
      r = 128*usersprite.zoom_x
      z = [0.5,0.25,1,0.75][rand(4)]
      x, y = randCircleCord(r)
      x = cx - r + x
      y = cy - r + y
      fp["#{i}s"].x = cx
      fp["#{i}s"].y = cy
      fp["#{i}s"].zoom_x = z*usersprite.zoom_x
      fp["#{i}s"].zoom_y = z*usersprite.zoom_x
      fp["#{i}s"].visible = false
      fp["#{i}s"].z = usersprite.z + 1
      dx.push(x)
      dy.push(y)
    end
    
    fp["shot"] = Sprite.new(targetsprite.viewport)
    fp["shot"].bitmap = pbBitmap("Graphics/Animations/eb609")
    fp["shot"].ox = fp["shot"].bitmap.width/2
    fp["shot"].oy = fp["shot"].bitmap.height/2
    fp["shot"].z = usersprite.z + 1
    fp["shot"].zoom_x = usersprite.zoom_x
    fp["shot"].zoom_y = usersprite.zoom_x
    fp["shot"].opacity = 0
    
    x = defaultvector[0]; y = defaultvector[1]
    x2, y2 = @vector.spoof(defaultvector)
    fp["shot"].x = cx
    fp["shot"].y = cy
    @scene.wait(1,true)
    for i in 0...20
      cx, cy = getCenter(usersprite)
      @vector.set(defaultvector) if i == 0
      if i > 0
        fp["shot"].angle = Math.atan(1.0*(@vector.y-@vector.y2)/(@vector.x2-@vector.x))*(180.0/Math::PI) + (player ? 180 : 0)
        fp["shot"].opacity += 32
        fp["shot"].zoom_x -= (fp["shot"].zoom_x - targetsprite.zoom_x)*0.1
        fp["shot"].zoom_y -= (fp["shot"].zoom_y - targetsprite.zoom_y)*0.1
        fp["shot"].x += (player ? -1 : 1)*(x2 - x)/42
        fp["shot"].y -= (player ? -1 : 1)*(y - y2)/42
        for j in 0...8
          fp["#{j}s"].visible = true
          fp["#{j}s"].opacity -= 32
          fp["#{j}s"].x -= (fp["#{j}s"].x - dx[j])*0.2
          fp["#{j}s"].y -= (fp["#{j}s"].y - dy[j])*0.2
        end
      end
      factor = targetsprite.zoom_x if i == 12
      cx, cy = getCenter(targetsprite,true)
      if !player
        fp["shot"].z = targetsprite.z - 1 if fp["shot"].x > cx && fp["shot"].y < cy
      else
        fp["shot"].z = targetsprite.z + 1 if fp["shot"].x < cx && fp["shot"].y > cy
      end
      @scene.wait(1,i < 12)
    end
    16.times do
      fp["shot"].angle = Math.atan(1.0*(@vector.y-@vector.y2)/(@vector.x2-@vector.x))*(180.0/Math::PI) + (player ? 180 : 0)
      fp["shot"].opacity += 32
      fp["shot"].zoom_x -= (fp["shot"].zoom_x - targetsprite.zoom_x)*0.1
      fp["shot"].zoom_y -= (fp["shot"].zoom_y - targetsprite.zoom_y)*0.1
      fp["shot"].x += (player ? -1 : 1)*(x2 - x)/42
      fp["shot"].y -= (player ? -1 : 1)*(y - y2)/42
      fp["shot"].opacity -= 56
    end
    fp["frame"] = Sprite.new(targetsprite.viewport)
    fp["frame"].z = targetsprite.z + 2
    fp["frame"].bitmap = pbBitmap("Graphics/Animations/eb540")
    fp["frame"].src_rect.set(0,0,64,64)
    fp["frame"].ox = 32
    fp["frame"].oy = 32
    fp["frame"].zoom_x = 0.5*targetsprite.zoom_x
    fp["frame"].zoom_y = 0.5*targetsprite.zoom_y
    fp["frame"].x, fp["frame"].y = getCenter(targetsprite,true)
    fp["frame"].opacity = 0
    fp["frame"].tone = Tone.new(255,255,255)
    px = []
    py = []
    for i in 0...24
      fp["p#{i}"] = Sprite.new(targetsprite.viewport)
      fp["p#{i}"].bitmap = Bitmap.new(16,16)
      fp["p#{i}"].bitmap.drawCircle
      fp["p#{i}"].ox = 8
      fp["p#{i}"].oy = 8
      fp["p#{i}"].opacity = 0
      fp["p#{i}"].z = targetsprite.z
      px.push(0)
      py.push(0)
    end
    for i in 0...64
      pbSEPlay("Water1",80) if i == 11
      if i.between?(11,15)
        targetsprite.still
        targetsprite.zoom_y-=0.05*targetsprite.zoom_y
        targetsprite.toneAll(-12.8)
        fp["frame"].zoom_x += 0.1*targetsprite.zoom_x
        fp["frame"].zoom_y += 0.1*targetsprite.zoom_y
        fp["frame"].opacity += 51
      end
      fp["frame"].tone = Tone.new(0,0,0) if i == 16
      if i.between?(16,20)
        targetsprite.still
        targetsprite.zoom_y+=0.05*targetsprite.zoom_y
        targetsprite.toneAll(+12.8)
        fp["frame"].angle += 2
      end
      fp["p#{i}"].src_rect.x = 64 if i == 10
      if i >= 20
        fp["frame"].opacity -= 25.5
        fp["frame"].zoom_x += 0.1*targetsprite.zoom_x
        fp["frame"].zoom_y += 0.1*targetsprite.zoom_y
        fp["frame"].angle += 2
      end      
      for l in 0...24
        next if i < 10
        next if l>((i-10)*8)
        cx, cy = getCenter(targetsprite,true)
        if fp["p#{l}"].opacity <= 0 && fp["p#{l}"].tone.blue <= 0
          fp["p#{l}"].opacity = 255 - rand(101)
          fp["p#{l}"].x = cx
          fp["p#{l}"].y = cy
          r = rand(2)
          fp["p#{l}"].zoom_x = r==0 ? 1 : 0.5
          fp["p#{l}"].zoom_y = r==0 ? 1 : 0.5
          x = rand(128); y = rand(128)
          px[l] = cx - 64*targetsprite.zoom_x + x*targetsprite.zoom_x
          py[l] = cy - 64*targetsprite.zoom_y + y*targetsprite.zoom_y
        end
        x2 = px[l]
        y2 = py[l]
        x0 = fp["p#{l}"].x
        y0 = fp["p#{l}"].y
        fp["p#{l}"].x += (x2 - x0)*0.1
        fp["p#{l}"].y += (y2 - y0)*0.1
        fp["p#{l}"].opacity -= 8
        fp["p#{l}"].tone.blue = 1 if fp["p#{l}"].opacity <= 0
      end
      @scene.wait(1)
    end
    @vector.set(defaultvector)
    pbDisposeSpriteHash(fp)
end
