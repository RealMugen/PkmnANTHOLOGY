
#==============================================================================#
#                      Adding Terrain Tags for Rock Climb                      #
#                          Edit Terrain Tag if needed                          #
#==============================================================================#
module GameData
  class TerrainTag
    attr_reader :rock_tag
    attr_reader :RockClimb
    attr_reader :RockCrest

	alias initialize_RockClimb initialize
    def initialize(hash)
	  initialize_RockClimb(hash)
      @rock_tag               = hash[:rock_tag]   || false
      @RockClimb              = hash[:RockClimb]  || false
      @RockCrest              = hash[:RockCrest]  || false
    end
  end
end

GameData::TerrainTag.register({
  :id                     => :RockClimb,
  :id_number              => 17,   #Default: 17
  :rock_tag               => true,
  :RockClimb              => true
})

GameData::TerrainTag.register({
  :id                     => :RockCrest,
  :id_number              => 18,   #Default: 18
  :rock_tag               => true,
  :RockCrest              => true
})

#==============================================================================#
#                        All Hidden Move Configurations                        #
#==============================================================================#
Events.onAction += proc { |_sender, _e|
  terrain = $game_player.pbFacingTerrainTag
  if terrain.RockClimb
    pbRockClimb if $game_player.direction != 2
  end
  if terrain.RockCrest
    # Only allow descending if enabled.
    if RockClimbSettings::ENABLE_DESCENDING
      pbRockClimb
    else
      pbMessage(_INTL("A wall of rock is in front of you."))
    end
  end
}

HiddenMoveHandlers::CanUseMove.add(:ROCKCLIMB,proc { |move,pkmn|
  next false if !pbCheckHiddenMoveBadge(RockClimbSettings::BADGE_FOR_ROCKCLIMB)
  terrain = $game_player.pbFacingTerrainTag
  if terrain.RockClimb
    pbMessage(_INTL("Can't use that here."))
    next false
  end
  next true
})

HiddenMoveHandlers::UseMove.add(:ROCKCLIMB,proc { |move,pokemon|
  if !pbHiddenMoveAnimation(pokemon)
    pbMessage(_INTL("{1} used {2}!",pokemon.name,GameData::Move.get(move).name))
  end
  pbAscendRock
  next true
})

#==============================================================================#
#                           Rock Climb Main Function                           #
#==============================================================================#
def pbRockClimb
  move = :ROCKCLIMB
  movefinder = $Trainer.get_pokemon_with_move(move)
  if !pbCheckHiddenMoveBadge(RockClimbSettings::BADGE_FOR_ROCKCLIMB,false) || (!$DEBUG && !movefinder)
    pbMessage(_INTL("A wall of rock is in front of you."))
	return false
  end
  if pbConfirmMessage(_INTL("A wall of rock is in front of you. Would you like to use Rock Climb?"))
    speciesname=!movefinder ? $Trainer.name : movefinder.name
    pbMessage(_INTL("{1} used Rock Climb.",speciesname))
    pbHiddenMoveAnimation(movefinder)
    # Only allow descending if enabled.
    if RockClimbSettings::ENABLE_DESCENDING
      case $game_player.direction
      when 2
        pbDescendRock
      when 8
        pbAscendRock
      end
    else
      pbAscendRock
    end
    return true
  end
  return false
end

#==============================================================================#
#                        Rock Climb Ascending Animation                        #
#==============================================================================#
def pbAscendRock
  return if $game_player.direction != 8 # Can't descend if not facing up
  old_through = $game_player.through
  old_move_speed = $game_player.move_speed
  terrain = $game_player.pbFacingTerrainTag
  return if terrain.RockClimb && terrain.RockCrest
  $game_player.through = true
  $game_player.move_speed = 4
  pbJumpToward
  pbCancelVehicles
  $PokemonEncounters.reset_step_count
  $PokemonGlobal.rockclimbing = true
  pbUpdateVehicle
  loop do
    $scene.spriteset.addUserAnimation(RockClimbSettings::ROCK_DUST_ANIMATION_UP_ID,$game_player.x,$game_player.y,true,1)
    $scene.spriteset.addUserAnimation(RockClimbSettings::ROCK_CRUMBLE_ANIMATION_ID,$game_player.x,$game_player.y,true,1)
    $game_player.move_up
    terrain = $game_player.pbFacingTerrainTag
    pbWait(5)
    break if !terrain.RockClimb && !terrain.RockCrest
  end
  $PokemonGlobal.rockclimbing = false
  pbJumpToward
  $game_player.through=old_through
  $game_player.move_speed=old_move_speed
  $game_map.autoplayAsCue
  $game_player.increase_steps
end

#==============================================================================#
#                     Rock Climb Descending [No Animation]                     #
#==============================================================================#
def pbDescendRock
  return if $game_player.direction != 2 # Can't descend if not facing down
  old_through = $game_player.through
  old_move_speed = $game_player.move_speed
  old_always_on_top = $game_player.always_on_top
  terrain = $game_player.pbFacingTerrainTag
  return if terrain.RockClimb && terrain.RockCrest
  $game_player.through = true
  $game_player.move_speed = 4
  $game_player.always_on_top = true
  pbJumpToward
  pbCancelVehicles
  $PokemonEncounters.reset_step_count
  $PokemonGlobal.rockclimbing = true
  pbUpdateVehicle
  loop do
    $scene.spriteset.addUserAnimation(RockClimbSettings::ROCK_DUST_ANIMATION_DOWN_ID,$game_player.x,$game_player.y,true,1)
    $scene.spriteset.addUserAnimation(RockClimbSettings::ROCK_CRUMBLE_ANIMATION_ID,$game_player.x,$game_player.y,true,1)
    $game_player.move_down
    terrain = $game_player.pbFacingTerrainTag
    pbWait(5)
	break if !terrain.RockClimb && !terrain.RockCrest
  end
  $PokemonGlobal.rockclimbing = false
  pbJumpToward
  $game_player.through=old_through
  $game_player.move_speed=old_move_speed
  $game_player.always_on_top=old_always_on_top
  $game_map.autoplayAsCue
  $game_player.increase_steps
end

#==============================================================================#
#                     Adds Rock Climbing to $PokemonGlobal                     #
#==============================================================================#
class PokemonGlobalMetadata
  attr_accessor :rockclimbing
  @rockclimbing = false
end

#==============================================================================#
#                       Adds the Rock Climb Base Spirte                        #
#==============================================================================#
class Sprite_Character
  alias initialize_RockClimb initialize
  def initialize(viewport, character = nil)
    @rockclimbbase = Sprite_RockClimbBase.new(self, character, viewport) if character == $game_player
    initialize_RockClimb(viewport,character)
  end

  alias dispose_RockClimb dispose
  def dispose
    dispose_RockClimb
    @rockclimbbase.dispose if @rockclimbbase
    @rockclimbbase = nil
  end

  alias update_RockClimb update
  def update
    update_RockClimb
    @rockclimbbase.update if @rockclimbbase
  end
end

class Sprite_RockClimbBase
  attr_reader   :visible
  attr_accessor :event

  def initialize(sprite,event,viewport=nil)
    @rsprite  = sprite
    @sprite   = nil
    @event    = event
    @viewport = viewport
    @disposed = false
    @surfbitmap = AnimatedBitmap.new("Graphics/Characters/base_rockclimb")
    RPG::Cache.retain("Graphics/Characters/base_rockclimb")
    @cws = @surfbitmap.width/4
    @chs = @surfbitmap.height/4
    update
  end

  def dispose
    return if @disposed
    @sprite.dispose if @sprite
    @sprite   = nil
    @surfbitmap.dispose
    @disposed = true
  end

  def disposed?
    @disposed
  end

  def visible=(value)
    @visible = value
    @sprite.visible = value if @sprite && !@sprite.disposed?
  end

  def update
    return if disposed?
    if !$PokemonGlobal.rockclimbing
      # Just-in-time disposal of sprite
      if @sprite
        @sprite.dispose
        @sprite = nil
      end
      return
    end
    # Just-in-time creation of sprite
    @sprite = Sprite.new(@viewport) if !@sprite
    if @sprite
      if $PokemonGlobal.rockclimbing
        @sprite.bitmap = @surfbitmap.bitmap
        cw = @cws
        ch = @chs
      end
      sx = @event.pattern_surf*cw
      sy = ((@event.direction-2)/2)*ch
      @sprite.src_rect.set(sx,sy,cw,ch)
      @sprite.x = @rsprite.x
      @sprite.y = @rsprite.y
      @sprite.ox      = cw/2
      @sprite.oy      = ch-16   # Assume base needs offsetting
      @sprite.oy      -= @event.bob_height
      @sprite.z       = @event.screen_z(ch)-1
      @sprite.zoom_x  = @rsprite.zoom_x
      @sprite.zoom_y  = @rsprite.zoom_y
      @sprite.tone    = @rsprite.tone
      @sprite.color   = @rsprite.color
      @sprite.opacity = @rsprite.opacity
    end
  end
end

#==============================================================================#
#       Overwrites these functions locally to add the Rock Climb section       #
#==============================================================================#
def pbUpdateVehicle
  meta = GameData::Metadata.get_player($Trainer.character_ID)
  if meta
    charset = 1                                 # Regular graphic
    if $PokemonGlobal.diving;          charset = 5   # Diving graphic
    elsif $PokemonGlobal.surfing;      charset = 3   # Surfing graphic
    elsif $PokemonGlobal.rockclimbing; charset = 3   # Rock Climb graphic
    elsif $PokemonGlobal.bicycle;      charset = 2   # Bicycle graphic
    end
    newCharName = pbGetPlayerCharset(meta,charset)
    $game_player.character_name = newCharName if newCharName
  end
end

#==============================================================================#
#          Overwrites functions locally to add the Rock Climb section          #
#==============================================================================#
class Game_Map
  def playerPassable?(x, y, d, self_event = nil)
    bit = (1 << (d / 2 - 1)) & 0x0f
    for i in [2, 1, 0]
      tile_id = data[x, y, i]
      terrain = GameData::TerrainTag.try_get(@terrain_tags[tile_id])
      passage = @passages[tile_id]
      if terrain
        # Ignore bridge tiles if not on a bridge
        next if terrain.bridge && $PokemonGlobal.bridge == 0
        # Make water tiles passable if player is surfing
        return true if $PokemonGlobal.surfing && terrain.can_surf && !terrain.waterfall
        # Make Rock Climb tiles passable if player is Rock Climbing
        return true if $PokemonGlobal.rockclimbing && terrain.rock_tag
        # Prevent cycling in really tall grass/on ice
        return false if $PokemonGlobal.bicycle && terrain.must_walk
        # Depend on passability of bridge tile if on bridge
        if terrain.bridge && $PokemonGlobal.bridge > 0
          return (passage & bit == 0 && passage & 0x0f != 0x0f)
        end
      end
      # Regular passability checks
      if !terrain || !terrain.ignore_passability
        return false if passage & bit != 0 || passage & 0x0f == 0x0f
        return true if @priorities[tile_id] == 0
      end
    end
    return true
  end
end


#==============================================================================#
#           Overwrites functions locally to add the Rock Climb check           #
#==============================================================================#
class Game_Player
  def character_name
    @defaultCharacterName = "" if !@defaultCharacterName
    return @defaultCharacterName if @defaultCharacterName!=""
    if !@move_route_forcing && $Trainer.character_ID>=0
      meta = GameData::Metadata.get_player($Trainer.character_ID)
      if meta && !$PokemonGlobal.bicycle && !$PokemonGlobal.diving && !$PokemonGlobal.surfing && !$PokemonGlobal.rockclimbing
        charset = 1   # Display normal character sprite
        if pbCanRun? && (moving? || @wasmoving) && Input.dir4!=0 && meta[4] && meta[4]!=""
          charset = 4   # Display running character sprite
        end
        newCharName = pbGetPlayerCharset(meta,charset)
        @character_name = newCharName if newCharName
        @wasmoving = moving?
      end
    end
    return @character_name
  end
end

class Game_Character
  attr_accessor :always_on_top
end