#==============================================================================#
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#
#==============================================================================#
#                          Rock Climb with Animation                           #
#                                    v1.1                                      #
#                             By Ulithium_Dragon                               #
#                     Edited by DarrylBD99 for V19 support                     #
#==============================================================================#
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#
#==============================================================================#
#          Implements Rock Climbing and plays a custom animation               #
#             when using the Rock Climb HM in the overworld.                   #
#------------------------------------------------------------------------------#
#    Place the Rock Climb graphics on the tileset you want to use them with    #
#      (You will need to do this manually using an image editing program)      #
#                                                                              #
#         Change the terrain tags of the tiles you added with numbers          #
#       used in line 172 and line 179 [make sure to change it if needed]       #
#                                                                              #
#==============================================================================#
#   If you want to have Rock Climb as an HM, you will need to add a new item   #
#   to your "items.txt" PSB file...                                            #
#                                                                              #
#-----------#                                                                  #
#| Example: |                                                                  #
#-----------#                                                                  #
#634,HM09,HM09,HM09s,4,0,"A charging attack that may also leave the foe confused. It can also be used to scale rocky walls.",4,0,0,ROCKCLIMB
#                                                                              #
#------------------------------------------------------------------------------#
#           in the provided pbs file "tm.txt" underneath [ROCKCLIMB]           #
#       add the Pokemon names (in all caps) you want to learn Rock Climb       #
#                                                                              #
#-----------#                                                                  #
#| Example: |                                                                  #
#-----------#                                                                  #
=begin
[ROCKCLIMB]
VENUSAUR,BLASTOISE,SANDSHREW,SANDSLASH,NIDOQUEEN,NIDOKING,GOLDUCK,MANKEY,PRIMEAPE,ARCANINE,POLIWRATH,MACHOP,MACHOKE,MACHAMP,GEODUDE,GRAVELER,GOLEM,ONIX,CUBONE,MAROWAK,HITMONLEE,HITMONCHAN,LICKITUNG,RHYHORN,RHYDON,CHANSEY,KANGASKHAN,ELECTABUZZ,MAGMAR,PINSIR,TAUROS,OMASTAR,KABUTOPS,SNORLAX,MEWTWO,MEW,MEGANIUM,TYPHLOSION,FERALIGATR,AMPHAROS,STEELIX,GRANBULL,URSARING,BLISSEY,RAIKOU,ENTEI,SUICUNE,TYRANITAR,SCEPTILE,BLAZIKEN,SWAMPERT,LUDICOLO,VIGOROTH,SLAKING,EXPLOUD,MAKUHITA,HARIYAMA,AGGRON,ZANGOOSE,REGIROCK,REGICE,REGISTEEL,GROUDON,TURTWIG,GROTLE,TORTERRA,CHIMCHAR,MONFERNO,INFERNAPE,EMPOLEON,BIBAREL,CRANIDOS,RAMPARDOS,GIBLE,GABITE,GARCHOMP,MUNCHLAX,LUCARIO,DRAPION,CROAGUNK,TOXICROAK,ABOMASNOW,LICKILICKY,RHYPERIOR,ELECTIVIRE,MAGMORTAR,MAMOSWINE,HEATRAN,REGIGIGAS,GIRATINA,DARKRAI,ARCEUS
=end
#------------------------------------------------------------------------------#
# make sure to hold CTRL for a fully recompile to add the tms into pokemon.txt #
#------------------------------------------------------------------------------#


#==============================================================================#
#                                   SETTINGS                                   #
#==============================================================================#
module RockClimbSettings
  # Animation IDs from visible encounters
  ROCK_CRUMBLE_ANIMATION_ID   = 19  #Rock Climb Rocks animation
  ROCK_DUST_ANIMATION_UP_ID   = 20  #Rock CLimb Dusts animation UP
  ROCK_DUST_ANIMATION_DOWN_ID = 21  #Rock CLimb Dusts animation DOWN

  # The badge needed to use Rock Climb in the field.                           #
  BADGE_FOR_ROCKCLIMB    = 8   #Default: 8

  # Descending does not have a custom animation, and was not something that    #
  #  Rock Climb ever did. If you want to use descending, set this to "true".   #
  ENABLE_DESCENDING = true  #Default: false
end
#==============================================================================#
