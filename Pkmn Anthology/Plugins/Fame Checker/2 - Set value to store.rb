# This is examples

# Store famous person
# Form: (name, hash)
# 'name' defines this person, must be unique
# 'hash' defines properties of this person
# 
# Value of hash:
#
# :name      -> Show name in list
# :graphic   -> Store in 'Graphics\Pictures\FameChecker\Character'
# :order     -> Order when you complete this feature
# :introduce -> Introduce this person
FameChecker.list("Player1", {
	:name => "BROCK", # Show name in list
	:graphic => "Brock", # Store in 'Graphics\Pictures\FameChecker\Character'
	:order => 2, # Order when you complete this feature
	# Add it in array, introduce this person
	:introduce => [
		"From: BROCK",
		"In this big world of ours, there must be many tough Trainers.",
		"Let's both keep training and making ourselves stronger!",
		"From: BROCK"	
	]
})

# Store information of this famous person
# Quantity of this information is equal to 6
#  -> Add it 6 times
#  Set this is first -> Show first in this script
#
# Form: (name, hash)
# 'name' defines this person, must write like in 'FameChecker.list'
# 'hash' defines properties of this information
# 
# Value of hash:
#
# :title     -> Title of this information, this size is less than 3 and greater than 0
# :graphic   -> Store in 'Graphics\Pictures\FameChecker\Information'
# :introduce -> Introduce this small information

# First
FameChecker.infor("Player1", {
	:title => ["PEWTER CITY","TEST 1"], # Title of this information, this size is less than 3 and greater than 0
	:graphic => "NPC 05", # Store in 'Graphics\Pictures\FameChecker\Information'
	# Add it in array, introduce this small information
	:introduce => [
		"What does this person do?",
		"...",
		"Test 1"
	]
})
# Second
FameChecker.infor("Player1", {
	:title => ["PEWTER GYM","TEST 2"],
	:graphic => "NPC 10",
	:introduce => [
		"What is this person like?",
		"Ah! I think...",
		"Test 2"
	]
})
# Third
FameChecker.infor("Player1", {
	:title => ["PEWTER CITY"],
	:graphic => "NPC 04",
	:introduce => [
		"Family and friends?",
		"I think...",
		"Test 3"
	]
})
# Fourth
FameChecker.infor("Player1", {
	:title => ["ROUTE 4","TEST 4"],
	:graphic => "NPC 09",
	:introduce => [
		"Family and friends?",
		"...",
		"Hmm...",
		"Test 4"
	]
})
# Fifth
FameChecker.infor("Player1", {
	:title => ["MT.MOON","TEST 5"],
	:graphic => "NPC 03",
	:introduce => [
		"What is this person like?",
		"Uhm...I think...",
		"Test 5"
	]
})
# Sixth
FameChecker.infor("Player1", {
	:title => ["PEWTER MUSEUM","TEST 6"],
	:graphic => "NPC 08",
	:introduce => [
		"What does this person do?",
		"...",
		"Test 6"
	]
})
#-------------------------------------#
#            New example              #
#-------------------------------------#
FameChecker.list("Giovanni", {
	:name => "GIOVANNI",
	:graphic => "Giovanni",
	:order => 1, # Change order
	:introduce => [
		"From: GIOVANNI",
		"There is nothing that I wish to say to you.",
		"I will concentrate solely on battering myself, and none other."
	]
})

# First
FameChecker.infor("Giovanni", {
	:title => ["ROCKET HIDEOUT"],
	:graphic => "NPC 02",
	:introduce => [
		"What does this person do?",
		"...",
		"Test 1"
	]
})
# Second
FameChecker.infor("Giovanni", {
	:title => ["SILPH CO.","TEST 2"],
	:graphic => "NPC 07",
	:introduce => [
		"What is this person like?",
		"Ah! I think...",
		"Test 2"
	]
})
# Third
FameChecker.infor("Giovanni", {
	:title => ["SILPH CO."],
	:graphic => "NPC 01",
	:introduce => [
		"Family and friends?",
		"I think...",
		"Test 3"
	]
})
# Fourth
FameChecker.infor("Giovanni", {
	:title => ["ROCKET WAREHOUSE"],
	:graphic => "NPC 06",
	:introduce => [
		"Family and friends?",
		"...",
		"Hmm...",
		"Test 4"
	]
})
# Fifth
FameChecker.infor("Giovanni", {
	:title => ["VIRIDIAN GYM","TEST 5"],
	:graphic => "NPC 03",
	:introduce => [
		"What is this person like?",
		"Uhm...I think...",
		"Test 5"
	]
})
# Sixth
FameChecker.infor("Giovanni", {
	:title => ["VIRIDIAN GYM"],
	:graphic => "NPC 08",
	:introduce => [
		"What does this person do?",
		"...",
		"Test 6"
	]
})
#-------------------------------------#
#                End                  #
#-------------------------------------#

# Add =begin in the first line and =end above this line or change information in these examples
# After that, add new below this