module FameChecker
	class Show

		# Draw background
		def drawScene
			@scene = 4 if @person.size<=0
			return if @sprites["bg"]
			# Background
			self.create_sprite("bg","Scene_#{@scene}",@viewport)
			# List
			self.create_sprite("list","List",@viewport)
			x = 14
			y = 46
			self.set_xy_sprite("list", x, y)
			@sprites["list"].z = -2
			# Bitmap of list
			self.create_sprite_2("list bitmap", @viewport)
			@sprites["list bitmap"].z = -1
			self.clearTxt("list bitmap")
			# Information (hide)
			self.create_sprite("hide white","Information_White",@viewport)
			self.set_xy_sprite("hide white", PosHide[0], PosHide[1])
			@sprites["hide white"].z = -3
			self.create_sprite_2("hide bitmap",@viewport)
			@sprites["hide bitmap"].z = -2
			self.clearTxt("hide bitmap")
			2.times { |i|
				self.create_sprite("hide window #{i}","Information_Hide",@viewport)
				w = @sprites["hide window #{i}"].bitmap.width
				h = @sprites["hide window #{i}"].bitmap.height / 2
				self.set_src_wh_sprite("hide window #{i}", w, h)
				y = h * i
				self.set_src_xy_sprite("hide window #{i}", 0, y)
				x = PosHide[0]
				y = PosHide[1] + h * i
				self.set_xy_sprite("hide window #{i}", x, y)
				@sprites["hide window #{i}"].z = -1
			}
		end
		
		# Change scene
		def changeScene(folder="Pictures/FameChecker/Machine")
			return if @scene==@oldscene
			@oldscene = @scene
			@sprites["bg"].bitmap = Bitmap.new("Graphics/#{folder}/Scene_#{@scene}")
		end

		# Show name of person
		def showList
			# Clear
			self.clearTxt("list bitmap")
			# Set position
			max = @person.size
			maxshow = self.maxShowPerson
			if max > 0 && max < maxshow
				pos = 0
			else
				if @posperson < maxshow / 2
					pos = 0
				elsif @posperson >= maxshow / 2 && @posperson < max - maxshow / 2
					pos = @posperson - (maxshow / 2 - 1)
				else
					pos = max - maxshow
				end
			end
			endnum = (max > 0 && max < maxshow)? max : maxshow
			bitmap = @sprites["list bitmap"].bitmap
			# Text
			text = []
			endnum.times { |i|
				string = "#{FameChecker.rList[@person[pos+i]][:name]}"
				x = @sprites["list"].x + 5
				y = @sprites["list"].y + (20 + 8) * i - 10
				text << [string, x, y, 0, Color.new(0,0,0), Color.new(255,255,255)]
			}
			self.drawTxt("list bitmap", text)
		end

		# Show arrow list
		def arrowList
			# Set y
			y = @sprites["list"].y - 10 + 20 / 2.0 + (20 + 8) * self.posArrow
			if @sprites["arrow list"]
				# Set src
				x = @choseperson ? @sprites["arrow list"].bitmap.width / 2 : 0
				self.set_src_xy_sprite("arrow list", x, 0)
				# Set y
				@sprites["arrow list"].y = y
				return
			end
			# Create arrow
			self.create_sprite("arrow list","Choice_Arrow",@viewport)
			w = @sprites["arrow list"].bitmap.width / 2
			h = @sprites["arrow list"].bitmap.height
			self.set_src_wh_sprite("arrow list", w, h)
			self.set_src_xy_sprite("arrow list", 0, 0)
			x = @sprites["list"].x + 2 - w
			self.set_xy_sprite("arrow list", x, y)
		end

		def signal
			# Create
			if !@sprites["signal 0"]
				2.times { |i|
					self.create_sprite("signal #{i}","Order_Arrow",@viewport)
					w = @sprites["signal #{i}"].bitmap.width / 2
					h = @sprites["signal #{i}"].bitmap.height
					self.set_src_wh_sprite("signal #{i}", w, h)
					self.set_src_xy_sprite("signal #{i}", w * i, 0)
					x = 14 + (@sprites["list"].bitmap.width - @sprites["signal #{i}"].src_rect.width) / 2
					y = 46 + @sprites["list"].bitmap.height * i - @sprites["signal #{i}"].src_rect.height / 2
					self.set_xy_sprite("signal #{i}", x, y)
					self.set_visible_sprite("signal #{i}")
				}
			end
			return if @person.size <= self.maxShowPerson
			self.set_visible_sprite("signal 0", @posperson > 2)
			self.set_visible_sprite("signal 1", @posperson < @person.size - self.maxShowPerson + 2)
		end

		# Draw information (casette)
		def drawBigImage
			return if @person.size<=0
			return if @sprites["casette"]
			# Casette
			self.create_sprite("casette","Screen",@viewport)
			x = Graphics.width
			y = 40
			self.set_xy_sprite("casette", x, y)
			# Circle
			self.create_sprite("casette circle","Ball",@viewport)
			ox = @sprites["casette circle"].bitmap.width / 2
			oy = @sprites["casette circle"].bitmap.height / 2
			self.set_oxoy_sprite("casette circle", ox, oy)
			# Character
			self.setCharacter
		end

		# Set bitmap to show character
		define_method(:setCharacter) {
			filename = "#{FameChecker.rList[@person[@posperson]][:graphic]}"
			@sprites["character"] = Sprite.new(@viewport) if !@sprites["character"]
			self.set_sprite_character("character",filename)
			# Set x, y
			self.set_xy_casette
		}

		# Set position of circle, character on casette
		define_method("set_xy_casette".to_sym) {
			# Casette circle
			x = @sprites["casette"].x + 278 + @sprites["casette circle"].ox
			y = @sprites["casette"].y + 71  + @sprites["casette circle"].oy
			self.set_xy_sprite("casette circle", x, y)
			# Set character
			x = @sprites["casette"].x + 42 + 208 / 2 - @sprites["character"].bitmap.width / 2
			y = @sprites["casette"].y + 24 + 138 / 2 - @sprites["character"].bitmap.height / 2
			self.set_xy_sprite("character", x, y)
		}

		# Move 'casette'
		def moveBigImage(right=false)
			loop do
				# Update
				self.update_ingame
				# Bitmap
				m = @sprites["casette"].bitmap.width / 20
				if right
					if @sprites["casette"].x >= Graphics.width
						@sprites["casette"].x = Graphics.width
						self.set_xy_casette
						break
					end
					# Move
					@sprites["casette"].x += m
					@sprites["casette circle"].x += m
					@sprites["character"].x += m
				else
					x = Graphics.width - @sprites["casette"].bitmap.width
					if @sprites["casette"].x <= x + 10
						@sprites["casette"].x = x
						self.set_xy_casette
						break
					end
					# Move
					@sprites["casette"].x -= m
					@sprites["casette circle"].x -= m
					@sprites["character"].x -= m
				end
			end
		end

		# Show introduce of person
		def introduce
			FameChecker.rList[@person[@posperson]][:introduce].each { |i| pbMessage(_INTL("#{i}")) { @sprites["casette circle"].angle += 10 } }
		end

		# Information
		def showInfor
			return if @person.size<=0
			6.times { |i|
				@sprites["infor #{i}"] = Sprite.new(@viewport) if !@sprites["infor #{i}"]
				hide = $PokemonGlobal.inforFame[@person[@posperson]]
				x = PosInformation[0]
				y = PosInformation[1]
				if hide && hide.include?(i)
					filename = FameChecker.rInfor[@person[@posperson]][i][:graphic]
					self.set_sprite_information("infor #{i}",filename)
					w = @sprites["infor #{i}"].bitmap.width / 4
					h = @sprites["infor #{i}"].bitmap.height / 4
					self.set_src_wh_sprite("infor #{i}", w, h)
					self.set_src_xy_sprite("infor #{i}", 0, 0)
				else
					self.set_sprite_machine("infor #{i}","Hide")
				end
				disx = (340 - @sprites["infor #{i}"].src_rect.width * 3) / 4
				x += disx + (disx + @sprites["infor #{i}"].src_rect.width) * (i%3)
				disy = (92 - @sprites["infor #{i}"].src_rect.height * 2) / 3
				y += disy + (i>2 ? @sprites["infor #{i}"].src_rect.height + disy : 0)
				self.set_xy_sprite("infor #{i}", x, y)
			}
		end

		# Set animate of information
		define_method(:setAnimate) {
			if !@choseperson
				6.times { |i| self.set_src_xy_sprite("infor #{i}", 0, 0) }
				return
			end
			6.times { |i|
				next if i==@posinfor
				self.set_src_xy_sprite("infor #{i}", 0, 0)
			}
			return if !$PokemonGlobal.inforFame[@person[@posperson]] || !$PokemonGlobal.inforFame[@person[@posperson]].include?(@posinfor) || @animation%4!=0
			x = @sprites["infor #{@posinfor}"].src_rect.x + @sprites["infor #{@posinfor}"].bitmap.width / 4
			x = 0 if x > @sprites["infor #{@posinfor}"].bitmap.width * 3 / 4
			self.set_src_xy_sprite("infor #{@posinfor}", x, 0)
		}

		# Set information (title)
		def showTitle
			self.clearTxt("hide bitmap")
			return if !@choseperson
			hide = $PokemonGlobal.inforFame[@person[@posperson]]
			return unless hide && hide.include?(@posinfor)
			text = []
			title = FameChecker.rInfor[@person[@posperson]][@posinfor][:title]
			title.size.times { |i|
				string = title[i]
				x = 230 + @sprites["hide window 0"].bitmap.width / 2
				disy = (@sprites["hide window 0"].bitmap.height - 20 * 2) / 3
				y = 156 + disy + (disy + 20) * i - 11
				text << [string, x, y, 0, Color.new(0,0,0), Color.new(255,255,255)]
			}
			self.drawTxt("hide bitmap", text, nil, nil, 1)
		end

		# Set rectangle 'choose'
		def chooseRect
			if !@choseperson
				self.set_visible_sprite("choose rect") if @sprites["choose rect"]
				return
			end
			self.create_sprite("choose rect","Select_square",@viewport) if !@sprites["choose rect"]
			self.set_visible_sprite("choose rect",true)
			x = @sprites["infor #{@posinfor}"].x - (@sprites["choose rect"].bitmap.width - @sprites["infor #{@posinfor}"].src_rect.width) / 2
			y = @sprites["infor #{@posinfor}"].y - (@sprites["choose rect"].bitmap.height - @sprites["infor #{@posinfor}"].src_rect.height) / 2
			self.set_xy_sprite("choose rect", x, y)
		end

		# Move hide
		def moveHide
			hide = $PokemonGlobal.inforFame[@person[@posperson]]
			# Show title
			self.showTitle
			# Move
			m = (@sprites["hide window 0"].bitmap.height / 2) / 3.0
			loop do
				# Update
				self.update_ingame
				# Break 
				if hide && hide.include?(@posinfor)
					if @sprites["hide window 0"].y <= PosHide[1] - @sprites["hide window 0"].src_rect.height
						@sprites["hide window 0"].y = PosHide[1] - @sprites["hide window 0"].src_rect.height
						@sprites["hide window 1"].y = PosHide[1] + 2 * @sprites["hide window 0"].src_rect.height
						break
					end
					@sprites["hide window 0"].y -= m
					@sprites["hide window 1"].y += m
				elsif !hide || !hide.include?(@posinfor)
					if @sprites["hide window 0"].y >= PosHide[1] - 10
						2.times { |i| @sprites["hide window #{i}"].y = PosHide[1] + @sprites["hide window #{i}"].src_rect.height * i}
						break
					end
					@sprites["hide window 0"].y += m
					@sprites["hide window 1"].y -= m
				end
			end
		end

		# Read information
		def readInfor
			hide = $PokemonGlobal.inforFame[@person[@posperson]]
			return if !hide || !hide.include?(@posinfor)
			FameChecker.rInfor[@person[@posperson]][@posinfor][:introduce].each { |i|
				pbMessage(_INTL("#{i}")) { 
					@animation += 1
					self.setAnimate
				} 
			}
		end

	end
end