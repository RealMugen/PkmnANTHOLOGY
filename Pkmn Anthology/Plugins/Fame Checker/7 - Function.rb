module FameChecker
	class Show

		# Maximum number of persons can show
		def maxShowPerson; return 6; end

		# Position of arrow
		def posArrow
			max = @person.size
			maxshow = self.maxShowPerson
			return @posperson if max > 0 && max < maxshow
			return @posperson if @posperson < maxshow / 2
			return maxshow / 2 - 1 if @posperson < max - maxshow / 2
			return (maxshow - 1) - ((max - 1) - @posperson)
		end

		# Moved 'casette'
		def movedBigImage(right=false)
			if right
				return true if @sprites["casette"].x >= Graphics.width
			else
				return true if @sprites["casette"].x <= (Graphics.width-@sprites["casette"].bitmap.width)
			end
			return false
		end

		# Check input when choosing person
		def inputPerson
			return if @choseperson || @introduce
			max = @person.size
			maxshow = self.maxShowPerson
			# Break and exit
			@exit = true if self.checkInput(Input::BACK)
			# Next, see information
			if self.checkInput(Input::USE)
				@choseperson = true
				# Move 'Hide'
				self.moveHide
			end
			# See 'introduction'
			if self.checkInput(Input::ACTION)
				# Character
				self.setCharacter
				# Set again bitmap
				self.setCharacter
				# Move image
				self.moveBigImage if !self.movedBigImage
				@introduce = true
			# Up / Down
			elsif self.checkInput(Input::UP)
				@posperson -= 1
				@posperson  = @person.size - 1 if @posperson < 0
				# Show information
				self.showInfor
			elsif self.checkInput(Input::DOWN)
				@posperson += 1
				@posperson  = 0 if @posperson >= @person.size
				# Show information
				self.showInfor
			end
		end

		# Check input when choosing information
		def inputInfor
			return if !@choseperson || @introduce
			# Return first scene
			if self.checkInput(Input::BACK)
				# Reset position
				@posinfor = 0
				@choseperson = false
			end
			# Show information
			self.readInfor if self.checkInput(Input::USE)
			# Up / Down / Left / Right
			if self.checkInput(Input::UP)
				@posinfor -= 3
				@posinfor += 6 if @posinfor<0
				# Move 'hide'
				self.moveHide
			elsif self.checkInput(Input::DOWN)
				@posinfor += 3
				@posinfor -= 6 if @posinfor>=6
				# Move 'hide'
				self.moveHide
			elsif self.checkInput(Input::LEFT)
				@posinfor -= 1
				@posinfor  = 5 if @posinfor<0
				# Move 'hide'
				self.moveHide
			elsif self.checkInput(Input::RIGHT)
				@posinfor += 1
				@posinfor  = 0 if @posinfor>=6
				# Move 'hide'
				self.moveHide
			end
		end

		# Check input when introducing
		def inputIntroduce
			return if !@introduce
			# Cancel
			if self.checkInput(Input::BACK)
				# Move image
				self.moveBigImage(true) if !self.movedBigImage(true)
				@sprites["casette circle"].angle = 0
				@introduce = false
			end
			# Play introduction
			self.introduce if self.checkInput(Input::USE)
		end

	end
end