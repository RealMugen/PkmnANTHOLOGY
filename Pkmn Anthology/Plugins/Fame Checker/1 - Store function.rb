class PokemonGlobalMetadata
	attr_accessor :personFame
	attr_accessor :inforFame

  alias fame_checker_ini initialize
  def initialize
    fame_checker_ini
		# Set seen
		@personFame = {}
		@inforFame  = {}
  end

end

module FameChecker
	@@list  = {}
	@@infor = {}

	def self.error(error=nil)
		p error
		Kernel.exit!
	end

	# Set list
	def self.list(name=nil,hash=nil)
		return if name.nil?
		if !hash.is_a?(Hash)
			self.error("Use #{name} with Hash! Please, read or see examples!")
			return
		end
		@@list[name.to_sym] = hash
	end

	# Set 'can check' list
	def self.seenList(name=nil,seen=true)
		return if name.nil?
		if !@@list[name.to_sym]
			self.error("Set this #{name} with method FameChecker.list() to use")
			return
		end
		$PokemonGlobal.personFame[name.to_sym] = seen
	end

	# Set informations
	def self.infor(name=nil,hash=nil)
		return if name.nil?
		if !hash.is_a?(Hash)
			self.error("Use #{name} with Hash! Please, read or see examples!")
			return
		end
		if !@@list[name.to_sym]
			self.error("Set this #{name} with method FameChecker.list() to use")
			return
		end
		@@infor[name.to_sym] = [] if !@@infor[name.to_sym]
		@@infor[name.to_sym] << hash
	end

	# Set 'can check' informations of list
	def self.seenInfor(name=nil,id=nil)
		return if name.nil? || id.nil?
		if !id.is_a?(Numeric)
			self.error("Id is number! Check this event again!")
			return
		end
		if id<0 || id>5
			self.error("Please, set id in the range 0-5!")
			return
		end
		if !@@list[name.to_sym]
			self.error("Set this #{name} with method FameChecker.list() to use")
			return
		end
		$PokemonGlobal.inforFame[name.to_sym] = [] if !$PokemonGlobal.inforFame[name.to_sym]
		$PokemonGlobal.inforFame[name.to_sym] << id
	end

	def self.rList; @@list; end
	def self.rInfor; @@infor; end
end