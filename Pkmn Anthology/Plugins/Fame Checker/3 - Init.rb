#===============================================================================
# Fame Checker by bo4p5687
# Graphics by Richard PT
#===============================================================================

# Add item
ItemHandlers::UseFromBag.add(:FAMECHECKER, proc { |item|
	next (FameChecker.show) ? 1 : 0
})

# Script
module FameChecker

	class Show
		# Position of bitmap information (small), [x,y]
		PosInformation = [178, 52]
		# Position of bitmap hide, [x,y]
		PosHide = [230,156]

		def initialize
			@sprites = {}
			# Viewport
      @viewport = Viewport.new(0,0,Graphics.width,Graphics.height)
      @viewport.z = 99999
			# Value
			# Changing scene depends progress
			@scene = 1
			@oldscene = @scene
			# Check quantity of person
			@person = []
			person = {}
			order = []
			$PokemonGlobal.personFame.each { |k,v| order << k if v } if $PokemonGlobal.personFame.size > 0
			number = []
			FameChecker.rList.each { |k,v|
				order.each { |i|
					next if i!=k
					number << v[:order]
					person[k] = v[:order]
				}
			}
			# Sort
			check = number.uniq!
			FameChecker.error("Check order number!") if !check.nil?
			person = person.sort_by(&:last)
			# Store person
			person.each { |i| @person << i[0] }
			# Check quantity of information
			@infor = {}
			$PokemonGlobal.inforFame.each { |k,v| @infor[k] = v } if $PokemonGlobal.inforFame.size > 0
			# Set position
			# Choose person
			@posperson = 0
			# Choose information
			@posinfor = 0
			# Set 'chose'
			# Person
			@choseperson = false
			# Introduce
			@introduce = false
			# Set animation
			@animation = 0
			# Trigger exit
			@exit = false
		end
		
	end

	def self.show
		pbFadeOutIn {
			f = Show.new
			f.show
			f.endScene
		}
	end

end