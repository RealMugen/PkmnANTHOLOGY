module FameChecker
	class Show

		def show
			loop do
				# Update
				self.update_ingame
				# Show
				self.progress
				break if @exit
			end
		end

		def progress
			# Draw background
			self.drawScene
			# Show information
			self.showInfor
			# Draw 'casette'
			self.drawBigImage
			# Check quantity of person
			if @person.size<=0
				@exit = true if self.checkInput(Input::BACK)
				return
			end
			# Progress - chose person
			self.choosePeson
		end
		
		def choosePeson
			loop do
				# Update
				self.update_ingame
				# Check scene and change it
				@scene = @choseperson ? 2 : @introduce ? 3 : 1
				self.changeScene
				# Show list to choose
				self.showList
				# Show arrow list (change graphics)
				self.arrowList
				# Show signal
				self.signal
				# Rectangle 'choose'
				self.chooseRect
				# Set animate
				@animation += 1
				self.setAnimate
				# Input
				self.inputPerson
				self.inputInfor
				self.inputIntroduce
				# Break
				break if @exit
			end
		end

	end
end