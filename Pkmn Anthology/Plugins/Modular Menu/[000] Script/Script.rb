#===============================================================================
#  Modular Pause Menu
#    by Luka S.J.
# ----------------
#  Provides only features present in the default version of the Pokedex in
#  Essentials. Mean as a new cosmetic overhaul, adhering to the UI design
#  language of EBS
#-------------------------------------------------------------------------------
#  Main module for handling each menu item/entry
#===============================================================================
module ModularMenu
  # hash used to store the elements inside of the menu
  @@menuEntry = {}
  # hash used to store whether or not an element is unlocked
  @@available = {}
  # hash used to store the index of each element; for sorting
  @@indexes = {}
  @@index = 0
  #-----------------------------------------------------------------------------
  # function to add a new element/entry to the menu
  #-----------------------------------------------------------------------------
  def self.add_entry(ref, name, icon, &block)
    raise "No function code block defined for Modular Menu entry: #{name}." if block.nil?
    @@menuEntry[ref] = [name, icon, block]
    @@available[ref] = true if !@@available.keys.include?(ref)
    @@indexes[ref] = @@index
    @@index += 1
  end
  #-----------------------------------------------------------------------------
  # function to add a conditional for an existing entry
  #-----------------------------------------------------------------------------
  def self.add_condition(ref, &block)
    raise "No condition code block defined for Modular Menu entry: #{name}." if block.nil?
    @@available[ref] = block
  end
  #-----------------------------------------------------------------------------
  # function to get the name of an element/entry
  #-----------------------------------------------------------------------------
  def self.name(ref)
    return @@menuEntry[ref][0]
  end
  #-----------------------------------------------------------------------------
  # function to get the icon of an element/entry
  #-----------------------------------------------------------------------------
  def self.icon(ref)
    return "Graphics/Icons/#{@@menuEntry[ref][1]}"
  end
  #-----------------------------------------------------------------------------
  # function to get all the possible keys from the main hash
  #-----------------------------------------------------------------------------
  def self.keys
    entries = Array.new(@@menuEntry.keys.length)
    for key in @@menuEntry.keys
      entries[@@indexes[key]] = key
    end
    return entries
  end
  #-----------------------------------------------------------------------------
  # function used to invoke the stored code for each element/entry
  #-----------------------------------------------------------------------------
  def self.run(ref, scene)
    @@menuEntry[ref][2].call(scene)
  end
  #-----------------------------------------------------------------------------
  # function to check if the player has access to an element/entry
  #-----------------------------------------------------------------------------
  def self.available?(ref)
    return @@available[ref].is_a?(Proc) ? @@available[ref].call : @@available[ref]
  end
  #-----------------------------------------------------------------------------
  # function that lists all accessible menu elements/entries
  #-----------------------------------------------------------------------------
  def self.elements?
    ent = self.keys
    items = 0
    for val in ent
      items += 1 if self.available?(val)
    end
    return items
  end
  #-----------------------------------------------------------------------------
end
#-------------------------------------------------------------------------------
#  Main class used to handle the visuals
#-------------------------------------------------------------------------------
class PokemonPauseMenu_Scene
  attr_accessor :index
  attr_accessor :entries
  attr_accessor :endscene
  attr_accessor :close
  attr_accessor :hidden
  
  Color2 = Color.new(255,255,255)
  ColorMed = Color.new(255,182,0)
  ColorDanger = Color.new(191,57,57)
  ColorCold = Color.new(103,167,255)
  ColorHeat = Color.new(255,144,54)
  Shadow = Color.new(20,20,20)
  Shadow2 = Color.new(40,40,40)
  #-----------------------------------------------------------------------------
  # retained for compatibility
  #-----------------------------------------------------------------------------
  def pbShowInfo(text)
    @sprites["helpwindow"].resizeToFit(text, Graphics.height)
    @sprites["helpwindow"].text = text
    @sprites["helpwindow"].visible = true
    @helpstate = true
    pbBottomLeft(@sprites["helpwindow"])
  end
  #-----------------------------------------------------------------------------
  # retained for compatibility
  #-----------------------------------------------------------------------------
  def pbShowHelp(text)
    @sprites["helpwindow"].resizeToFit(text, Graphics.height)
    @sprites["helpwindow"].text = text
    @sprites["helpwindow"].visible = true
    @helpstate = true
    pbBottomLeft(@sprites["helpwindow"])
  end
  #-----------------------------------------------------------------------------
  # main scene generation
  #-----------------------------------------------------------------------------
  def pbStartScene
    # sets the default index
    @index = $PokemonTemp.menuLastChoice.nil? ? 0 : $PokemonTemp.menuLastChoice
    @index = 0 if @index >= ModularMenu.elements?
    @oldindex = 0
    @endscene = true
    @close = false
    @hidden = false
    # loads the visual parts of the
    @viewport = Viewport.new(0,0, Graphics.width, Graphics.height)
    @viewport.z = 99999
    @sprites = {}
    # initializes the background graphic
    @bitmap = Graphics.snap_to_bitmap if !@bitmap
    @sprites["background"] = Sprite.new(@viewport)
    @sprites["background"].bitmap = @bitmap
    @sprites["background"].blur_sprite(3)
    @sprites["background"].bitmap.blt(0, 0, pbBitmap("Graphics/Pictures/PauseMenu/bg"),Rect.new(0, 0, Graphics.width, Graphics.height))
    bmp = pbBitmap("Graphics/Pictures/Common/scrollbar_bg")
    @sprites["background"].bitmap.blt(Graphics.width - 28, (Graphics.height - bmp.height)/2, bmp, Rect.new(0, 0, bmp.width, bmp.height))
    # initializes the scrolling panorama
    @sprites["panorama"] = ScrollingSprite.new(@viewport)
    @sprites["panorama"].setBitmap("Graphics/Pictures/Common/panorama")
    @sprites["panorama"].speed = 1
    # retained for compatibility
    @sprites["infowindow"] = Window_UnformattedTextPokemon.newWithSize("", 0, 0, 32, 32, @viewport)
    @sprites["infowindow"].visible = false
    @sprites["helpwindow"] = Window_UnformattedTextPokemon.newWithSize("", 0, 0, 32, 32, @viewport)
    @sprites["helpwindow"].visible = false
    # draw the contest crap
    @sprites["textOverlay"] = Sprite.new(@viewport)
    @sprites["textOverlay"].bitmap = Bitmap.new(@viewport.rect.width, @viewport.rect.height)
    @sprites["textOverlay"].end_x = 0
    @sprites["textOverlay"].x = -@viewport.rect.width
    pbSetSystemFont(@sprites["textOverlay"].bitmap)
    bmp = pbBitmap("Graphics/Pictures/Common/partyBar")
    content = []
    text = []
    if pbInSafari?
      content.push(_INTL("Pasos: {1}/{2}", pbSafariState.steps, Settings::SAFARI_STEPS)) if Settings::SAFARI_STEPS > 0
      content.push(_INTL("Balls: {1}", pbSafariState.ballcount))
    elsif pbInBugContest?
      if pbBugContestState.lastPokemon
        content.push(_INTL("Capturados: {1}", PBSpecies.getName(pbBugContestState.lastPokemon.species)))
        content.push(_INTL("Niveles: {1}", pbBugContestState.lastPokemon.level))
        content.push(_INTL("Balls: {1}", pbBugContestState.ballcount))
      else
        content.push("Capturas: Ninguna")
      end
      content.push(_INTL("Balls: {1}", pbBugContestState.ballcount))
    end
    for i in 0...content.length
      text.push([content[i], 16, 54 + i*50, 0, Color.new(255, 255, 255), Color.new(0, 0, 0, 65)])
      @sprites["textOverlay"].bitmap.blt(-2, 92 + i*50, bmp, Rect.new(0, 0, bmp.width, bmp.height))
    end
    pbDrawTextPositionsOLD(@sprites["textOverlay"].bitmap, text)
    # Draw collective RED FEATHER
    @sprites["textOverlay2"]=BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["textOverlay2"].z=999999
    pbSetFont(@sprites["textOverlay2"].bitmap,"Barlow Condensed",22)
    @sprites["textOverlay4"]=BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["textOverlay4"].z=999999
    pbSetFont(@sprites["textOverlay4"].bitmap,"Barlow Condensed",22)
    @sprites["textOverlay5"]=BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["textOverlay5"].z=999999
    pbSetFont(@sprites["textOverlay4"].bitmap,"Barlow Condensed",22)
    @sprites["collectibles"]=IconSprite.new(12,124,@viewport)
    @sprites["collectibles"].setBitmap("Graphics/Icons/01PLUMA")
    @sprites["collectibles"].z = @viewport.z
    pbDrawTextPositionsOLD(@sprites["textOverlay2"].bitmap,[
    ["#{$game_variables[97]}",38,140,0,Color2,Shadow]])
    # Draw collective BLUE FEATHER
    @sprites["textOverlay4"]=BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["textOverlay4"].z=999999
    pbSetFont(@sprites["textOverlay4"].bitmap,"Barlow Condensed",22)
    @sprites["collectibles2"]=IconSprite.new(52,124,@viewport)
    @sprites["collectibles2"].setBitmap("Graphics/Icons/02PLUMA")
    @sprites["collectibles2"].z = @viewport.z
    pbDrawTextPositionsOLD(@sprites["textOverlay4"].bitmap,[
    ["#{$game_variables[98]}",78,140,0,Color2,Shadow]])
     # Draw collective YELLOW FEATHER
    @sprites["textOverlay5"]=BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["textOverlay5"].z=999999
    pbSetFont(@sprites["textOverlay5"].bitmap,"Barlow Condensed",22)
    @sprites["collectibles3"]=IconSprite.new(92,124,@viewport)
    @sprites["collectibles3"].setBitmap("Graphics/Icons/03PLUMA")
    @sprites["collectibles3"].z = @viewport.z
    pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[
    ["#{$game_variables[99]}",118,140,0,Color2,Shadow]])
    # Draw Clock
    @sprites["clock"]=IconSprite.new(105,42,@viewport)
    @sprites["clock"].setBitmap("Graphics/Icons/clock") if $game_switches[127]
    @sprites["clock"].z = @viewport.z
    #Draw Day
    @sprites["textOverlay3"]=BitmapSprite.new(Graphics.width,Graphics.height,@viewport)
    @sprites["textOverlay3"].z=999999
    pbSetFont(@sprites["textOverlay3"].bitmap,"Barlow Condensed",22)
    text2 = _INTL("{1}",pbGetTimeNow.strftime("%I:%M %p"))
    pbDrawTextPositionsOLD(@sprites["textOverlay3"].bitmap,[[text2,130,37,0,Color2,Shadow2]]) if $game_switches[127]
    timenow = pbGetTimeNow()
    timestring = timenow.to_s.split
    day=["LUNES","MARTES","MIÉRCOLES","JUEVES","VIERNES","SÁBADO","DOMINGO"][timenow.wday-1]
    #pbDrawTextPositionsOLD(@sprites["textOverlay3"].bitmap,[[pbGetTimeNow.strftime("%A"),130,17,0,Color2,Shadow2]])
    pbDrawTextPositionsOLD(@sprites["textOverlay3"].bitmap,[[day,130,17,0,Color2,Shadow2]]) if $game_switches[127]
    #Drae Trainer Status
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[205]}%",58,174,0,ColorDanger,Shadow]]) if $game_variables[205]<40
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[205]}%",58,174,0,Color2,Shadow]]) if $game_variables[205]>40
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[206]}%",58,214,0,ColorDanger,Shadow]]) if  $game_variables[206]<40
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[206]}%",58,214,0,Color2,Shadow]]) if  $game_variables[206]>40
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[208]}%",58,254,0,ColorDanger,Shadow]]) if  $game_variables[208]<40
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[208]}%",58,254,0,Color2,Shadow]]) if  $game_variables[208]>40
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[207]}°C",58,294,0,Color2,Shadow]]) if  $game_variables[207]<35 && $game_variables[207]>8
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[207]}°C",58,294,0,ColorHeat,Shadow]]) if  $game_variables[207]>=35 
     pbDrawTextPositionsOLD(@sprites["textOverlay5"].bitmap,[["#{$game_variables[207]}°C",58,294,0,ColorCold,Shadow]]) if  $game_variables[207]<=8
    #Draw Season
    @sprites["season"]=IconSprite.new(100,64,@viewport)
    @sprites["season"].setBitmap("Graphics/Icons/spring") if pbIsSpring
    @sprites["season"].setBitmap("Graphics/Icons/winter") if pbIsWinter
     @sprites["season"].setBitmap("Graphics/Icons/summer") if pbIsSummer
    @sprites["season"].setBitmap("Graphics/Icons/autumn") if pbIsAutumn
    @sprites["season"].z = @viewport.z
    #Draw Weather
    @sprites["weather"]=IconSprite.new(130,64,@viewport)
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/weather") if GameData::Weather.get($game_screen.weather_type).category == :None
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/moon") if GameData::Weather.get($game_screen.weather_type).category == :None && PBDayNight.isNight?
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/rain") if GameData::Weather.get($game_screen.weather_type).category == :Rain
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/storm") if GameData::Weather.get($game_screen.weather_type).category == :Storm
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/storm") if GameData::Weather.get($game_screen.weather_type).category == :Thunderstom
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/snow") if GameData::Weather.get($game_screen.weather_type).category == :Snow
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/blizzard") if GameData::Weather.get($game_screen.weather_type).category == :Blizzard
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/sandstorm") if GameData::Weather.get($game_screen.weather_type).category == :Sandstorm
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/storm") if GameData::Weather.get($game_screen.weather_type).category == :HeavyRain
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/sunny") if GameData::Weather.get($game_screen.weather_type).category == :Sun
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/leaves") if GameData::Weather.get($game_screen.weather_type).category == :Leaves
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/fog") if GameData::Weather.get($game_screen.weather_type).category == :Fog
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/moonlight") if GameData::Weather.get($game_screen.weather_type).category == :Moonlight
    @sprites["weather"].setBitmap("Graphics/Pictures/Weather/sakura") if GameData::Weather.get($game_screen.weather_type).category == :Sakura
    @sprites["weather"].z = @viewport.z
    #Draw Trainer Status Icon
    @sprites["hambre"]=IconSprite.new(18,174,@viewport)
    @sprites["hambre"].setBitmap("Graphics/Pictures/Weather/hambre") if $game_variables[205]>=40
     @sprites["hambre"].setBitmap("Graphics/Pictures/Weather/hambre2") if $game_variables[205]<=39
    @sprites["hambre"].z = @viewport.z
    @sprites["sed"]=IconSprite.new(18,214,@viewport)
    @sprites["sed"].setBitmap("Graphics/Pictures/Weather/sed") if $game_variables[206]>=40
    @sprites["sed"].setBitmap("Graphics/Pictures/Weather/sed2") if $game_variables[206]<=39
    @sprites["sed"].z = @viewport.z
    @sprites["sueño"]=IconSprite.new(18,254,@viewport)
    @sprites["sueño"].setBitmap("Graphics/Pictures/Weather/sueño") if $game_variables[208]>=80 && $game_variables[208]<=100
    @sprites["sueño"].setBitmap("Graphics/Pictures/Weather/sueño2") if $game_variables[208]>=50 && $game_variables[208]<=80
    @sprites["sueño"].setBitmap("Graphics/Pictures/Weather/sueño3") if $game_variables[208]>=30 && $game_variables[208]<=50
    @sprites["sueño"].setBitmap("Graphics/Pictures/Weather/sueño4") if $game_variables[208]>=0 && $game_variables[208]<=30
    @sprites["sueño"].z = @viewport.z
    @sprites["temperatura"]=IconSprite.new(18,294,@viewport)
    @sprites["temperatura"].setBitmap("Graphics/Pictures/Weather/calor") if $game_variables[207]>=35
    @sprites["temperatura"].setBitmap("Graphics/Pictures/Weather/frío") if $game_variables[207]<=8
    @sprites["temperatura"].setBitmap("Graphics/Pictures/Weather/tok") if $game_variables[207]<=35 && $game_variables[207]>=8
    @sprites["temperatura"].z = @viewport.z
    #Draw Trainer Team
    @sprites["ash"]=IconSprite.new(18,330,@viewport)
    @sprites["ash"].setBitmap("Graphics/Pictures/Weather/Ash") 
    @sprites["ash"].z = @viewport.z
    @sprites["misty"]=IconSprite.new(50,330,@viewport)
    @sprites["misty"].setBitmap("Graphics/Pictures/Weather/Misty") if $game_switches[116]
    @sprites["misty"].setBitmap("Graphics/Pictures/Weather/MistyNO") if !$game_switches[116]
    @sprites["misty"].z = @viewport.z
    @sprites["brock"]=IconSprite.new(78,330,@viewport)
    @sprites["brock"].setBitmap("Graphics/Pictures/Weather/Brock") if $game_switches[117]
    @sprites["brock"].setBitmap("Graphics/Pictures/Weather/BrockNO") if !$game_switches[117]
    @sprites["brock"].z = @viewport.z
    #Draw Map Name
    pbDrawTextPositionsOLD(@sprites["textOverlay3"].bitmap,[
    ["#{pbGetMapNameFromId($game_map.map_id)}",168,100,1,Color2,Shadow]])
    # initializes the scroll bar
    @sprites["scroll"] = Sprite.new(@viewport)
    # rendering elements on screen
    self.refresh
    self.update
    # memorizes the target opacities and sets them to 0
    @opacities = {}
    for key in @sprites.keys
      @opacities[key] = @sprites[key].opacity
      @sprites[key].opacity = 0
    end
  end
  #-----------------------------------------------------------------------------
  # hide menu
  #-----------------------------------------------------------------------------
  def pbHideMenu
    # animations for closing the menu
    @sprites["textOverlay"].end_x = -@viewport.rect.width
    8.times do
      for key in @sprites.keys
        next if !@sprites[key] || @sprites[key].disposed?
        @sprites[key].opacity -= 32
      end
      @sprites["textOverlay"].x += (@sprites["textOverlay"].end_x - @sprites["textOverlay"].x)*0.2
      Graphics.update
    end
  end
  #-----------------------------------------------------------------------------
  # show menu
  #-----------------------------------------------------------------------------
  def pbShowMenu
    # animations for opening the menu
    @sprites["textOverlay"].end_x = 0
    8.times do
      for key in @sprites.keys
        next if !@sprites[key] || @sprites[key].disposed?
        @sprites[key].opacity += 32 if @sprites[key].opacity < @opacities[key]
      end
      @sprites["textOverlay"].x += (@sprites["textOverlay"].end_x - @sprites["textOverlay"].x)*0.4
      Graphics.update
    end
  end
  #-----------------------------------------------------------------------------
  # refresh content
  #-----------------------------------------------------------------------------
  def refresh
    # index safety
    @index = ModularMenu.elements? - 1 if @index >= ModularMenu.elements?
    @oldindex = @index
    # disposes old items in the menu
    if @entries
      for i in 0...@entries.length
        @sprites["#{i}"].dispose if @sprites["#{i}"]
      end
    end
    # creates a new list of available items
    @entries = []
    for val in ModularMenu.keys
      @entries.push(val) if ModularMenu.available?(val)
    end
    # draws individual item entries
    bmp = pbBitmap("Graphics/Pictures/PauseMenu/sel")
    for i in 0...@entries.length
      key = @entries[i]
      @sprites["#{i}"] = Sprite.new(@viewport)
      @sprites["#{i}"].bitmap = Bitmap.new(bmp.width, bmp.height)
      pbSetFont(@sprites["#{i}"].bitmap,"Bebas Neue",28)
      @sprites["#{i}"].src_rect.set(0, 0, bmp.width/2, bmp.height)
      @sprites["#{i}"].bitmap.blt(0, 0, bmp, Rect.new(0, 0, bmp.width, bmp.height))
      for j in 0...2
        opac = j == 0 ? 155 : 255
        icon = pbBitmap(ModularMenu.icon(key))
        text = ModularMenu.name(key)
        text.gsub!("\\PN", "#{$Trainer.name}")
        text.gsub!("\\JUEGO", pbInSafari? ? "Terminar" : "Terminar Juego")
        @sprites["#{i}"].bitmap.blt(18 + j*bmp.width/2, 6, icon, Rect.new(0, 0, 48, 48), opac)
        pbDrawOutlineText(@sprites["#{i}"].bitmap, 66 + j*bmp.width/2, 18, 136, 48, text, Color.new(255, 255, 255), Color.new(64, 64, 64), 1)
      end
      @sprites["#{i}"].x = Graphics.width - bmp.width/2 - 52
      @sprites["#{i}"].y = 49 + (bmp.height + 12)*i
      @sprites["#{i}"].opacity = 128
    end
    # configures the scroll bar
    n = (@entries.length < 4 ? 1 : @entries.length - 3)
    height = 204/n
    height += 204 - (height*n)
    height += 16
    @sprites["scroll"].bitmap = Bitmap.new(16, height)
    bmp = pbBitmap("Graphics/Pictures/Common/scrollbar_kn")
    @sprites["scroll"].bitmap.blt(0, 0, bmp, Rect.new(0, 0, 16, 6))
    @sprites["scroll"].bitmap.stretch_blt(Rect.new(0, 6, 16, height-14), bmp, Rect.new(0, 6, 16, 1))
    @sprites["scroll"].bitmap.blt(0, height-8, bmp, Rect.new(0, 8, 16, 8))
    @sprites["scroll"].x = Graphics.width - 32
    @sprites["scroll"].y = (Graphics.height - 204)/2
    @sprites["scroll"].end_y = (Graphics.height - 204)/2
  end
  #-----------------------------------------------------------------------------
  # menu update function
  #-----------------------------------------------------------------------------
  def update
    # scrolling background image
    @sprites["panorama"].update
    # calculations for updating the scrollbar position
    k = (@entries.length < 4 ? 0 : @index - 3)
    k = 0 if k < 0
    n = (@entries.length < 4 ? 1 : @entries.length - 3)
    height = 204/n
    @sprites["scroll"].end_y = (Graphics.height - 204)/2 + height*k
    @sprites["scroll"].y += (@sprites["scroll"].end_y - @sprites["scroll"].y)*0.2
    # updates for each element/entry in the menu
    for i in 0...@entries.length
      j = @entries.length < 4 ? 0 : (@index - 3)
      j = 0 if j < 0
      y = (-j)*(@sprites["#{i}"].src_rect.height + 12) + 49 + i*(@sprites["#{i}"].src_rect.height + 12)
      @sprites["#{i}"].y -= (@sprites["#{i}"].y - y)*0.1
      @sprites["#{i}"].src_rect.x = @sprites["#{i}"].src_rect.width*(@index == i ? 1 : 0)
      @sprites["#{i}"].x += 2 if @sprites["#{i}"].x < Graphics.width - @sprites["#{i}"].src_rect.width - 52
      if i.between?(j,j+3)
        @sprites["#{i}"].opacity += 15 if @sprites["#{i}"].opacity < 255
      else
        @sprites["#{i}"].opacity -= 15 if @sprites["#{i}"].opacity > 128
      end
      if @index == i
        @sprites["#{i}"].tone.gray -= 51 if @sprites["#{i}"].tone.gray > 0
      else
        @sprites["#{i}"].tone.gray += 51 if @sprites["#{i}"].tone.gray < 255
      end
    end
    # sets the index
    if @oldindex != @index
      @sprites["#{@index}"].x -= 6
      @oldindex = @index
    end
  end
  
   # update the date display if necessary
  def updateDateIfNecessary
    showSeconds = true # Make it false to won't show the seconds
    newDate = pbGetTimeNow.strftime(showSeconds ? "%I:%M:%S %p" : "%I:%M %p")
    return false if @dateString==newDate
    @dateString=newDate  
    overlay=@sprites["overlay"].bitmap
    overlay.clear 
    baseColor=Color.new(72,72,72)
    shadowColor=Color.new(160,160,160)
    textPositions=[
       [@dateString,Graphics.width/2,-2,2,baseColor,shadowColor]]
    pbDrawTextPositionsOLD(overlay,textPositions)
    return true
  end

  def pbUpdate
    updateDateIfNecessary #mod
    end

  #-----------------------------------------------------------------------------
  # close out of the menu scene
  #-----------------------------------------------------------------------------
  def pbEndScene
    # disposes the sprite hash
    pbSEPlay("GUI menu close")
    pbHideMenu
    pbDisposeSpriteHash(@sprites)
    @viewport.dispose
  end
  #-----------------------------------------------------------------------------
  def pbRefresh; end
  #-----------------------------------------------------------------------------
end
#===============================================================================
#  Main class used to handle the logic of the pause menu
#===============================================================================
class PokemonPauseMenu
  #-----------------------------------------------------------------------------
  # constructor
  #-----------------------------------------------------------------------------
  def initialize(scene)
    @scene = scene
  end
  #-----------------------------------------------------------------------------
  # show scene
  #-----------------------------------------------------------------------------
  def pbShowMenu
    @scene.pbShowMenu
  end
  #-----------------------------------------------------------------------------
  # start menu
  #-----------------------------------------------------------------------------
  def pbStartPokemonMenu
    # loads up the scene
    pbSEPlay("SE_Zoom2")
    @scene.pbStartScene
    @scene.pbShowMenu
    loop do
      # main loop
      Graphics.update
      Input.update
      @scene.update
      if Input.repeat?(Input::DOWN)
        @scene.index += 1
        @scene.index = 0 if @scene.index > @scene.entries.length - 1
        $PokemonTemp.menuLastChoice = @scene.index
        pbSEPlay("SE_Select1", 75)
      elsif Input.repeat?(Input::UP)
        @scene.index -= 1
        @scene.index = @scene.entries.length - 1 if @scene.index < 0
        $PokemonTemp.menuLastChoice = @scene.index
        pbSEPlay("SE_Select1", 75)
      elsif Input.trigger?(Input::C)
        pbPlayDecisionSE()
        ModularMenu.run(@scene.entries[@scene.index], @scene)
      end
      break if @scene.close || Input.trigger?(Input::B)
    end
    # used to dispose of the scene
    @scene.pbEndScene if @scene.endscene
  end
  #-----------------------------------------------------------------------------
end
