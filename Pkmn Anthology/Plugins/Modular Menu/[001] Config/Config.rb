#-------------------------------------------------------------------------------
#  Your own entries for the pause menu
#
#  To add a new menu entry to the list, register a function block with:
#
#  ModularMenu.add_entry(:NAME, "button text", "icon name") do |menu|
#    # code you want to run
#    # when the entry in the menu is selected
#  end
#
#  To add a condition whether or not the menu entry should appear,
#  register a condition with:
#
#  ModularMenu.add_condition(:NAME) { next (conditional statement here) }
#-------------------------------------------------------------------------------
#  PokeDex
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:POKEDEX, _INTL("Pokedéx"), "menuPokedex") do |menu|
  if Settings::USE_CURRENT_REGION_DEX
    pbFadeOutIn(99999){
      scene = PokemonPokedex_Scene.new
      screen = PokemonPokedexScreen.new(scene)
      screen.pbStartScreen
      menu.refresh
    }
  else
    if $Trainer.pokedex.accessible_dexes.length == 1
      $PokemonGlobal.pokedexDex = $Trainer.pokedex.accessible_dexes[0]
      pbFadeOutIn(99999) {
        pbSEPlay("DEXTER")
        scene = PokemonPokedex_Scene.new
        screen = PokemonPokedexScreen.new(scene)
        screen.pbStartScreen
        menu.refresh
      }
    else
      pbFadeOutIn(99999) {
        pbSEPlay("DEXTER")
        scene = PokemonPokedexMenu_Scene.new
        screen = PokemonPokedexMenuScreen.new(scene)
        screen.pbStartScreen
        menu.refresh
      }
    end
  end
end
# condition to satisfy
#ModularMenu.add_condition(:POKEDEX) { next $Trainer.pokedex && $Trainer.pokedex.accessible_dexes.length > 0 }
ModularMenu.add_condition(:POKEDEX) { next $Trainer.has_pokedex }
#-------------------------------------------------------------------------------
#  Party Screen
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:POKEMON, _INTL("Pokemón"), "menuPokemon") do |menu|
  sscene = PokemonParty_Scene.new
  sscreen = PokemonPartyScreen.new(sscene,$Trainer.party)
  hiddenmove = nil
 pbFadeOutIn(99999) {
    hiddenmove = sscreen.pbPokemonScreen
    if hiddenmove
      menu.pbEndScene
      menu.endscene = false
    end
  }
  if hiddenmove
    Kernel.pbUseHiddenMove(hiddenmove[0],hiddenmove[1])
    menu.close = true
  end
end
# condition to satisfy
ModularMenu.add_condition(:POKEMON) { next $Trainer.party.length > 0 }
#-------------------------------------------------------------------------------
#  Bag Screen
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:BAG, _INTL("Bolso"), "menuBag") do |menu|
  item = 0
  scene = PokemonBag_Scene.new
  screen = PokemonBagScreen.new(scene,$PokemonBag)
  pbFadeOutIn(99999) {
    item = screen.pbStartScreen
    if item
      menu.pbEndScene
      menu.endscene = false
    end
  }
  if item 
    #Kernel.pbUseKeyItemInField(item)
    menu.close = true
  end
end

#-------------------------------------------------------------------------------
#  Quest Screen
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:QUEST, _INTL("Objetivos"), "menuQuest") do |menu|
  pbFadeOutIn(99999) {
    pbViewQuests
    #@scene.pbRefresh
  }
end
#-------------------------------------------------------------------------------
#  Poketch Screen
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:POKETCH, _INTL("Pokewatch"), "menuPoketch") do |menu|
  pbFadeOutIn(99999) {
  pbPoketch
  }  
end
# condition to satisfy
ModularMenu.add_condition(:POKETCH) { next $Trainer.poketch }
#-------------------------------------------------------------------------------
#  PokeGear
#-------------------------------------------------------------------------------
#ModularMenu.add_entry(:POKEGEAR, _INTL("Pokégear"), "menuPokegear") do |menu|
  #scene = PokemonPokegear_Scene.new
  #screen = PokemonPokegearScreen.new(scene)
  #pbFadeOutIn(99999) {
    #screen.pbStartScreen
  #}
#end
# condition to satisfy
#ModularMenu.add_condition(:POKEGEAR) { next $Trainer.has_pokegear }
#-------------------------------------------------------------------------------
#  Trainer Card
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:TRAINER, _INTL("Licencia"), "menuTrainer") do |menu|
  pbFadeOutIn(99999) {
    scene = PokemonTrainerCard_Scene.new
    screen = PokemonTrainerCardScreen.new(scene)
    screen.pbStartScreen
  }
end
#-------------------------------------------------------------------------------
#  Trofeos
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:TROFEOS, _INTL("Logros"), "menuAchievements") do |menu|
  pbFadeOutIn(99999) {
    AchievementsScreen.new
  }
end
#-------------------------------------------------------------------------------
#  Interacción
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:INTERACCIÓN, _INTL("Interacción"), "menuCare") do |menu|
  pbFadeOutIn(99999) {
    PkmnAR.show
  }
end 
ModularMenu.add_condition(:INTERACCIÓN) { next $game_switches[90]==true }
#-------------------------------------------------------------------------------
#  Música
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:JUKEBOX, _INTL("Jukebox"), "menujukebox") do |menu|
  pbFadeOutIn(99999) {
    EnhancedJukebox.new
  }
end
#-------------------------------------------------------------------------------
#  Save Screen
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:SAVE, _INTL("Guardar"), "menuSave") do |menu|
  scene = PokemonSave_Scene.new
  screen = PokemonSaveScreen.new(scene)
  menu.pbEndScene
  menu.endscene = false
  if screen.pbSaveScreen
    menu.close = true
  else
    menu.pbStartScene
    menu.pbShowMenu
    menu.close = false
  end
end
# condition to satisfy
ModularMenu.add_condition(:SAVE) { next !$game_system || !$game_system.save_disabled && !(pbInSafari? || pbInBugContest?) }
#-------------------------------------------------------------------------------
#  Quit Safari-Zone
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:QUIT, _INTL("\\CONTEST"), "menuQuit") do |menu|
  if pbInSafari?
    if Kernel.pbConfirmMessage(_INTL("Would you like to leave the Safari Game right now?"))
      menu.pbEndScene
      menu.endscene = false
      menu.close = true
      pbSafariState.decision = 1
      pbSafariState.pbGoToStart
    end
  else
    if Kernel.pbConfirmMessage(_INTL("Would you like to end the Contest now?"))
      menu.pbEndScene
      menu.endscene = false
      menu.close = true
      pbBugContestState.pbStartJudging
      next
    end
  end
end
# condition to satisfy
ModularMenu.add_condition(:QUIT) { next pbInSafari? || pbInBugContest? }
#-------------------------------------------------------------------------------
#  Options Screen
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:OPTIONS, _INTL("Opciones"), "menuOptions") do |menu|
     scene = PokemonOption_Scene.new
        screen = PokemonOptionScreen.new(scene)  
  pbFadeOutIn(99999) {
    screen.pbStartScreen(true)
    pbUpdateSceneMap
  }
end
#-------------------------------------------------------------------------------
#  Debug Menu
#-------------------------------------------------------------------------------
ModularMenu.add_entry(:DEBUG, _INTL("Debug"), "menuDebug") do |menu|
  pbFadeOutIn(99999) {
    pbDebugMenu
    menu.refresh
  }
end
# condition to satisfy
ModularMenu.add_condition(:DEBUG) { next $DEBUG }