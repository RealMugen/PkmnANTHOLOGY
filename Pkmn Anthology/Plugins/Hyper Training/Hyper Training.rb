#===============================================================================
#  Hyper Training Script
#  Credit to Jonas930
#===============================================================================
#  How to Use:
#     pbMrHyper(ITEM1,ITEM2)
#         ITEM1 => the item that you want player use with boosting ONE stats into MAX
#         ITEM2 => the item that you want player use with boosting ALL stats into MAX
#  Example: pbMrHyper(:STARDUST,:STARPIECE)
#===============================================================================

def pbMrHyper(item1,item2)
  @nameitem1=GameData::Item.get(item1).name
  @nameitem2=GameData::Item.get(item2).name
  @hasitem1=$PokemonBag.pbHasItem?(item1)
  @hasitem2=$PokemonBag.pbHasItem?(item2)
  Kernel.pbMessage(_INTL("The name's Mr. Hyper!\nI can help Pokemon do Hyper Training!"))
  if Kernel.pbConfirmMessage(_INTL("Want to try some of my Hyper Training to boost your Pokemon's stats?"))
    if @hasitem1 || @hasitem2
      item = 0 if @hasitem1 && !@hasitem2
      item = 1 if !@hasitem1 && @hasitem2
      item = Kernel.pbMessage(_INTL("Which item would you want to use on Hyper Training?"),[@nameitem1,@nameitem2]) if @hasitem1 && @hasitem2
      itemuse = (item == 0 ? @nameitem1 : @nameitem2)
      if Kernel.pbConfirmMessage(_INTL("Are you gonna use one \\c[1]{1}\\c[0] for Hyper Training?",itemuse))
        Kernel.pbMessage(_INTL("Which one of your Pokemon do you want to do some Hyper Training on?"))
        pbChoosePokemon(1,2)
        pokemon = $Trainer.pokemonParty[pbGet(1)]
        if pbGet(1) < 0
        elsif $Trainer.party[pbGet(1)].egg?
          Kernel.pbMessage(_INTL("An Egg?!\nI understand why you're hyped to have one,\nbut I can't train that thing yet!"))
        elsif pokemon.level!=100
          Kernel.pbMessage(_INTL("Oh no...\nNo, no, no!"))
          Kernel.pbMessage(_INTL("That Pokemon hasn't leveled up enough to be ready for my amazing Hyper Training!"))
          Kernel.pbMessage(_INTL("Only Lv. 100 Pokemon can handle the hype!"))
        else
          if item==0
            stat = Kernel.pbMessage(_INTL("Which one of {1}'s stats do you want to do some Hyper Training on?",pokemon.name),
            [_INTL("HP"),_INTL("Attack"),_INTL("Defense"),_INTL("Speed"),_INTL("Sp. Atk"),_INTL("Sp. Def")])
            if pokemon.ivMaxed[stat]==true
              Kernel.pbMessage(_INTL("But that Pokemon is already so awesome that it doesn't need any training!"))
            else
              pokemon.ivMaxed[stat]=true
              itemuse = (item == 0 ? item1 : item2)
              $PokemonBag.pbDeleteItem(itemuse)
              Kernel.pbMessage(_INTL("Then get hype!"))
              Kernel.pbMessage(_INTL("Because I'm about to do some real Hyper Training on {1} here!",pokemon.name))
              Kernel.pbMessage(_INTL("All right!\n{1} got even stronger thanks to my Hyper Training!",pokemon.name))
            end
          elsif item==1
            if (pokemon.ivMaxed[0]==true && pokemon.ivMaxed[1]==true && pokemon.ivMaxed[2]==true && 
                pokemon.ivMaxed[3]==true && pokemon.ivMaxed[4]==true && pokemon.ivMaxed[5]==true)
              Kernel.pbMessage(_INTL("But that Pokemon is already so awesome that it doesn't need any training!"))
            else
              for i in 0..5
                pokemon.ivMaxed[i]=true
              end
              itemuse = (item == 0 ? item1 : item2)
              $PokemonBag.pbDeleteItem(itemuse)
              Kernel.pbMessage(_INTL("Then get hype!"))
              Kernel.pbMessage(_INTL("Because I'm about to do some real Hyper Training on {1} here!",pokemon.name))
              Kernel.pbMessage(_INTL("All right!\n{1} got even stronger thanks to my Hyper Training!",pokemon.name))
            end
          end
        end
      end
    else
      Kernel.pbMessage(_INTL("Oh no...\nNo, no, no!"))
      Kernel.pbMessage(_INTL("You don't have any {1} or {2}!\nNot even one!",@nameitem1,@nameitem2))
    end
  end
  Kernel.pbMessage(_INTL("Then come back anytime!\nMr. Hyper will always be hyped up to see you!"))
end