=begin

Use AdventureGuide.list(hash)
	hash stores information of big title (Hash)
	Value in this hash:
		name -> it's title of this guide, big title (String)
		description -> it's description of big title (String)
		sub -> it's array to store all of sub-title, small title (Array)
			Each value in array is Hash, they are these values:
			name -> it's name of sub-title
			description -> it's description of small title

	In description, use \n if you want to create new line.

=end

# This is examples
# You can delete or edit it or add =begin below this line and add =end above line 'Add new list here'

AdventureGuide.list({
	:name => "10 Máximas del Aventurero.",
	:description => "Las 10 máximas que todo entrenador debe conocer.",
	:sub => [
		{:name => "Primera máxima de aventurero", :description => "Abre el Menú con el Botón BACK, por defecto (X)."},
		{:name => "Segunda máxima de aventurero", :description => "Guarda tu progreso con GUARDAR. Hazlo constantemente para no perder tus avances."},
		{:name => "Tercera máxima de aventurero", :description => "Lanza Pokeball para capturar Pokemón"},
		{:name => "Cuarta máxima de aventurero", :description => "Para atrapar Pokemón, primero reduce sus puntos de salud o HP."},
		{:name => "Quinta máxima de aventurero", :description => "Entrena a tu propio Pokémon haciéndolo combatir."},
		{:name => "Sexta máxima de aventurero", :description => "Cura a tu equipo en los Centros Pokémon."},
		{:name => "Séptima máxima de aventurero", :description => "Haz acopio de objetos."},
		{:name => "Octava máxima de aventurero", :description => "Habla con tanta gente como puedas."},
		{:name => "Novena máxima de aventurero", :description => "Consulta el Mapa si te pierdes."},
		{:name => "Décima máxima de aventurero", :description => "¡Disfruta cada segundo de tu aventura Pokemón!"},
	]
})

AdventureGuide.list({
	:name => "Sobre el Cuidado Personal.",
	:description => "Información necesaria para la sobrevivencia.",
	:sub => [
		{:name => "Sobre el Cansancio", :description => "Si no descansas, tu energía baja. Sin energías es imposible correr o continuar el viaje. Si la energía baja de 50% el hambre y la sed comenzarán a sentirse con fuerza."},
		{:name => "Sobre la Sed", :description => "Es importante hidratarse constantemente. Una ausencia constante de líquidos puede significar serios riesgos para la salud. Si la sed es extrema y la falta de descanso es evidente, podrías desvanecerte."},
		{:name => "Sobre el Hambre", :description => "Una alimentación sana garantiza un buen porvenir. No comer durante demasiado tiempo, puede significar serios riesgos para la salud. Si el hambre se suma a una falta de cansancio podrías desvanecerte."},
		{:name => "Sobre las Bajas Temperaturas", :description => "La temperatura varía constantemente. Si la temperatura baja demasiado, el cansancio aumenta y el cuerpo requiere más calorías, por lo que el hambre subirá."},
            {:name => "Sobre las Altas Temperaturas", :description => "La temperatura varía constantemente. Si la temperatura sube demasiado, el cansancio aumenta y el cuerpo requiere más hidratación, por lo que la sed subirá."},
            {:name => "Sobre las Temperaturas Extremas", :description => "En algunos momentos la temperatura extrema podría causar estragos. Puedes usar algunos objetos para protegerte del frío o del calor."},
            {:name => "Sobre la Recuperación", :description => "Para recuperarte del cansancio, de la sed o del hambre puedes alimentarte, levantar un campamento o utilizar los servicios del Centro Pokemón."},
            {:name => "Sobre la Alimentación", :description => "Si necesitas alimentarte puedes consumir todo tipo de alimentos o bebestibles. Algunos tienen solo efectos positivos y otros tantos algunos efectos contrarios."},
	]
})

AdventureGuide.list({
	:name => "Sobre los Encuentros Pokemón.",
	:description => "Información necesaria para la captura.",
	:sub => [
		{:name => "¿Dónde encontrar Pokemón?", :description => "Los Pokemón están en todos lados. Puedes encontrarlos en la hierba, en cuevas, en el mar, en ríos, lagos y donde puedas imaginar."},
		{:name => "¿Cómo capturar un Pokemón?", :description => "Para capturar un Pokemón debes usar Pokeball, diferentes Pokeball tienen diferentes efectos. Reduce su salud o cambia sus estados para aumentar tus posibilidades."},
		{:name => "¿Los Pokemón huyen?", :description => "Sí, tienes que estar atento, pues una vez que lanzas la Pokeball y fallas en la captura, el Pokemón tiene altas posibilidades de escabullirse e huir."},
		{:name => "¡La Hierba se sacude!", :description => "A veces la hierba se sacude, debes explorarla en el momento adecuado, pues podrías llevarte algunas sorpresas."},
            {:name => "¡Apareció un Pokemón diferente en la lluvia!", :description => "A veces los encuentros cambian dependiendo de la época del año o del clima. Debes estar atento a elegir el momento para buscar Pokemón."},
            {:name => "¿Pokemón fuera de la hierba?", :description => "Algunos eventos, lugares u objetos pueden posibilitar encuentros visibles. Es decir, podrás ver al Pokemón pasearse por la hierba e iniciar un combate con él."},
	]
})

AdventureGuide.list({
	:name => "Sobre el Cuidado Pokemón.",
	:description => "Información necesaria para el cuidado Pkmn.",
	:sub => [
		{:name => "¿Cómo curar a mis Pokemón?", :description => "Si los Pokemón pierden HP (Puntos de Salud) puedes curarlos parcial o totalmente con objetos o por completo en un Centro Pokemón."},
		{:name => "¿Y los movimientos?", :description => "Para poder luchar, un Pokemón necesita usar ataques que dependen de los PP (Puntos de Poder). Si se acaban, recupéralos con objetos o en un Centro Pokemón."},
		{:name => "¿Problemas de Estado?", :description => "A veces los Pokemón se envenenan, paralizan, congelan, queman, duermen, adormecen o enfrían producto del combate. Cúralos con objetos o en el Centro Pokemón"},
		{:name => "¡Mi Pokemón está confundido!", :description => "Algunos cambios de estado como el enamoramiento o la confusión solo permanecen durante el combate. Tranquilo, tu Pokemón va a estar bien."}
	]
})

AdventureGuide.list({
	:name => "Sobre el Entorno.",
	:description => "Información sobre lo que nos rodea.",
	:sub => [
            {:name => "Sobre las Estaciones", :description => "Durante el año las estaciones cambian y esto no solo quiere decir un cambio en el paisaje, también puede cambiar el clima, los efectos en batalla e incluso los Pokemón que pueden aparecer en la hierba."},
		{:name => "Sobre el Verano", :description => "El verano es la época más templada del año. Las temperaturas se mantienen equilibradas y el clima generalmente es agradable, a excepción de algunos días calurosos."},
		{:name => "Sobre el Otoño", :description => "Los bellos colores del otoño reinan tras el verano. Si bien el clima es agradable y pueden verse algunas hojas caer, la temperatura a veces puede bajar ligeramente."},
		{:name => "Sobre el Invierno", :description => "El invierno es la época más gris en Kanto, las lluvias e incluso algo de nieve están a la orden del día. Las temperaturas pueden bajar, sobre todo durante la noche."},
		{:name => "Sobre la Primavera", :description => "En primavera los colores son más intensos y las flores reinan. El clima es agradable, pero generalmente puede haber más polen o hojas de flores cayendo de los árboles."},
            {:name => "Sobre el Clima", :description => "Los climas varían y dependen de muchos factores. La mayoría de ellos tienen efectos negativos o positivos en combate dependiendo del tipo o las habilidades del Pokemón."}
	]
})

AdventureGuide.list({
	:name => "Sobre los Entrenadores.",
	:description => "Información sobre los duelos.",
	:sub => [
            {:name => "Batallas Pokemón", :description => "En el mundo encontrarás cientos de entrenadores que buscarán desafiarte en combate. Las batallas te ayudan a mejorar, hacen crecer a tus Pokemón y te dan experiencia."},
		{:name => "¿Puedo evitar un combate?", :description => "La única forma de evitar un combate es alejándote de la mirada de los entrenadores, generalmente verás una barra negra en ambos lados de la pantalla indicando que estás peligrosamente cerca."},
		{:name => "Sobre el Dinero", :description => "Es una buena práctica entregar una recompensa en dinero al entrenador vencedor, pero no todos lo hacen. Procura hacerlo siempre para cuidar las buenas costumbres."},
		{:name => "Sobre las Batallas Dobles", :description => "En algunos momentos determinados podrás utilizar dos o hasta tres Pokemón paralelamente en combate. A veces lo harás solo, otras veces en compañía de otros entrenadores."},
		{:name => "Sobre los Equipos", :description => "Hay algunos entrenadores que forman parte de algunos equipos específicos. Solo te retarán en combate si te identifican como un miembro de un equipo rival."},
            {:name => "¿Tengo Ventaja?", :description => "Las posibilidades de triunfo no solo dependen del nivel del Pokemón. También de las debilidades y fortalezas frente a ciertos tipos, las estadísticas del Pokemón o la astucia del entrenador."},
	]
})



# Add new list here